package com.viettel.validator;

import com.viettel.fw.common.util.GetTextFromBundleHelper;
import nl.captcha.Captcha;
import org.omnifaces.util.Faces;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Created by ThanhNT77 on 6/14/2015.
 */
@FacesValidator("capchaValidator")
public class CapchaValidator implements Validator {
    @Override
    public void validate(FacesContext context, UIComponent uiComponent, Object o) throws ValidatorException {
        Captcha secretcaptcha = Faces.getSessionAttribute(Captcha.NAME);

        if (secretcaptcha.isCorrect(o.toString())) {
            return;
        }
        // optional: clear field
        ((HtmlInputText) uiComponent).setSubmittedValue("");
        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, GetTextFromBundleHelper.getText("validate.capcha.validatorMessage"), GetTextFromBundleHelper.getText("validate.capcha.validatorMessage")));
    }
}
