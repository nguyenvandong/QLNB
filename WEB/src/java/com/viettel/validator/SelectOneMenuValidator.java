package com.viettel.validator;

import com.viettel.common.Const;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Created by ThanhNT77 on 6/14/2015.
 */
@FacesValidator("selectOneMenuValidator")
public class SelectOneMenuValidator implements Validator {
    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
        boolean isValid = o != null && o instanceof Long && Const.DEFAULT_LONG_VALUE.equals(o) || o != null && o instanceof String && Const.DEFAULT_STRING_VALUE.equals(o);
        if (isValid) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
}
