package com.viettel.validator;

import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.GetTextFromBundleHelper;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ThanhNT77 on 6/14/2015.
 */
@FacesValidator("custIdNoValidator")
public class CustIdNoValidator implements Validator {
    public static final int max1 = 9;
    public static final int max2 = 12;

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
        if (o != null) {
//            CustIdentityDTO identityDTO = (CustIdentityDTO)o;
//            String idNo = identityDTO.getIdNo();
            String idNo = (String)o;
            if (!DataUtil.isNullOrEmpty(idNo) && (!isTrueLenghIdNo(idNo) || !isValidRegixIdNo(idNo))) {
//                String msgValidate = (String)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("msgValidate");
//                String msgRequire = (String)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("msgRequire");
                String id = uiComponent.getId();
                if (id != null) {
                    String arr[] = id.split("_");
                    if (arr.length > 0) {
                        id = arr[0];
                    }
                }
                String msgError = (String)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get(id);
                RequestContext.getCurrentInstance().execute("focusElementError('" + id + "')");
                FacesMessage message = new FacesMessage(msgError);
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(message);

            }
        }
    }

    /**
     * valiate do dai
     * @author ThanhNT
     * @param idNo
     * @return
     */
    private boolean isTrueLenghIdNo(String idNo) {
        return (max1 == idNo.trim().length() || max2 == idNo.trim().length());
    }

    /**
     * validate format idno
     * @author ThanhNT
     * @param idNo
     * @return
     */
    private boolean isValidRegixIdNo(String idNo) {
        Pattern pt = Pattern.compile(GetTextFromBundleHelper.getText("isdn.phone.regex"));
        Matcher matcher = pt.matcher(idNo);
        return matcher.matches();
    }
}
