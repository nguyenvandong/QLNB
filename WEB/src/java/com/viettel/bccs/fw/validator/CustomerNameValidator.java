package com.viettel.bccs.fw.validator;

import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.GetTextFromBundleHelper;
import org.apache.commons.lang.StringUtils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("validator.custname")
public class CustomerNameValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {

        String custName = DataUtil.safeToString(value);
        if (StringUtils.length(custName) > 300) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, GetTextFromBundleHelper.getText("common.validate.cust.sum"),
                    GetTextFromBundleHelper.getText("common.validate.cust.detail.length"));
            throw new ValidatorException(msg);
        }
    }

}
