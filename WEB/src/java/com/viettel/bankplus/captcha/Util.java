package com.viettel.bankplus.captcha;

import java.security.SecureRandom;

/**
 * Created by Truongxp on 09/07/17.
 */
public class Util {
    public static String generateCaptchaText(int captchaLength) {
        String saltChars = "QWERTYUOPASDFGHKJZXCVBNM1234567890";
        StringBuilder captchaStrBuffer = new StringBuilder();
//        java.util.Random rnd = new java.util.Random();
        SecureRandom ranGen = new SecureRandom();
        // build a random captchaLength chars salt
        while (captchaStrBuffer.length() < captchaLength) {
            int index =  (int)(ranGen.nextDouble() * saltChars.length());
            captchaStrBuffer.append(saltChars.substring(index, index + 1));
        }
        return captchaStrBuffer.toString();
    }
}
