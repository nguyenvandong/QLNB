package com.viettel.bankplus.captcha;

import nl.captcha.Captcha;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.text.producer.DefaultTextProducer;
import nl.captcha.text.renderer.DefaultWordRenderer;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Truongxp on 09/07/17.
 */
public class CaptchaServlet extends HttpServlet {
    private static final long serialVersionUID = 1;
    public static final String FILE_TYPE = "jpg";
    private static final Logger logger = Logger.getLogger(CaptchaServlet.class);


    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Max-Age", 0);
            int width = 200;
            int height = 50;
            java.util.List<Color> textColors = Arrays.asList(
                    Color.BLACK);

            java.util.List<Font> textFonts = Arrays.asList(
                    new Font("Arial", Font.BOLD, 48),
                    new Font("Courier", Font.BOLD, 48));
            Captcha captcha = (new Captcha.Builder(width, height))
                    .addText(
                            new DefaultTextProducer(),
                            new DefaultWordRenderer(textColors, textFonts)).
                            addNoise().addBackground()
                    .build();

            CaptchaServletUtil.writeImage(response, captcha.getImage());
            String captchaStr = captcha.getAnswer();
            request.getSession().setAttribute("captcha", captchaStr);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        try {
            doPost(request, response);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

}

