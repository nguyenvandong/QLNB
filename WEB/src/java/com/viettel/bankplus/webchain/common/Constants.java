package com.viettel.bankplus.webchain.common;

import java.util.HashMap;

/**
 * TungHuynh Created by sondt18 on 9/13/2017.
 */
public class Constants {
    public static final String PREFIX = "84";

    public static interface RESPONE_CODE {
        String SUCCESS = "00";
        String OTHER = "xx";
    }

    public static interface PAY_TYPE {
        String MOBILE = "telecom";
        String HOMEPHONE = "homePhone";
        String FIXEDPHONE = "fixedTelecom";
        String INTERNET = "internet";
        String CAB = "television";
    }



    public class MESSAGE_CONTENT {
        public static final String SYS_ERROR = "Có lỗi khi xử lý";
        public static final String MISSING_AMOUNT = "Bạn chưa chọn giá trị thẻ nạp";
        public static final String MISSING_MSISDN = "Bạn chưa nhập số thuê bao";
        public static final String MISSING_ACCOUNT = "Bạn chưa nhập số tài khoản/thuê bao";
        public static final String MISSING_TELCO = "Bạn chưa chọn nhà mạng";
        public static final String MISSING_PARTNER = "Bạn chưa chọn nhà cung cấp";
        public static final String MSISDN_INVALID = "Số điện thoại không hợp lệ";
        public static final String DISCOUNT_INVALID = " Lỗi, dịch vụ chưa được cấu hình % chiết khấu, hoặc % chiết khấu không hợp lệ.";
        public static final String PAY_SUCCESS = "QUÝ KHÁCH ĐÃ THỰC HIỆN GIAO DỊCH THÀNH CÔNG!\n" +
                "Số điện thoại: %s\n" +
                "Số tiền: %s\n" +
                "Chiết khấu: %s\n" +
                "Công nợ: %s\n" +
                "Thời gian giao dịch: %s\n";
        public static final String RECHARGE_SUCCESS = "QUÝ KHÁCH ĐÃ THỰC HIỆN GIAO DỊCH THÀNH CÔNG!\n" +
                "Tài khoản: %s\n" +
                "Số tiền: %s\n" +
                "Thời gian giao dịch: %s\n";
    }

}
