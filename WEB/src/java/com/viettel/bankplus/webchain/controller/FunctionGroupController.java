package com.viettel.bankplus.webchain.controller;

import com.google.common.collect.Lists;
import com.viettel.bankplus.webchain.dto.*;
import com.viettel.bankplus.webchain.input.InputLazyFunctionGroupSearch;
import com.viettel.bankplus.webchain.service.*;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.utils.ExcelUtil;
import com.viettel.web.common.controller.BaseController;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * @author daibq on 5/7/2018
 */
@ManagedBean
@Component
@Scope("view")
public class FunctionGroupController extends BaseController {
    private static final long serialVersionUID = 1;
    @Autowired
    private transient ApDomainService apDomainService;
    @Autowired
    private transient PermissionGroupService permissionGroupService;
    @Autowired
    private transient ActionService actionService;
    @Autowired
    private transient EmployeeService employeeService;


    private List<ActionDTO> actionDTOList;
    private transient InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch;
    private List<ApDomainDTO> listStatus = new ArrayList<>();
    private List<PermissionGroupDTO> listPermissionCode = new ArrayList<>();
    private List<ApDomainDTO> listUserObjectType = new ArrayList<>();
    private LazyDataModel<PermissionGroupDTO> permissionGroupDTOS;
    private PermissionGroupDTO permissionGroupDTO;
    private boolean isCreateState;
    private LazyDataModel<FunctionObjectDTO> functionObjectDTOS;
    private PermissionGroupDTO dtoDEtail;
    private List<PermissionGroupDTO> permissionGroupDTOExportList;
    private File fileExport;
    private String suffix;
    private String templatePath;
    private Map<String, String> functionGruopStatusMap;
    private Map<String, String> permissionTypeMap;
    private EmployeeDTO employeeDTO;

    @PostConstruct
    public void init() {
        permissionGroupDTO = new PermissionGroupDTO();

        inputLazyFunctionGroupSearch = new InputLazyFunctionGroupSearch();
        inputLazyFunctionGroupSearch.setChainShopId(BccsLoginSuccessHandler.getShopId());
        templatePath = Const.WEB_CHAIN.FUNCTION_GROUP_EXCEL_TEMPLATE_PATH;
        try {
            employeeDTO = employeeService.findOne(BccsLoginSuccessHandler.getChainUserType());
            listStatus = apDomainService.getValueApDomainByType(Const.AP_DOMAIN.WC_STATUS);
            listPermissionCode = permissionGroupService.getPermissionCodeListByShopId(BccsLoginSuccessHandler.getShopId());
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.PERMISSION_GROUP_TYPE);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        /*permissionGroupDTOS = findLazy();*/
        getNameforExcel();
    }

    // lấy tên theo giá trị đưa vào excel
    public void getNameforExcel() {
        functionGruopStatusMap = new HashMap<>();
        permissionTypeMap = new HashMap<>();
        if (!listStatus.isEmpty()) {
            for (ApDomainDTO apDomainDTO : listStatus) {
                functionGruopStatusMap.put(apDomainDTO.getValue().trim().toLowerCase(), apDomainDTO.getName());
            }
        }
        if (!listUserObjectType.isEmpty()) {
            for (ApDomainDTO apDomainDTO : listUserObjectType) {
                permissionTypeMap.put(apDomainDTO.getValue().trim().toLowerCase(), apDomainDTO.getName());
            }
        }

    }

    public void prepareToShowAdd() {
        permissionGroupDTO = new PermissionGroupDTO();
    }

    public void prepareToShowEdit(PermissionGroupDTO permissionGroupDtoOld) {
        permissionGroupDTO = new PermissionGroupDTO();
        BeanUtils.copyProperties(permissionGroupDtoOld, permissionGroupDTO);

    }

    private LazyDataModel<PermissionGroupDTO> findLazy() {
        return new LazyDataModel<PermissionGroupDTO>() {
            @Override
            public List<PermissionGroupDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    if (sortField != null && sortOrder != null) {
                        if (SortOrder.ASCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.ASC, sortField);
                        } else if (SortOrder.DESCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.DESC, sortField);
                        }
                    }
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(permissionGroupService.countLazyFunctionGroup(inputLazyFunctionGroupSearch));
                    return permissionGroupService.findLazingPaging(inputLazyFunctionGroupSearch, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findLazy", e);
                    return Lists.newArrayList();
                }
            }


            @Override
            public PermissionGroupDTO getRowData(String rowKey) {
                List<PermissionGroupDTO> tmp = (List<PermissionGroupDTO>) getWrappedData();
                Optional<PermissionGroupDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getPermissionGroupId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(PermissionGroupDTO object) {
                return object != null ? object.getPermissionGroupId() : null;
            }
        };

    }

    /*
   * check null các trường có trong form của danh mục tham số
   */
    private boolean validateAddNew() {
        Pattern docCode;
        docCode = Pattern.compile("[\\d\\w_]+");
        Pattern pattern;
        pattern = Pattern.compile("[\\d\\w_ ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+");
        if (DataUtil.isNullObject(permissionGroupDTO.getPermissionGroupCode())) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.permissionGroupCodeEmpty")));
            return false;
        }
        if (permissionGroupDTO.getPermissionGroupCode().length() > 50) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupCode")));
            return false;
        }
        if (!docCode.matcher(permissionGroupDTO.getPermissionGroupCode()).matches()) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImportWord.permissionGroupCode")));
            return false;
        }

        if (DataUtil.isNullObject(permissionGroupDTO.getPermissionGroupName())) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.permissionGroupNameEmpty")));
            return false;
        }

        if (permissionGroupDTO.getPermissionGroupName().length() > 200) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupName")));
            return false;
        }
        if (permissionGroupDTO.getDescription().length() > 500) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupDescription")));
            return false;
        }
        if (!pattern.matcher(permissionGroupDTO.getPermissionGroupName()).matches()) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImportWord.permissionGroupName")));
            return false;
        }

        if (DataUtil.isNullObject(permissionGroupDTO.getUserObjectType())) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.permissionGroupTypeEmpty")));
            return false;
        }
        if (DataUtil.isNullObject(permissionGroupDTO.getStatus())) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.permissionGroupStatusEmpty")));
            return false;
        }


        return true;
    }

    /*
   * check code của nhóm chức năng
   */
    public boolean validateCodeBeforeSave() {

        try {
            boolean isDuplicateCode = permissionGroupService.checkCodeOrId(permissionGroupDTO.getPermissionGroupCode(), null);
            if (!isDuplicateCode) {
                FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                        getText("webchain.functionGroup.duplicate.permissionGroupCode")));
                return false;
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.err")));
            logger.error(e.getMessage(), e);
        }
        return true;
    }

    public boolean validateNameBeforeSave() {

        try {
            boolean isDuplicateName = permissionGroupService.checkName(BccsLoginSuccessHandler.getShopId(), permissionGroupDTO.getPermissionGroupName());
            if (!isDuplicateName) {
                FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.duplicate.permissionGroupName")));
                return false;

            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.err")));
            logger.error(e.getMessage(), e);
        }
        return true;
    }

      /*
    * check bản ghi còn tồn tại hay không của nhóm chức năng
    */

    public boolean checkRecordExist(PermissionGroupDTO dto) {
        try {
            boolean isRecordExist = permissionGroupService.checkCodeOrId(null, dto.getPermissionGroupId());
            if (isRecordExist) {
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.exist")));
                RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                return false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return true;
    }

    /*
* check va update bản ghi của danh mục tham số
*/
    public boolean checkValidateUpdateAll() {

        try {
            if (employeeDTO.getEmployeeType() == 1) {
                permissionGroupDTO.setPermissionGroupType(2l);

            } else if (employeeDTO.getEmployeeType() == 2) {
                permissionGroupDTO.setPermissionGroupType(3l);
            } else {
                permissionGroupDTO.setPermissionGroupType(3l);
            }
            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_TYPE_UPDATE));
            actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_TYPE_UPDATE));
            Long statusValue = DataUtil.isNullObject(permissionGroupDTO.getStatus()) ? 1 : permissionGroupDTO.getStatus();
            PermissionGroupDTO oldPermissionGroupDTO = permissionGroupService.findOne(permissionGroupDTO.getPermissionGroupId());
            String oldCode = oldPermissionGroupDTO.getPermissionGroupCode();
            String oldName = oldPermissionGroupDTO.getPermissionGroupName();
            if (!DataUtil.safeEqual(oldCode.toUpperCase(), permissionGroupDTO.getPermissionGroupCode().toUpperCase())) {
                if (validateCodeBeforeSave()) {
//                    apDomainDTO.setStatus(1L);
                    permissionGroupDTO.setStatus(statusValue);
                    actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                    permissionGroupDTO.setChainShopId(BccsLoginSuccessHandler.getShopId());
                    permissionGroupDTO.setUpdateUser(BccsLoginSuccessHandler.getUserName());
                    permissionGroupService.saveorUodate(permissionGroupDTO, actionLogDTO);
                    RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                    FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.update.success")));
                }
            } else if (!DataUtil.safeEqual(oldName, permissionGroupDTO.getPermissionGroupName())) {
                if (validateNameBeforeSave()) {
                    permissionGroupDTO.setStatus(statusValue);
                    actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                    permissionGroupDTO.setChainShopId(BccsLoginSuccessHandler.getShopId());
                    permissionGroupDTO.setUpdateUser(BccsLoginSuccessHandler.getUserName());
                    permissionGroupService.saveorUodate(permissionGroupDTO, actionLogDTO);
                    RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                    FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.update.success")));
                }
            } else {
//            apDomainDTO.setStatus(1L);
                permissionGroupDTO.setStatus(statusValue);
                actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                permissionGroupDTO.setChainShopId(BccsLoginSuccessHandler.getShopId());
                permissionGroupDTO.setUpdateUser(BccsLoginSuccessHandler.getUserName());
                permissionGroupService.saveorUodate(permissionGroupDTO, actionLogDTO);
                RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.update.success")));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.err")));
            logger.error(e.getMessage(), e);
        }


        return true;
    }

    @Secured("@")
    public void doSearch() {
        try {
            permissionGroupDTOS = findLazy();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void doSaveorUpdate() {
        ActionLogDTO actionLogDTO = new ActionLogDTO();
        try {
            if (!(validateAddNew()))
                return;
            Long statusValue = DataUtil.isNullObject(permissionGroupDTO.getStatus()) ? 1 : permissionGroupDTO.getStatus();
            isCreateState = permissionGroupDTO.getPermissionGroupId() == null ? true : false;
            if (isCreateState) {
                if (validateCodeBeforeSave() && validateNameBeforeSave()) {
                    if (employeeDTO.getEmployeeType() == 1) {
                        permissionGroupDTO.setPermissionGroupType(2l);

                    } else if (employeeDTO.getEmployeeType() == 2) {
                        permissionGroupDTO.setPermissionGroupType(3l);
                    } else {
                        permissionGroupDTO.setPermissionGroupType(3l);
                    }
                    permissionGroupDTO.setStatus(statusValue);
                    actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_TYPE_ADD_NEW));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_TYPE_ADD_NEW));
                    permissionGroupDTO.setChainShopId(BccsLoginSuccessHandler.getShopId());
                    permissionGroupDTO.setCreateUser(BccsLoginSuccessHandler.getUserName());
                    permissionGroupService.saveorUodate(permissionGroupDTO, actionLogDTO);
                    RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                    FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.create.success")));

                }
            } else {
                checkValidateUpdateAll();
            }
            listPermissionCode = permissionGroupService.getPermissionCodeListByShopId(BccsLoginSuccessHandler.getShopId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.err")));
        }
    }

    public void doDelete(PermissionGroupDTO dto) {
        ActionLogDTO actionLogDTO = new ActionLogDTO();
        actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_TYPE_DELETE));
        actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_TYPE_DELETE));
        actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
        if (employeeDTO.getEmployeeType() == 1) {
            permissionGroupDTO.setPermissionGroupType(2l);

        } else if (employeeDTO.getEmployeeType() == 2) {
            permissionGroupDTO.setPermissionGroupType(3l);
        } else {
            permissionGroupDTO.setPermissionGroupType(3l);
        }
        try {
            if (checkRecordExist(dto)) {
                permissionGroupService.deleteList(dto, actionLogDTO, BccsLoginSuccessHandler.getShopId());
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO,
                        null, getText("webchain.functionGroup.delete.success")));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.catalog.err")));
        }

    }

    public void doViewDetail(PermissionGroupDTO dto) {
        try {
            dtoDEtail = new PermissionGroupDTO();
            BeanUtils.copyProperties(dto, dtoDEtail);
            functionObjectDTOS = lazyViewDetail();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }






    /**
     * @throws Exception
     */
    public void prepareDownloadFileExport() throws Exception {

        permissionGroupDTOExportList = permissionGroupService.findLazingPaging(inputLazyFunctionGroupSearch, 0, 0);
    }

    private LazyDataModel<FunctionObjectDTO> lazyViewDetail() {
        return new LazyDataModel<FunctionObjectDTO>() {
            @Override
            public List<FunctionObjectDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    if (sortField != null && sortOrder != null) {
                        if (SortOrder.ASCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.ASC, sortField);
                        } else if (SortOrder.DESCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.DESC, sortField);
                        }
                    }
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(permissionGroupService.listDetailCount(dtoDEtail));
                    return permissionGroupService.listDetail(dtoDEtail, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findLazy", e);
                    return Lists.newArrayList();
                }
            }


            @Override
            public FunctionObjectDTO getRowData(String rowKey) {
                List<FunctionObjectDTO> tmp = (List<FunctionObjectDTO>) getWrappedData();
                Optional<FunctionObjectDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getFunctionObjectId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(FunctionObjectDTO object) {
                return object != null ? object.getFunctionObjectId() : null;
            }
        };

    }


    //    getter,setter


    public LazyDataModel<FunctionObjectDTO> getFunctionObjectDTOS() {
        return functionObjectDTOS;
    }

    public void setFunctionObjectDTOS(LazyDataModel<FunctionObjectDTO> functionObjectDTOS) {
        this.functionObjectDTOS = functionObjectDTOS;
    }

    public InputLazyFunctionGroupSearch getInputLazyFunctionGroupSearch() {
        return inputLazyFunctionGroupSearch;
    }

    public void setInputLazyFunctionGroupSearch(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch) {
        this.inputLazyFunctionGroupSearch = inputLazyFunctionGroupSearch;
    }

    public List<ApDomainDTO> getListStatus() {
        return listStatus;
    }

    public void setListStatus(List<ApDomainDTO> listStatus) {
        this.listStatus = listStatus;
    }

    public List<ApDomainDTO> getListUserObjectType() {
        return listUserObjectType;
    }

    public void setListUserObjectType(List<ApDomainDTO> listUserObjectType) {
        this.listUserObjectType = listUserObjectType;
    }

    public LazyDataModel<PermissionGroupDTO> getPermissionGroupDTOS() {
        return permissionGroupDTOS;
    }

    public void setPermissionGroupDTOS(LazyDataModel<PermissionGroupDTO> permissionGroupDTOS) {
        this.permissionGroupDTOS = permissionGroupDTOS;
    }

    public PermissionGroupDTO getPermissionGroupDTO() {
        return permissionGroupDTO;
    }

    public void setPermissionGroupDTO(PermissionGroupDTO permissionGroupDTO) {
        this.permissionGroupDTO = permissionGroupDTO;
    }

    public boolean isCreateState() {
        return isCreateState;
    }

    public void setCreateState(boolean createState) {
        isCreateState = createState;
    }

    public PermissionGroupDTO getDtoDEtail() {
        return dtoDEtail;
    }

    public void setDtoDEtail(PermissionGroupDTO dtoDEtail) {
        this.dtoDEtail = dtoDEtail;
    }

    public List<PermissionGroupDTO> getPermissionGroupDTOExportList() {
        return permissionGroupDTOExportList;
    }

    public void setPermissionGroupDTOExportList(List<PermissionGroupDTO> permissionGroupDTOExportList) {
        this.permissionGroupDTOExportList = permissionGroupDTOExportList;
    }

    public ApDomainService getApDomainService() {
        return apDomainService;
    }

    public void setApDomainService(ApDomainService apDomainService) {
        this.apDomainService = apDomainService;
    }

    public File getFileExport() {
        return fileExport;
    }

    public void setFileExport(File fileExport) {
        this.fileExport = fileExport;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public Map<String, String> getFunctionGruopStatusMap() {
        return functionGruopStatusMap;
    }

    public void setFunctionGruopStatusMap(Map<String, String> functionGruopStatusMap) {
        this.functionGruopStatusMap = functionGruopStatusMap;
    }

    public Map<String, String> getPermissionTypeMap() {
        return permissionTypeMap;
    }

    public void setPermissionTypeMap(Map<String, String> permissionTypeMap) {
        this.permissionTypeMap = permissionTypeMap;
    }

    public List<ActionDTO> getActionDTOList() {
        return actionDTOList;
    }

    public void setActionDTOList(List<ActionDTO> actionDTOList) {
        this.actionDTOList = actionDTOList;
    }


    public List<PermissionGroupDTO> getListPermissionCode() {
        return listPermissionCode;
    }

    public void setListPermissionCode(List<PermissionGroupDTO> listPermissionCode) {
        this.listPermissionCode = listPermissionCode;
    }


}
