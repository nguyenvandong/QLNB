/*
package com.viettel.bankplus.webchain.controller;

import com.google.common.collect.Lists;
import com.viettel.bankplus.webchain.dto.*;
import com.viettel.bankplus.webchain.input.InputLazyChainUserSearch;
import com.viettel.bankplus.webchain.service.*;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.utils.ExcelUtil;
import com.viettel.web.common.controller.BaseController;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.*;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;
import vn.com.viettel.bccs.api.SendSmsWebRequest;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


@ManagedBean
@Component
@Scope("view")
public class EmployeeController extends BaseController {
    private static final long serialVersionUID = 1;
    @Autowired
    private transient ApDomainService apDomainService;
    @Autowired
    private transient ChainUserService chainUserService;
    @Autowired
    private transient ActionService actionService;
    private transient InputLazyChainUserSearch inputLazyChainUserSearch;
    private LazyDataModel<ChainUserDTO> chainUserDTOList;
    private List<ActionDTO> actionDTOList;
    private ChainUserDTO chainUserDTO;
    private List<ApDomainDTO> statusList = new ArrayList<>();

    private String dlgHeader;
    private String dlgConfirmHeader;
    private String dlgConfirmContent;



    @PostConstruct
    public void init() {
        try {
            inputLazyChainUserSearch = new InputLazyChainUserSearch();
            inputLazyChainUserSearch.setParentShopId(BccsLoginSuccessHandler.getShopId());
            statusList = apDomainService.getValueApDomainByTypeOrderByCode(Const.AP_DOMAIN.WC_STATUS);
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.CHAIN_USER_TYPE);
            chainUserDTO = new ChainUserDTO();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }


    @Secured("@")
    public void preAddNewChainUser() {
        try {
            dlgHeader = getText("dlg.webchain.user.add.new.header");
            chainUserDTO = new ChainUserDTO();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().validationFailed();
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    public void genUserNameByChainShopCode() {
        try {
            if(DataUtil.isNullObject(chainUserDTO.getChainShopId())){
                chainUserDTO.setPrefixUsername(null);
                chainUserDTO.setChainShopCode(null);
                chainUserDTO.setChainShopName(null);
                RequestContext.getCurrentInstance().update("dlgChainUser:msgFormDlg");
            }else {
                chainUserDTO.setPrefixUsername(chainUserDTO.getSalt().toLowerCase() + "_");
                RequestContext.getCurrentInstance().update("dlgChainUser:msgFormDlg");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().validationFailed();
            logger.error(e.getMessage(), e);
        }
    }

    @Secured("@")
    public void preEditChainUser(ChainUserDTO chainUserDTOEdit) {
        try {
            chainUserDTO = new ChainUserDTO();
            dlgHeader = getText("dlg.webchain.user.update.header");
            BeanUtils.copyProperties(chainUserDTOEdit, chainUserDTO);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    @Secured("@")
    public void deleteChainUser(ChainUserDTO chainUserDTODelete) {
        try {
            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.CHAIN_USER_TYPE_DELETE));
            actionLogDTO.setDescription(getText(Const.ACTION_LOG.CHAIN_USER_TYPE_DELETE));
            chainUserService.deleteChainUser(chainUserDTODelete.getChainUserId(), actionLogDTO);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("common.delete.msg.success")));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }

    }

    @Secured("@")
    public void doSearch() {
        try {
            chainUserDTOList = findLazyChainUser();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }




    public LazyDataModel<ChainUserDTO> findLazyChainUser() {
        return new LazyDataModel<ChainUserDTO>() {
            @Override
            public List<ChainUserDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(chainUserService.countLazyChainUser(inputLazyChainUserSearch));
                    return chainUserService.findLazyChainUser(inputLazyChainUserSearch, page.getPageSize(), page.getPageNumber(), true);
                } catch (Exception e) {
                    logger.error("findAllLazy", e);
                    return Lists.newArrayList();
                }
            }

            @Override
            public ChainUserDTO getRowData(String rowKey) {
                List<ChainUserDTO> tmp = (List<ChainUserDTO>) getWrappedData();
                Optional<ChainUserDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getChainUserId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(ChainUserDTO object) {
                return object != null ? object.getChainUserId() : null;
            }
        };
    }

    @Secured("@")
    public void doCreateOrUpdate() {
        try {
            if (validateCreateOrUpdate()) {
                ActionLogDTO actionLogDTO = new ActionLogDTO();
                // xac dinh xem no la update hay create
                if (chainUserDTO.getChainUserId() == null) {
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.CHAIN_USER_TYPE_ADD_NEW));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.CHAIN_USER_TYPE_ADD_NEW));
                } else {
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.CHAIN_USER_TYPE_UPDATE));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.CHAIN_USER_TYPE_UPDATE));
                }
                actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                chainUserDTO.setPhoneNumber(DataUtil.formatPhoneNumber(chainUserDTO.getPhoneNumber()));
                chainUserDTO.setUserName(chainUserDTO.getUserName().toLowerCase());
                chainUserService.saveOrUpdate(chainUserDTO, actionLogDTO);
                RequestContext.getCurrentInstance().update("chainUser:msgSearch");
                RequestContext.getCurrentInstance().execute("PF('dlgChainUserForm').hide()");
                chainUserDTO = new ChainUserDTO();
                doSearch();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    private SendSmsWebRequest createSendSMSCustomer(ChainUserDTO chainUserDTOSMS) {
        SendSmsWebRequest sendSmsWebRequest = new SendSmsWebRequest();
        sendSmsWebRequest.setRequestDate(new Date());
        sendSmsWebRequest.setMsisdn(chainUserDTOSMS.getPhoneNumber());
        sendSmsWebRequest.setStaffUsername(BccsLoginSuccessHandler.getUserName());
        sendSmsWebRequest.setProcessCode(Const.WEB_CHAIN.SMS_CREATE_USER);
        sendSmsWebRequest.setStaffUsername(BccsLoginSuccessHandler.getUserName());
        sendSmsWebRequest.setChainName(BccsLoginSuccessHandler.getChainName());
        sendSmsWebRequest.setObjectStaffUsername(chainUserDTOSMS.getPrefixUsername()+ chainUserDTOSMS.getUserName());
        sendSmsWebRequest.setObjectChainCode(BccsLoginSuccessHandler.getChainCode());
        sendSmsWebRequest.setObjectChainName(BccsLoginSuccessHandler.getChainName());
        sendSmsWebRequest.setObjectShopCode(chainUserDTOSMS.getChainShopCode());
        sendSmsWebRequest.setObjectShopName(chainUserDTOSMS.getChainShopName());
        sendSmsWebRequest.setNewPIN(chainUserDTOSMS.getPassword());
        sendSmsWebRequest.setPartnerCode(BccsLoginSuccessHandler.getChainCode());
        return sendSmsWebRequest;
    }



    private boolean validateCreateOrUpdate() throws Exception {
        // cua hang chuoi
        if (DataUtil.isNullObject(chainUserDTO.getChainShopId())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.shop.code.empty")));
            return false;
        } else {
            // chain shop
            ChainShopDTO chainShopDTO = chainShopService.findOne(chainUserDTO.getChainShopId());
            if (null == chainShopDTO || chainShopDTO.getStatus() != 1) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.shop.not.exist")));
                return false;
            }
        }
        //ten user name
        if (DataUtil.isNullObject(chainUserDTO.getUserName())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.name.empty")));
            return false;
        } else if (chainUserDTO.getUserName().length() > 100) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.name.len.100")));
            return false;
        }
        if (chainUserService.checkUserNameExist(chainUserDTO.getChainUserId(), chainUserDTO.getChainUserId() == null ? (chainUserDTO.getPrefixUsername() + chainUserDTO.getUserName()) : chainUserDTO
                .getUserName()))
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.name.exist")));
            return false;
        }
        //fullname

        if (DataUtil.isNullObject(chainUserDTO.getFullName())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.full.name.empty")));
            return false;
        } else if (chainUserDTO.getFullName().length() > 100) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.full.name.len.100")));
            return false;
        }
        // so dien thoai
        String phoneNumber = DataUtil.formatPhoneNumber(chainUserDTO.getPhoneNumber());
        if (DataUtil.isNullOrEmpty(phoneNumber)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.phone.number.empty")));
            return false;
        } else if (!DataUtil.checkPhoneNumber(phoneNumber)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getTextParam("validate.webchain.shop.phone.number.format", phoneNumber + "")));
            return false;
        }

        // mat khau
        if (chainUserDTO.getChainUserId() == null) {
            if (DataUtil.isNullObject(chainUserDTO.getPassword())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.password.empty")));
                return false;
            } else if (!DataUtil.validatePassword(chainUserDTO.getPassword())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.password.format")));
                return false;
            }
            // nhac lai mat khau
            if (DataUtil.isNullObject(chainUserDTO.getRepeatPassword())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.repeat.password.empty")));
                return false;
            } else
                // check trung mat khau
                if (!chainUserDTO.getPassword().equals(chainUserDTO.getRepeatPassword())) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("validate.webchain.user.password.not.match")));
                    return false;
                }
        }
        return true;
    }

    public void preImport() {
        importExcelChainUserDTOList = new ArrayList<>();
        fileData = null;
        uploadedFile = null;
        contentInBytes = null;
        renderResultFileError = false;
        renderTemplateFile = true;
        importExcelSuccessList = new ArrayList<>();
    }

    @Secured("@")
    public void fileUploadAction(FileUploadEvent event) {
        try {
            uploadedFile = event.getFile();
            BaseMessage message = validateFileUploadWhiteList(uploadedFile, ALLOW_EXTENSION_TEMPLATE, Const.WEB_CHAIN.MAX_SIZE_10M);
            if (!message.isSuccess()) {
                fileData = null;
                focusElementByRawCSSSlector(".outputAttachFiledata");
                renderResultFileError = false;
                renderTemplateFile = true;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, message.getDescription()));
                return;
            }
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
            name = fmt.format(new Date()) + uploadedFile.getFileName().substring(uploadedFile.getFileName().lastIndexOf('.'));
            fileData = uploadedFile.getFileName();
            contentInBytes = uploadedFile.getContents();

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

*
     * @param contentInBytes
     **

/**//*

     * @return


    private List<ChainUserDTO> readDataInFile(byte[] contentInBytes) {
        List<ChainUserDTO> importChainUserList = new ArrayList<>();
        importExcelChainUserDTOList = new ArrayList<>();
        ChainUserDTO dto;
        isExceptionHyperLink = false;
        ExcelUtil ex = null;
        try {
            ex = new ExcelUtil(uploadedFile, contentInBytes);
            List<Integer> rowIgnore = new ArrayList<>();
            Sheet sheet = ex.getWorkbook().getSheetAt(0);
            Row xRow;
            Map<String, String> duplicateUsernameMap = new ConcurrentHashMap<>();
//            int totalCol = xRow.getLastCellNum();

            int totalRow = 0;
            for (int row = 1; row <= sheet.getLastRowNum(); row++) {
                xRow = sheet.getRow(row);
                if (totalRow >= 100) {
                    importMaxRecord = true;
                    importExcelChainUserDTOList.addAll(0, importChainUserList);
                    return new ArrayList();
                }
                if (xRow != null && xRow.getFirstCellNum() < 9) {
                    dto = new ChainUserDTO();
                    String chainShopCode = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(1)))) {
                        chainShopCode = ExcelUtil.getStringValue(xRow.getCell(1)).trim().toUpperCase();
                    }
                    String username = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(2)))) {
                        username = ExcelUtil.getStringValue(xRow.getCell(2)).trim().toLowerCase();
                    }
                    String fullName = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(3)))) {
                        fullName = ExcelUtil.getStringValue(xRow.getCell(3)).trim();
                    }
                    String phoneNumber = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(4)))) {
                        phoneNumber = ExcelUtil.getStringValue(xRow.getCell(4)).trim();
                    }
                    String password = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(5)))) {
                        password = ExcelUtil.getStringValue(xRow.getCell(5)).trim();
                    }
                    String status = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(6)))) {
                        status = ExcelUtil.getStringValue(xRow.getCell(6)).trim();
                    }
                    if (DataUtil.isNullOrEmpty(chainShopCode) && DataUtil.isNullOrEmpty(username) && DataUtil.isNullOrEmpty(fullName) && DataUtil.isNullOrEmpty(phoneNumber)
                            && DataUtil.isNullOrEmpty(password) && DataUtil.isNullOrEmpty(status)) {
                        rowIgnore.add(row);
                        continue;
                    }
                    totalRow++;
                    String result = validateChainUser(dto, duplicateUsernameMap, chainShopCode, username, fullName, phoneNumber, password, status);
                    if (!DataUtil.isNullOrEmpty(result)) {
                        dto.setImportExcelError(Const.WEB_CHAIN.IMPORT_ERROR + result);
                        importExcelChainUserDTOList.add(dto);
                    } else {
                        dto.setPhoneNumber(DataUtil.formatPhoneNumber(dto.getPhoneNumber()));
                        dto.setImportExcelError(null);
                        importChainUserList.add(dto);
                    }

                }
            }
        } catch (Exception e) {
            if (ex == null) {
                FacesContext context = FacesContext.getCurrentInstance();
                Iterator<FacesMessage> it = context.getMessages();
                while (it.hasNext()) {
                    it.next();
                    it.remove();
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.file.hyperLink")));
                isExceptionHyperLink = true;
                return new ArrayList();
            }
            dto = new ChainUserDTO();
            dto.setImportExcelError(Const.WEB_CHAIN.IMPORT_ERROR + "." + getText("common.error.happened.againAction"));
            importExcelChainUserDTOList.add(dto);
            logger.error(e.getMessage(), e);

        }
        return importChainUserList;
    }

*
     * @param dto
     * @param duplicateUsernameMap
     * @param chainShopCode
     * @param username
     * @param fullName
     * @param phoneNumber
     * @param password
     * @param status
     *
     * @return
     *
     * @throws Exception


    private String validateChainUser(ChainUserDTO dto, Map<String, String> duplicateUsernameMap, String chainShopCode, String username, String
            fullName, String phoneNumber, String password, String status) throws Exception {
        StringBuilder result = new StringBuilder();
        // check shop code
        if (DataUtil.isNullOrEmpty(chainShopCode)) {
            result.append(". " + getText("validate.webchain.user.shop.code.empty"));
        } else {
            dto.setChainShopCode(chainShopCode);
            if (chainShopCode.length() > Const.CHAIN_SHOP.MAXLENGTH_CODE) {
                result.append(". " + getText("validate.webchain.user.shop.code.len.100"));
            } else if (!DataUtil.isCheckSpecialCharacter(chainShopCode)) {
                result.append(". " + getText("validate.webchain.user.shop.code.format"));
            } else {
                if (!chainShopService.checkShopCodeExist(BccsLoginSuccessHandler.getShopId(), null, chainShopCode)) {
                    result.append(". " + getText("validate.webchain.user.import.shop.code.not.exist"));
                } else {
                    ChainShopDTO chainShopDTO = chainShopService.findByChainShopCode(chainShopCode);
                    if (null != chainShopDTO) {
                        dto.setChainShopId(chainShopDTO.getChainShopId());
                    } else {
                        result.append(". " + getText("validate.webchain.user.import.shop.code.not.exist"));
                    }
                }
            }
        }
        // check username
        if (DataUtil.isNullOrEmpty(username)) {
            result.append(". " + getText("validate.webchain.user.name.empty"));
        } else {
            dto.setUserName(username);
            if (username.length() > Const.CHAIN_USER.MAXLENGTH_USER_NAME) {
                result.append(". " + getText("validate.webchain.user.name.len.100"));
            } else if (!DataUtil.isCheckSpecialCharacter(username)) {
                result.append(". " + getText("validate.webchain.user.name.format"));
            } else if (chainShopCode == null || !username.startsWith(chainShopCode.toLowerCase())) {
                result.append(". " + getText("validate.webchain.user.name.format"));
            } else if (chainShopCode == null || username.equalsIgnoreCase(chainShopCode + "_")) {
                result.append(". " + getText("validate.webchain.user.name.format"));
            } else if (chainUserService.checkUserNameExist(null, username)) {
                result.append(". " + getText("validate.webchain.user.name.exist"));
            }

        }
        // check fullName
        if (DataUtil.isNullOrEmpty(fullName)) {
            result.append(". " + getText("validate.webchain.user.full.name.empty"));
        } else {
            dto.setFullName(fullName);
            if (fullName.length() > Const.CHAIN_USER.MAXLENGTH_FULL_NAME) {
                result.append(". " + getText("validate.webchain.user.full.name.len.100"));

            }
        }
        // check phone number
        if (DataUtil.isNullOrEmpty(phoneNumber)) {
            result.append(". " + getText("validate.webchain.user.phone.number.empty"));
        } else {
            phoneNumber = DataUtil.formatPhoneNumber(phoneNumber);
            if (!DataUtil.checkPhoneNumber(phoneNumber)) {
                dto.setPhoneNumber(phoneNumber);
                result.append(". " + getText("validate.webchain.user.phone.number.format", phoneNumber + ""));
            } else {
                dto.setPhoneNumber(phoneNumber);
            }
        }
        // check password
        if (DataUtil.isNullOrEmpty(password)) {
            result.append(". " + getText("validate.webchain.user.password.empty"));
        } else {
            dto.setPassword(password);
            if (password.length() > Const.CHAIN_USER.MAXLENGTH_PASSWORD) {
                result.append(". " + getText("validate.webchain.user.password.len.50"));
            } else if (!DataUtil.validatePassword(password)) {
                result.append(". " + getText("validate.webchain.user.password.format"));
            }
        }

        // check status
        if (DataUtil.isNullOrEmpty(status)) {
            dto.setStatus(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        } else {
            try {
                if (!"1".equals(status.trim()) && !"0".equals(status.trim())) {
                    dto.setStatus(Short.valueOf(status));
                    result.append(". " + getText("validate.webchain.user.import.status.format"));
                } else {
                    dto.setStatus(Short.valueOf(status));
                }
            } catch (Exception e) {
                result.append(". " + getText("validate.webchain.user.import.status.format"));
                logger.error(e.getMessage(), e);
            }
        }
        // Check trung ten
        if (DataUtil.isNullOrEmpty(result.toString()) && DataUtil.isNullOrEmpty(username)) {
            String tempName = duplicateUsernameMap.get(username);
            if (null != tempName) {
                result.append(". " + getText("validate.webchain.user.import.name.file.exist"));
            } else {
                duplicateUsernameMap.put(username.toLowerCase(), username.toLowerCase());
            }
        }

        return result.toString();
    }

*
     *



    public void importDataByExcel() {
        try {
            importMaxRecord = false;
            importExcelSuccessList = new ArrayList<>();
            importExcelChainUserDTOList = new ArrayList<>();
            BaseMessage message = validateFileUploadWhiteList(uploadedFile, ALLOW_EXTENSION_TEMPLATE, Const.WEB_CHAIN.MAX_SIZE_5M);
            if (!message.isSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, message.getDescription()));
                contentInBytes = null;
                fileData = null;
                renderResultFileError = false;
                renderTemplateFile = true;
                return;
            }
            // check file mau template
            if (!checkFileTemplate(contentInBytes)) {
                renderResultFileError = false;
                renderTemplateFile = true;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.template")));
                return;
            }
            if (contentInBytes == null || DataUtil.isNullOrEmpty(fileData)) {
                renderResultFileError = false;
                renderTemplateFile = true;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.empty")));
                return;
            }
            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.CHAIN_USER_TYPE_IMPORT));
            actionLogDTO.setDescription(getText(Const.ACTION_LOG.CHAIN_USER_TYPE_IMPORT));
            importExcelSuccessList = readDataInFile(contentInBytes);
            if (isExceptionHyperLink) {
                renderResultFileError = false;
                renderTemplateFile = true;
                return;
            }
            if (!DataUtil.isNullOrEmpty(importExcelSuccessList)) {
                List<ChainUserDTO> listResult = chainUserService.importChainUserTemplate(importExcelSuccessList, actionLogDTO);
                renderResultFileError = true;
                renderTemplateFile = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.user.import.successful")));
                importExcelChainUserDTOList.addAll(0, listResult);
                importExcelSuccessList.clear();
                fileData = null;
                uploadedFile = null;
            } else if (DataUtil.isNullOrEmpty(importExcelChainUserDTOList)) {
                renderResultFileError = false;
                renderTemplateFile = true;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.empty")));
            } else {
                fileData = null;
                uploadedFile = null;
                if (importMaxRecord) {
                    renderResultFileError = false;
                    renderTemplateFile = true;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.file.max.record")));
                } else {
                    renderResultFileError = true;
                    renderTemplateFile = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.user.import.successful")));
                }
                importMaxRecord = false;

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

*
     * @param contentByte
     *
     * @return



    private boolean checkFileTemplate(byte[] contentByte) {
        try {
            ExcelUtil ex = new ExcelUtil(uploadedFile, contentByte);
            Sheet sheet = ex.getWorkbook().getSheetAt(0);
            Row row2 = sheet.getRow(0);
            if (null == row2)
                return false;
            int totalCol = row2.getLastCellNum();
            if (totalCol < 7)
                return false;
            // shop code
            String chainShopCode = ExcelUtil.getStringValue(row2.getCell(1)).trim();
            if (!getText("webchain.user.shop.code").equalsIgnoreCase(chainShopCode.trim())) {
                return false;
            }
            // user name
            String username = ExcelUtil.getStringValue(row2.getCell(2)).trim();
            if (!getText("webchain.user.user.name").equalsIgnoreCase(username)) {
                return false;
            }
            // full name
            String fullName = ExcelUtil.getStringValue(row2.getCell(3)).trim();
            if (!getText("webchain.user.full.name").equalsIgnoreCase(fullName)) {
                return false;
            }
            //phone_number
            String phoneNumber = ExcelUtil.getStringValue(row2.getCell(4)).trim();
            if (!getText("webchain.user.phone.number").equalsIgnoreCase(phoneNumber)) {
                return false;
            }
            //password
            String password = ExcelUtil.getStringValue(row2.getCell(5)).trim();
            if (!getText("webchain.user.password").equalsIgnoreCase(password)) {
                return false;
            }
            //status
            String status = ExcelUtil.getStringValue(row2.getCell(6)).trim();
            if (!getText("webchain.user.status").equalsIgnoreCase(status)) {
                return false;
            }

            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
        return true;
    }


    @Secured("@")
    public void downloadFileResult() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            InputStream createStream = Faces.getResourceAsStream(Const.RESOURCE_TEMPLATE.RESOURCE_TEMPLATE_PATH + Const.CHAIN_USER.FILE_NAME_RESULT + ".xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(createStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            XSSFCellStyle style = workbook.createCellStyle();
            style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style.setWrapText(true);
            for (int i = 0; i < importExcelChainUserDTOList.size(); i++) {
                ChainUserDTO chainUserDTO = importExcelChainUserDTOList.get(i);
                sheet.setAutobreaks(true);
                XSSFRow row = sheet.createRow(i + 1);
                //STT
                XSSFCell cell00 = row.createCell(0);
                cell00.setCellStyle(style);
                cell00.setCellValue((i + 1));
                //chain shop code
                XSSFCell cell01 = row.createCell(1);
                cell01.setCellStyle(style);
                cell01.setCellValue(chainUserDTO.getChainShopCode());
                //chain username
                XSSFCell cell02 = row.createCell(2);
                cell02.setCellStyle(style);
                cell02.setCellValue(chainUserDTO.getUserName());
                //chain fullName
                XSSFCell cell03 = row.createCell(3);
                cell03.setCellStyle(style);
                cell03.setCellValue(chainUserDTO.getFullName());
                // phone number
                XSSFCell cell04 = row.createCell(4);
                cell04.setCellStyle(style);
                cell04.setCellValue(chainUserDTO.getPhoneNumber());
                // password
                XSSFCell cell05 = row.createCell(5);
                cell05.setCellStyle(style);
                cell05.setCellValue(chainUserDTO.getPassword());
                // status
                XSSFCell cell06 = row.createCell(6);
                cell06.setCellStyle(style);
                String status = chainUserDTO.getStatus() == null ? "" : chainUserDTO.getStatus().toString();
                cell06.setCellValue(status);
                // ket qua import
                XSSFCell cell07 = row.createCell(7);
                cell07.setCellStyle(style);
                String result = chainUserDTO.getImportExcelError();
                if ("0".equals(result)) {
                    cell07.setCellValue(getText("webchain.user.import.notOK"));
                } else if ("1".equals(result)) {
                    cell07.setCellValue(getText("webchain.user.import.OK"));
                } else {
                    cell07.setCellValue(result);
                }
            }
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + Const.CHAIN_USER.FILE_NAME_RESULT + "_" + sdf.format(new Date()) + ".xlsx");
            externalContext.setResponseContentType("application/excel");
            workbook.write(externalContext.getResponseOutputStream());
            facesContext.responseComplete();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    @Secured("@")
    public void downloadFileTemplate() {
        try {
            InputStream createStream = Faces.getResourceAsStream(Const.RESOURCE_TEMPLATE.RESOURCE_TEMPLATE_PATH + Const.CHAIN_USER.FILE_NAME);
            XSSFWorkbook workbook = new XSSFWorkbook(createStream);
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + Const.CHAIN_USER.FILE_NAME + "\"");
            externalContext.setResponseContentType("application/excel");
            workbook.write(externalContext.getResponseOutputStream());
            facesContext.responseComplete();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

*
     * @param shopCode
     *
     * @return


    public List<ChainShopDTO> searchChainShopAutoComplete(String shopCode) {
        try {
            shopCode = (shopCode == null ? "" : shopCode.trim());
            inputLazyChainUserSearch.setShopCode(shopCode);
            chainShopDTOSearchList = new ArrayList<>();
            chainShopDTOSearchList = chainShopService.getChainShopDTOListByShopCode(shopCode, inputLazyChainUserSearch.getParentShopId(), 1L);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
        return chainShopDTOSearchList;
    }

    public void selectChainShop(ChainShopDTO chainShopDTO) {
        try {
            inputLazyChainUserSearch.setShopCode(chainShopDTO.getChainShopCode());
            inputLazyChainUserSearch.setChainShopId(chainShopDTO.getChainShopId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

*
     * reset auto complete


    public void resetAutoCompleteChainShop() {
        inputLazyChainUserSearch.setShopCode(null);
        inputLazyChainUserSearch.setChainShopId(null);
        chainShopDTOSearchResult = new ChainShopDTO();
        chainShopDTOSearchList = new ArrayList<>();
    }

    public ChainShopService getChainShopService() {
        return chainShopService;
    }

    public void setChainShopService(ChainShopService chainShopService) {
        this.chainShopService = chainShopService;
    }


    public List<ApDomainDTO> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<ApDomainDTO> statusList) {
        this.statusList = statusList;
    }


    public String getDlgHeader() {
        return dlgHeader;
    }

    public void setDlgHeader(String dlgHeader) {
        this.dlgHeader = dlgHeader;
    }

    public String getDlgConfirmHeader() {
        return dlgConfirmHeader;
    }

    public void setDlgConfirmHeader(String dlgConfirmHeader) {
        this.dlgConfirmHeader = dlgConfirmHeader;
    }

    public String getDlgConfirmContent() {
        return dlgConfirmContent;
    }

    public void setDlgConfirmContent(String dlgConfirmContent) {
        this.dlgConfirmContent = dlgConfirmContent;
    }

    public InputLazyChainUserSearch getInputLazyChainUserSearch() {
        return inputLazyChainUserSearch;
    }

    public void setInputLazyChainUserSearch(InputLazyChainUserSearch inputLazyChainUserSearch) {
        this.inputLazyChainUserSearch = inputLazyChainUserSearch;
    }

    public LazyDataModel<ChainUserDTO> getChainUserDTOList() {
        return chainUserDTOList;
    }

    public void setChainUserDTOList(LazyDataModel<ChainUserDTO> chainUserDTOList) {
        this.chainUserDTOList = chainUserDTOList;
    }

    public List<ChainShopDTO> getChainShopDTOList() {
        return chainShopDTOList;
    }

    public void setChainShopDTOList(List<ChainShopDTO> chainShopDTOList) {
        this.chainShopDTOList = chainShopDTOList;
    }

    public ChainUserDTO getChainUserDTO() {
        return chainUserDTO;
    }

    public void setChainUserDTO(ChainUserDTO chainUserDTO) {
        this.chainUserDTO = chainUserDTO;
    }

    public List<ActionDTO> getActionDTOList() {
        return actionDTOList;
    }

    public void setActionDTOList(List<ActionDTO> actionDTOList) {
        this.actionDTOList = actionDTOList;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public boolean isUpdateFile() {
        return updateFile;
    }

    public void setUpdateFile(boolean updateFile) {
        this.updateFile = updateFile;
    }

    public boolean isShowContractDetail() {
        return showContractDetail;
    }

    public void setShowContractDetail(boolean showContractDetail) {
        this.showContractDetail = showContractDetail;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContentInBytes() {
        return contentInBytes;
    }

    public void setContentInBytes(byte[] contentInBytes) {
        this.contentInBytes = contentInBytes;
    }


    public StreamedContent getFileView() {
        return fileView;
    }

    public void setFileView(StreamedContent fileView) {
        this.fileView = fileView;
    }

    public boolean isRenderResultFileError() {
        return renderResultFileError;
    }

    public void setRenderResultFileError(boolean renderResultFileError) {
        this.renderResultFileError = renderResultFileError;
    }

    public boolean isRenderTemplateFile() {
        return renderTemplateFile;
    }

    public void setRenderTemplateFile(boolean renderTemplateFile) {
        this.renderTemplateFile = renderTemplateFile;
    }

    public List<ChainUserDTO> getImportExcelChainUserDTOList() {
        return importExcelChainUserDTOList;
    }

    public void setImportExcelChainUserDTOList(List<ChainUserDTO> importExcelChainUserDTOList) {
        this.importExcelChainUserDTOList = importExcelChainUserDTOList;
    }

    public List<ChainUserDTO> getImportExcelSuccessList() {
        return importExcelSuccessList;
    }

    public void setImportExcelSuccessList(List<ChainUserDTO> importExcelSuccessList) {
        this.importExcelSuccessList = importExcelSuccessList;
    }

    public ChainShopDTO getChainShopDTOSearchResult() {
        return chainShopDTOSearchResult;
    }

    public void setChainShopDTOSearchResult(ChainShopDTO chainShopDTOSearchResult) {
        this.chainShopDTOSearchResult = chainShopDTOSearchResult;
    }

    public List<ChainShopDTO> getChainShopDTOSearchList() {
        return chainShopDTOSearchList;
    }

    public void setChainShopDTOSearchList(List<ChainShopDTO> chainShopDTOSearchList) {
        this.chainShopDTOSearchList = chainShopDTOSearchList;
    }
}
*/
