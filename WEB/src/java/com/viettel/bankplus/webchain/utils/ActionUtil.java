package com.viettel.bankplus.webchain.utils;

import com.viettel.bankplus.webchain.dto.ActionDTO;

import java.util.List;

/**
 * @author hungnq on 9/9/2017
 */
public class ActionUtil {

    public static Long getActionIdByCode(List<ActionDTO> list, String type) {
        if (null == list || list.isEmpty()) {
            return null;
        } else if (null == type || "".equals(type.trim())) {
            return null;
        } else {
            for (ActionDTO actionDTO : list) {
                if (type.equalsIgnoreCase(actionDTO.getCode())) {
                    return actionDTO.getActionId();
                }
            }
        }
        return null;
    }
}
