package com.viettel.bankplus.webchain.utils;

import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TungHuynh Created by sondt18 on 9/11/2017.
 */
public class StringUtils {
    public enum OPERATOR {
        GMOBILE("LST_GMOBILE"),
        VIETTEL("LIST_VIETTEL_PRE_NUMBER"),
        VIETTELDIDONG("LIST_VIETTEL_DI_DONG"),
        VINAPHONE("LST_VINAPHONE"),
        MOBIFONE("LST_MOBIFONE"),
        VIETNAMMOBILE("LST_VIETNAMMOBILE");

        private String code;

        OPERATOR(String code) {
            this.code = code;
        }

        public static OPERATOR getByCode(String code) {
            if (code != null) {
                for (OPERATOR op : OPERATOR.values()) {
                    if (code.equals(op.getCode())) {
                        return op;
                    }
                }
            }
            return null;
        }

        public String getCode() {
            return code;
        }
    }

    private static Map<String, String> prefix = new HashMap<>();

    static {
        prefix.put("LST_GMOBILE", "84993,84994,84995,84996,84199,84997,0993,0994,0995,0996,0199,0997,993,994,995,996,199,997");
        prefix.put("LIST_VIETTEL_PRE_NUMBER", "8496|8497|8498|84162|84163|84164|84165|84166|84167|84168|84169|096|097|098|0162|0163|0164|0165|0166|0167|0168|0169|96|97|98|162|163|164|165|166|167|168" +
                "|169|84868|0868|868|086|86|8486|43|44|46|043|046|8443|8446|844|043|04|046|466|436|84466");
        /*Đai*/
        /*prefix.put("LIST_VIETTEL_HOME_PHONE", "84868|0868|868|086|86|8486|43|44|46|043|046|8443|8446|844|043|04|046|466|436|84466");*/
        prefix.put("LIST_VIETTEL_DI_DONG", "8496|8497|8498|84162|84163|84164|84165|84166|84167|84168|84169|096|097|098|0162|0163|0164|0165|0166|0167|0168|0169|96|97|98|162|163|164|165|166|167|168" +
                "|169");
       /* Đại END*/
        prefix.put("LST_VINAPHONE", "8491,8494,84123,84124,84125,84127,84129,8488,091,094,0123,0124,0125,0127,0129,088,91,94,123,124,125,127,129,88");
        prefix.put("LST_MOBIFONE", "8490,8493,84120,84121,84122,84126,84128,8489,090,093,0120,0121,0122,0126,0128,089,90,93,120,121,122,126,128,89");
        prefix.put("LST_VIETNAMMOBILE", "8492,84188,84186,092,0188,0186,92,188,186");
    }

    public static String checkMsisdnOperator(String msisdn) {
        for (Map.Entry<String, String> item : prefix.entrySet()) {
            String lst = item.getValue().replaceAll(",", "|").replaceAll("\\s+", "");
            String pattern = "(" + lst + ")" + "(\\d{7}|\\d{8})";
            Pattern pt = Pattern.compile(pattern);
            Matcher matcher = pt.matcher(msisdn);
            if (matcher.matches()) {
                return OPERATOR.getByCode(item.getKey()).toString();
            }
        }
        return null;
    }


    public static boolean stringIsNullOrEmty(String str) {
        if (str == null)
            return true;
        else {
            if (str.trim().length() <= 0)
                return true;
        }
        return false;
    }

    public static boolean stringIsNullOrEmty(Object str) {
        if (str == null)
            return true;
        else {
            if (str.toString().trim().length() <= 0)
                return true;
        }
        return false;
    }

    public static String trimPhoneNumber(String msisdn) {
        if (StringUtils.stringIsNullOrEmty(msisdn)) {
            return "";
        } else {
            msisdn = msisdn.trim();
            if (msisdn.startsWith("0")) {
                msisdn = msisdn.substring(1);
            } else if (msisdn.startsWith("84")) {
                msisdn = msisdn.substring(2);
            } else if (msisdn.startsWith("+84")) {
                msisdn = msisdn.substring(3);
            }
        }
        return msisdn;
    }
}
