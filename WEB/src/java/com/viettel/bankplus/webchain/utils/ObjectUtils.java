package com.viettel.bankplus.webchain.utils;

import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import vn.com.viettel.bccs.api.CommonRequest;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * TungHuynh Created by sondt18 on 9/16/2017.
 */
public class ObjectUtils {
    public static CommonRequest assignCommonRequest(HttpSession session) {
        CommonRequest commonRequest = new CommonRequest();
        commonRequest.setRequestDate(new Date());
        //Rxxx hard-code user/pass API
        commonRequest.setClientId("alice");
        if (session!=null) {
            commonRequest.setStaffClientId(BccsLoginSuccessHandler.getUserName());
            commonRequest.setShopCode(BccsLoginSuccessHandler.getShopCode());
            commonRequest.setPartnerCode(BccsLoginSuccessHandler.getChainCode());
        }
        return commonRequest;
    }
}
