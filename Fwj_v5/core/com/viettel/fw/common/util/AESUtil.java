package com.viettel.fw.common.util;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AESUtil {
    public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";

    public static final int HASH_BYTES = 128;
    public static final int PBKDF2_ITERATIONS = 1000;

    /**
     * Returns a salted PBKDF2 hash of the password.
     *
     * @param password the password to hash
     * @return a salted PBKDF2 hash of the password
     */
    public static String createHash(String password, String salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        return createHash(password.toCharArray(), salt.getBytes());
    }

    /**
     * Returns a salted PBKDF2 hash of the password.
     *
     * @param password the password to hash
     * @return a salted PBKDF2 hash of the password
     */
    public static String createHash(char[] password, byte[] salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Hash the password
        byte[] hash = pbkdf2(password, salt, PBKDF2_ITERATIONS, HASH_BYTES);
        // format iterations:salt:hash
        return Hex.encodeHexString(hash);
    }

    /**
     * Validates a password using a hash.
     *
     * @param password the password to check
     * @param goodHash the hash of the valid password
     * @return true if the password is correct, false if not
     */
    public static boolean validatePassword(String password, String salt, String goodHash)
            throws NoSuchAlgorithmException, InvalidKeySpecException, DecoderException {
        // Decode the hash into its parameters
        byte[] saltBytes = salt.getBytes();
        byte[] hash = Hex.decodeHex(goodHash.toCharArray());
        // Compute the hash of the provided password, using the same salt,
        // iteration count, and hash length
        byte[] testHash = pbkdf2(password.toCharArray(), saltBytes, PBKDF2_ITERATIONS, hash.length);
        // Compare the hashes in constant time. The password is correct if
        // both hashes match.
        return slowEquals(hash, testHash);
    }

    /**
     * Compares two byte arrays in length-constant time. This comparison method is
     * used so that password hashes cannot be extracted from an on-line system using
     * a timing attack and then attacked off-line.
     *
     * @param a the first byte array
     * @param b the second byte array
     * @return true if both byte arrays are the same, false if not
     */
    private static boolean slowEquals(byte[] a, byte[] b) {
        int diff = a.length ^ b.length;
        for (int i = 0; i < a.length && i < b.length; i++)
            diff |= a[i] ^ b[i];
        return diff == 0;
    }

    /**
     * Computes the PBKDF2 hash of a password.
     *
     * @param password   the password to hash.
     * @param salt       the salt
     * @param iterations the iteration count (slowness factor)
     * @param bytes      the length of the hash to compute in bytes
     * @return the PBDKF2 hash of the password
     */
    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
        return skf.generateSecret(spec).getEncoded();
    }

//    /**
//     * Tests the basic functionality of the PasswordHash class
//     *
//     * @param args ignored
//     */
//    public static void main(String[] args) {
//        try {
////            String salt = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//            String salt ="20170905174159";
//            System.out.println(salt);
//            String password = "Sonat@123";
//            // Print out 10 hashes
//            System.out.println(AESUtil.createHash(password, salt));
//            // Test password validation
//            String password2 = password + "1";
//            String hash = createHash(password, salt);
//            System.out.println(" here " + validatePassword(password, salt, hash));
//            if (validatePassword(password, salt, hash)) {
//                System.out.println("TESTS PASSED!");
//
//            } else {
//                System.out.println("FAILURE 1: GOOD PASSWORD NOT ACCEPTED!");
//            }
//            if (validatePassword(password2, salt, hash)) {
//                System.out.println("TESTS PASSED!");
//
//            } else
//                System.out.println("FAILURE 2: GOOD PASSWORD NOT ACCEPTED!");
//        } catch (Exception ex) {
//            System.out.println("ERROR: " + ex);
//        }
//    }
}