package com.viettel.ws.provider;

/**
 * @author hungnq on 11/21/2017
 */
public class WsCoreSystem {
    private String apiUrl;
    private String apiTimeout;


    public WsCoreSystem() {
    }

    public WsCoreSystem(String apiUrl, String apiTimeout) {
        this.apiUrl = apiUrl;
        this.apiTimeout = apiTimeout;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiTimeout() {
        return apiTimeout;
    }

    public void setApiTimeout(String apiTimeout) {
        this.apiTimeout = apiTimeout;
    }
}
