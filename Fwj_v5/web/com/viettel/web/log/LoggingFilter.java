package com.viettel.web.log;

import akka.actor.ActorSystem;
import com.viettel.fw.SystemConfig;
import com.viettel.fw.common.util.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.log.LogCollectorActor;
import org.apache.commons.lang3.RandomStringUtils;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by thiendn1 on 4/12/2015.
 */

public class LoggingFilter extends OncePerRequestFilter {

    @Autowired(required = false)
    ActorSystem actorSystem;

    @Autowired
    SystemConfig systemConfig;

    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException {
        String sessionId = req.getSession().getId();
        String callId = RandomStringUtils.randomNumeric(15);
        req.setAttribute(Const.CALL_ID_TOKEN, callId);
        req.setAttribute(Const.KPI_ID_TOKEN, 0);
        chain.doFilter(req, res);
        if (actorSystem != null) {
            if(systemConfig.IS_RUN_SYS_LOG){
                actorSystem.actorSelection("user/" + Const.LOGGING_COLLECTOR_ACTOR + sessionId)
                        .tell(new LogCollectorActor.ANALYTIC(callId), null);
            }
            if(systemConfig.IS_RUN_KPI_LOG){
                String kpiHeader = req.getHeader("VTS-KPIID");
                if (DataUtil.isNullOrEmpty(kpiHeader)) {
                    return;
                }
                actorSystem.actorSelection("user/" + Const.LOGGING_COLLECTOR_ACTOR + sessionId)
                        .tell(new LogCollectorActor.ANALYTIC_KPI(callId), null);
            }
        }

    }
}
