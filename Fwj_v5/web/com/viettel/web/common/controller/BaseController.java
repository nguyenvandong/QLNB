package com.viettel.web.common.controller;


import com.viettel.fw.Exception.LogicException;
import com.viettel.fw.common.util.BundleUtil;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.fw.log.LogNotify;
import com.viettel.fw.passport.CustomConnector;
import com.viettel.web.common.LocateBean;
import com.viettel.web.log.ReportLogService;
import com.viettel.ws.common.utils.Locate;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import viettel.passport.client.UserToken;

import javax.annotation.Nonnull;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class BaseController implements Serializable {

    public static final Logger logger = Logger.getLogger(BaseController.class);


    @Autowired(required = false)
    transient private ReportLogService reportLogService;
    @Autowired
    transient private ApplicationContext context;
    @Autowired
    protected LocateBean locateBean;

    public String getText(String key) {
        return BundleUtil.getText(new Locate(locateBean.getSelectedLocale().getLanguage(),
                locateBean.getSelectedLocale().getCountry()), key);
    }

    public String getText(String key, Object... param) {
        if (param.length > 0) {
            return String.format(getText(key), param);
        }
        return getText(key);
    }

    public void redirectLogout(HttpServletRequest request) throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect(request.getContextPath() + "/logout");
    }

    protected FacesContext getContext() {
        return FacesContext.getCurrentInstance();
    }

    protected Map getRequestMap() {
        return getContext().getExternalContext().getRequestMap();
    }

    protected HttpSession getSession() {
        return getRequest().getSession();
    }

    protected HttpServletResponse getResponse() {
        return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
    }

    protected Object evaluateEL(String elExpression, Class beanClazz) {
        return getContext().getApplication().evaluateExpressionGet(getContext(), elExpression, beanClazz);
    }

    public String convertNumberUsingCurrentLocate(Number number) {
        return DataUtil.convertNumberUsingCurrentLocale(number);
    }

    public String getTextParam(String key, String... params) {
        return MessageFormat.format(getText(key), params);
    }


    private void baseReportSuccess(String displayArea, String msg) {
        try {
            if (reportLogService != null) {
                reportLogService.successMessage(new LogNotify(msg, null));
            }
            BundleUtil.sendSuccessMsg(displayArea, "", msg);
        } catch (Exception ex) {
            logger.error("", ex);
        }
    }

    private void baseReportError(String displayArea, String errorMsg, String logMsg, Exception exc) {
        try {
            if (reportLogService != null) {
                reportLogService.errorMessage(new LogNotify(logMsg, exc));
            }

            String keyMsg;
            if (exc instanceof LogicException) {
                keyMsg = ((LogicException) exc).getErrorCode();
            } else {
                keyMsg = exc.getMessage();
            }

            BundleUtil.sendErrorMsg(displayArea, keyMsg, errorMsg);
            FacesContext.getCurrentInstance().validationFailed();
        } catch (Exception ex) {
            logger.error("", ex);
        }
    }
    
    public String getStrDiscount(Float discount) {
        if(DataUtil.safeEqual(DataUtil.safeToFloat(String.format("%.0f", discount)),discount)){
            return String.format("%.0f", discount);
        }else {
            return (DataUtil.safeToString(discount)).replaceAll("\\.",",");
        }
    }

    public void showSuccessMessage(String telcoServiceCode, String mobile, int amount, Float discountPercent, Date transDate) {
        NumberFormat format = new DecimalFormat("###,###");
        showMess(0, getText("success.msg"));
        showMess(0, getText("label.operator") + ": " + telcoServiceCode);
        showMess(0, getText("label.transmsisdn") + ": " + mobile);
        showMess(0, getText("label.amount") + ": " + format.format(amount).replaceAll(",", ".") + " " + getText("label.vnd"));
        showMess(0, getText("label.discount") + ": " + discountPercent + "%\n");
        showMess(0, getText("label.cpamount") + ": " + format.format(amount * (100 - discountPercent) / 100).replaceAll(",", ".") + " " + getText("label.vnd"));
        showMess(0, getText("label.transdate") + " " + DataUtil.dateToString(transDate, DataUtil.FORMAT_DATE_TIME2));
    }

    public void showSuccessGameCardMessage(String gameProviderName, String mobile, int amount, Float discountPercent, Date transDate) {
        NumberFormat format = new DecimalFormat("###,###");
        showMess(0, getText("success.msg"));
        showMess(0, getText("label.game.provider") + ": " + gameProviderName);
        showMess(0, getText("label.transmsisdn") + ": " + mobile);
        showMess(0, getText("label.amount") + ": " + format.format(amount).replaceAll(",", ".") + " " + getText("label.vnd"));
        showMess(0, getText("label.discount") + ": " + discountPercent + "%\n");
        showMess(0, getText("label.cpamount") + ": " + format.format(amount * (100 - discountPercent) / 100).replaceAll(",", ".") + " " + getText("label.vnd"));
        showMess(0, getText("label.transdate") + " " + DataUtil.dateToString(transDate, DataUtil.FORMAT_DATE_TIME2));
    }


    public void showMess(int type, String content) {
        if (type == 0) {
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, content));
        } else {
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, content));
        }
    }

    public void reportSuccess(String displayArea, String keyMsg, Object... params) {
        String msg = MessageFormat.format(getText(keyMsg), params);
        baseReportSuccess(displayArea, msg);
    }

    public void reportSuccess(String displayArea, String keyMsg) {
        baseReportSuccess(displayArea, getText(keyMsg));
    }

    public void reportError(String displayArea, String errCode, String keyMsg) {
        String errorMsg = getText(keyMsg);
        baseReportError(displayArea, errorMsg, getMsgByKey(keyMsg), new LogicException(errCode, keyMsg));

    }

    public void reportError(String displayArea, String keyMsg, Exception exc) {
        baseReportError(displayArea, getText(keyMsg), getMsgByKey(keyMsg), exc);
    }

    public void reportErrorMsg(String displayArea, String errorCode, String msg) {
        baseReportError(displayArea, msg, msg, new LogicException(errorCode, msg));
    }

    private String getErrorMsg(@Nonnull BaseMessage baseMessage) {
        return getErrorMsg(baseMessage.getErrorCode(), baseMessage.getKeyMsg(), baseMessage.getParamsMsg());
    }

    private String getErrorMsg(String errorCode, String keyMsg, Object... params) {
        String msg = "";
        if (!DataUtil.isNullOrEmpty(keyMsg)) {
            if (DataUtil.isNullOrEmpty(params)) {
                msg = getMsgByKey(keyMsg);
            } else {
                msg = MessageFormat.format(getMsgByKey(keyMsg), params);
            }
        }

        String errorMsg;
        if (!DataUtil.isNullOrEmpty(errorCode)) {
            errorMsg = errorCode + ": " + msg;
        } else {
            errorMsg = msg;
        }
        return errorMsg;
    }

    public void reportError(String displayArea, @Nonnull BaseMessage baseMessage) {
        String errorMsg = getErrorMsg(baseMessage);
        baseReportError(displayArea, errorMsg, getMsgByKey(baseMessage.getKeyMsg()), new LogicException(baseMessage.getErrorCode(), baseMessage.getKeyMsg()));
    }

    public void reportError(String displayArea, String errCode, String keyMsg, Object... params) {
        String errorMsg = getErrorMsg(errCode, keyMsg, params);
        baseReportError(displayArea, errorMsg, getMsgByKey(keyMsg), new LogicException(errCode, keyMsg));
    }


    public void reportError(String displayArea, LogicException ex) {
        String errorMsg = getErrorMsg(ex.getErrorCode(), ex.getDescription());
        baseReportError(displayArea, errorMsg, errorMsg, ex);
    }

    private String getMsgByKey(String keyMsg) {
        String getTextKey = getText(keyMsg);
        return DataUtil.isNullOrEmpty(getTextKey) ? keyMsg : getTextKey;
    }

    @Deprecated
    public void reportErrorMsg(String displayArea, String msg) {
        try {
            BundleUtil.sendErrorMsg(displayArea, "", msg);
            FacesContext.getCurrentInstance().validationFailed();
        } catch (Exception ex) {
            logger.error("", ex);
        }

    }

    @Deprecated
    public void
    reportError(String displayArea, String keyMsg) {
        try {
            BundleUtil.sendErrorMsg(displayArea, "", getText(keyMsg));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Deprecated
    public void reportError(String keyMsg, Object... params) {

    }

    public void reportWarn(String displayArea, String keyMsg, Object... params) {
        try {
            String msg = MessageFormat.format(getText(keyMsg), params);
            BundleUtil.sendWarnMsg(displayArea, "", msg);
        } catch (Exception ex) {
            logger.error("", ex);
        }

    }

    public HttpServletRequest getRequest() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext extenalContext = facesContext.getExternalContext();
        return (HttpServletRequest) extenalContext.getRequest();
    }

    public UserToken getUserToken() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext extenalContext = facesContext.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) extenalContext.getRequest();
        return (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
    }

    public boolean validatePattern(String regex, String value) {
        if (DataUtil.isNullOrEmpty(regex) || DataUtil.isNullOrEmpty(value)) {
            return false;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }


    public void reportErrorValidateFail(String displayArea, String errCode, String keyMsg) {
        reportError(displayArea, errCode, keyMsg);
        FacesContext.getCurrentInstance().validationFailed();
    }

    public void reportErrorValidateFail(String displayArea, @Nonnull BaseMessage baseMessage) {
        reportError(displayArea, baseMessage);
        FacesContext.getCurrentInstance().validationFailed();
    }

    @Deprecated
    public void reportErrorValidateFail(String displayArea, String keyMsg) {
        try {
            BundleUtil.sendErrorMsg(displayArea, "", getText(keyMsg));
            FacesContext.getCurrentInstance().validationFailed();
        } catch (Exception ex) {
            logger.error("", ex);
        }
    }

    public void reportErrorValidateFail(String displayArea, String errCode, String keyMsg, Object... params) {
        try {
            String msg = MessageFormat.format(getText(keyMsg), params);
            BundleUtil.sendErrorMsg(displayArea, "", errCode + " " + msg);
            FacesContext.getCurrentInstance().validationFailed();
        } catch (Exception ex) {
            logger.error("", ex);
        }
    }

    public void reportErrorValidateFail(String displayArea, LogicException ex) {
        try {
            if (!DataUtil.isNullOrEmpty(ex.getErrorCode())) {
                BundleUtil.sendErrorMsg(displayArea, "", ex.getErrorCode() + " " + ex.getDescription());
            } else {
                BundleUtil.sendErrorMsg(displayArea, "", ex.getDescription());
            }
            FacesContext.getCurrentInstance().validationFailed();
        } catch (Exception ex1) {
            logger.error("", ex1);
        }
    }

    public void reportErrorValidateFail(String displayArea, String errCode, Exception ex) {
        try {
            BundleUtil.sendErrorMsg(displayArea, "", errCode + " " + ex.getMessage());
            FacesContext.getCurrentInstance().validationFailed();
        } catch (Exception ex1) {
            logger.error("", ex1);
        }
    }

    public void reportWarnValidateFail(String displayArea, String keyMsg, Object... params) {
        try {
            String msg = MessageFormat.format(getText(keyMsg), params);
            BundleUtil.sendWarnMsg(displayArea, "", msg);
            FacesContext.getCurrentInstance().validationFailed();
        } catch (Exception ex) {
            logger.error("", ex);
        }
    }

    public void showDialog(String keyInfo, String keyMesg) {
        try {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, getText(keyInfo), getText(keyMesg));
            RequestContext.getCurrentInstance().showMessageInDialog(facesMessage);
        } catch (Exception ex) {
            logger.error("", ex);
        }
    }

    /**
     * Bien cac ki tu dac biet ve dang ascii
     *
     * @param input
     *
     * @return
     */
    public String convertCharacter(String input) {
        if (DataUtil.isNullOrEmpty(input)) {
            return "";
        }
        return DataUtil.stripAccents(input);
    }

    public String trimAndLowercase(String input) {
        String temp = convertCharacter(DataUtil.trim(input));
        return StringUtils.lowerCase(temp);
    }


    /**
     * @param widgetVar
     *
     * @author KhuongDV
     * @datecreate Nov 24th, 2014
     */
    public void focusElement(String widgetVar) {
        RequestContext.getCurrentInstance().execute("focusElement('" + widgetVar + "')");
    }

    public void focusElementWithCursor(String cssSelector) {
        RequestContext.getCurrentInstance().execute("focusElementWithCursor('" + cssSelector + "')");
    }

    /**
     * @param selector
     *
     * @author KhuongDV
     * @datecreate Nov 24th, 2014
     */
    public void focusElementByRawCSSSlector(String selector) {
        RequestContext.getCurrentInstance().execute("focusElementByRawCSSSlector('" + selector + "')");
    }

    /**
     * @param selector
     *
     * @author KhuongDV
     * @datecreate Nov 24th, 2014
     */
    public void showErrorToElement(String selector) {
        RequestContext.getCurrentInstance().execute("showErrorToElement('" + selector + "')");
    }

    public void showDialog(String widgetVar) {
        RequestContext.getCurrentInstance().execute("PF('" + widgetVar + "').show()");
    }

    public void hideDialog(String widgetVar) {
        RequestContext.getCurrentInstance().execute("PF('" + widgetVar + "').hide()");
    }


    /**
     * Phan tich content cua warning message/email
     *
     * @param content
     * @param firstSign
     * @param lastSign
     *
     * @return
     *
     * @author KhuongDV
     */
    public int countParameter(String content, String firstSign, String lastSign) {
        int count = 0;
        firstSign = Pattern.quote(firstSign);
        lastSign = Pattern.quote(lastSign);
        Pattern pattern = Pattern.compile(firstSign + "([a-zA-Z_][a-zA-Z0-9_]*)" + lastSign);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    public Set<String> getParameterInText(String text, String firstSign, String lastSign) {
        Set<String> set = new java.util.HashSet<>();
        String QfirstSign = Pattern.quote(firstSign);
        String QlastSign = Pattern.quote(lastSign);
        Pattern pattern = Pattern.compile(QfirstSign + "([a-zA-Z_][a-zA-Z0-9_]*)" + QlastSign);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            set.add(matcher.group().replace(firstSign, "").replace(lastSign, "").toLowerCase());
        }
        return set;
    }

    int count(String src, String sub) {
        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {

            lastIndex = src.indexOf(sub, lastIndex);

            if (lastIndex != -1) {
                count++;
                lastIndex += sub.length();
            }
        }
        return count;
    }

    public void topPage() {
        RequestContext.getCurrentInstance().execute("topPage()");
    }

    public void focusElementError(String elementId) {
        RequestContext.getCurrentInstance().execute("focusElementError('" + elementId + "')");
    }

    public void removeFocusElementError(String elementId) {
        RequestContext.getCurrentInstance().execute("removeFocusElementError('" + elementId + "')");
    }

    protected void resetTable(String tableClientId) {
        DataTable tableIsdn = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(tableClientId);
        if (tableIsdn != null) {
            tableIsdn.setRows(Integer.parseInt(getText("common.paging.rows.default")));
            tableIsdn.setFirst(0);
        }
    }

    protected void invokeMethod(Object controller, String action) {
        try {
            if (StringUtils.isNotBlank(action) && controller != null) {
                Method method = controller.getClass().getMethod(action);
                method.invoke(controller);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    protected void reportError(String areaUpdate, Exception e) {
        if (e instanceof LogicException) {
            LogicException lg = (LogicException) e;
            reportError(areaUpdate, lg.getErrorCode(), lg.getKeyMsg(), lg.getParamsMsg());
        } else if (e instanceof SocketTimeoutException) {
            logger.error(e.getMessage(), e);
            reportError(areaUpdate, "common.error.service", e);
        } else {
            logger.error(e.getMessage(), e);
            reportError(areaUpdate, e.getMessage(), e);
        }
    }

    protected void reportError(Exception e) {
        reportError("", e);
    }

    protected void reportErrorValidateFail(Exception e) {
        reportError(e);
    }

    /**
     * ham tong quat invoke toi noi dung
     *
     * @param methodName
     * @param param1
     * @param param2
     *
     * @author ThanhNT
     */
    public void callBackMethod(Object objectModel, String methodName, Object param1, Object param2) throws Exception {
        if (objectModel == null || DataUtil.isNullOrEmpty(methodName)) {
            return;
        }
        Class<?> classModel = objectModel.getClass();
        if (param1 != null && param2 != null) {
            Method method = classModel.getMethod(methodName, param1.getClass(), param2.getClass());
            if (method != null) {
                method.invoke(objectModel, param1, param2);
            }
        } else if (param1 != null) {
            Method method = classModel.getMethod(methodName, param1.getClass());
            if (method != null) {
                method.invoke(objectModel, param1);
            }
        } else if (param2 != null) {
            Method method = classModel.getMethod(methodName, param2.getClass());
            if (method != null) {
                method.invoke(objectModel, param2);
            }
        } else {
            Method method = classModel.getMethod(methodName);
            if (method != null) {
                method.invoke(objectModel);
            }
        }
    }


    public BaseMessage validateFileUploadWhiteList(UploadedFile fileAttachment, String[] extension, int maxSize) {
        BaseMessage baseMessage = new BaseMessage(true);
        if (fileAttachment == null) {
            baseMessage.setSuccess(false);
            baseMessage.setDescription(getText("common.import.file.error.file.not.found"));
            return baseMessage;
        }
        String fileName = fileAttachment.getFileName();
//         check file name
        if (DataUtil.isNullOrEmpty(fileName)) {
            baseMessage.setSuccess(false);
            baseMessage.setDescription(getText("common.import.file.error.template"));
            return baseMessage;
        }
        // check duoi file
        String[] fileNameArr = fileName.split("\\.");
        if (fileNameArr == null || fileNameArr.length != 2) {

            BaseMessage msgValid = validateFileName(fileName);
            if (!msgValid.isSuccess()) {
                return msgValid;
            }

            baseMessage.setSuccess(false);
            baseMessage.setDescription(getText("common.import.file.error.template"));
            return baseMessage;
        }

        if (fileAttachment.getContents().length == 0) {
            baseMessage.setSuccess(false);
            baseMessage.setDescription(getText("common.import.file.error.empty"));
            return baseMessage;
        }

        if (maxSize != 0 && fileAttachment.getContents().length > maxSize) {
            baseMessage.setSuccess(false);
            if(DataUtil.safeEqual(maxSize,5*1024*1024)){
                baseMessage.setDescription(getText("common.import.file.error.max.size.5m"));
            }else {
                baseMessage.setDescription(getText("common.import.file.error.max.size"));
            }
            return baseMessage;
        }

        //kiem tra ten file mo rong co fai la extension ko
        String extensionFileName = fileNameArr[1];
        if (!Arrays.asList(extension).contains(extensionFileName.toLowerCase())) {
            baseMessage.setSuccess(false);
            baseMessage.setDescription(getText("common.import.file.error.template"));
            return baseMessage;
        }
        return validateFileName(fileName);
    }

    /**
     * @param fileName
     *
     * @return
     */
    public BaseMessage validateFileName(String fileName) {
        BaseMessage baseMessage = new BaseMessage(true);
        String expression = "[\\s_a-zA-Z0-9\\-\\.\\()]+";
        //Make the comparison case-insensitive.
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(fileName);
        if (!matcher.matches()) {
            baseMessage.setSuccess(false);
            baseMessage.setDescription(getText("common.import.file.error.file.name.invalid"));
            return baseMessage;
        }
        if (fileName.length() > 100) {
            baseMessage.setSuccess(false);
            baseMessage.setDescription(getTextParam("common.import.file.error.file.name.len", fileName));
            return baseMessage;
        }

        return baseMessage;
    }

}
