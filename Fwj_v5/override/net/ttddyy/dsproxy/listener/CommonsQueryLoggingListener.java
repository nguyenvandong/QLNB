//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package net.ttddyy.dsproxy.listener;

import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.GetTextFromBundleHelper;
import net.ttddyy.dsproxy.listener.AbstractQueryLoggingListener;
import net.ttddyy.dsproxy.listener.CommonsLogLevel;
import net.ttddyy.dsproxy.support.CommonsLogUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CommonsQueryLoggingListener extends AbstractQueryLoggingListener {
    protected Log log = LogFactory.getLog(CommonsQueryLoggingListener.class);
    protected CommonsLogLevel logLevel;

    public CommonsQueryLoggingListener() {
        this.logLevel = CommonsLogLevel.DEBUG;
    }

    protected void writeLog(String message) {
        String callId = GetTextFromBundleHelper.getReqId();
        if (!DataUtil.isNullOrEmpty(callId)) {
//            message = "[" + callId  + "]:" + message;
            message = "|" + callId  + "|" + message;
        } else {
            message = "||" + message;
        }

        CommonsLogUtils.writeLog(this.log, this.logLevel, message);
    }

    public void setLogLevel(CommonsLogLevel logLevel) {
        this.logLevel = logLevel;
    }

    protected void resetLogger(String loggerName) {
        this.log = LogFactory.getLog(loggerName);
    }
}

