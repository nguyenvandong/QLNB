package com.viettel.common;

import com.viettel.fw.common.util.DataUtil;
import org.apache.log4j.Logger;

import javax.persistence.Column;
import javax.persistence.Table;
import java.beans.Transient;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by duynh-sonat on 3/6/2017.
 */
public class ReflectUtils {
    private ReflectUtils() {
    }
    public static final Logger logger = Logger.getLogger(ReflectUtils.class);
    private static final String ANNO_COLUMN_NAME = "name=";
    private static final String ANNO_TEMPORAL_TYPE = "value=";
    public static List<Method> getGetters(Class c) {
        List<Method> getters = new ArrayList<Method>();
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods) {
            if (isGetter(method)) {
                getters.add(method);
            }
        }
        return getters;
    }

    public static List<Method> getSetters(Class c) {
        List<Method> setters = new ArrayList<Method>();
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods) {
            if (isSetter(method)) {
                setters.add(method);
            }
        }
        return setters;
    }



    public static boolean isGetter(Method method) {
        boolean result = true;
        if (!method.getName().startsWith("get")
                || method.getParameterTypes().length != 0
                || method.getReturnType().equals(Void.TYPE)
                || method.getAnnotation(Transient.class) != null) {
            result = false;
        }
        return result;
    }

    public static boolean isSetter(Method method) {
        boolean result = true;
        if (!method.getName().startsWith("set")
                || method.getParameterTypes().length != 1
                || !method.getReturnType().equals(Void.TYPE)) {
            result = false;
        }
        return result;
    }


    public static String getColumnName(Class c, Method method) {
        return getAnnotationProperty(c, method, ANNO_COLUMN_NAME);
    }

    public static Method getSetter(Class c, Method getter) {
        Method setter = null;
        String getterName = getter.getName();
        if (getterName != null && getterName.length() > 1) {
            String setterName = "s" + getterName.substring(1);
            Class returnType = getter.getReturnType();
            try {
                setter = c.getMethod(setterName, returnType);
            } catch (Exception ex) {
                logger.error("ERR_95870001: Error at ReflectUtils.getSetter() ", ex);
            }
        }
        return setter;
    }
    public static String getAnnotationProperty(Class c, Method method, String property) {
        String result = "";
        Field[] arr = c.getDeclaredFields();
        String attribute = method.getName();
        if (attribute.startsWith("get") && arr != null && arr.length > 0) {
            attribute = attribute.substring(3, 4).toLowerCase() + attribute.substring(4);
            for (Field field : arr) {
                if (field.getName().equals(attribute)) {
                    Annotation annos = field.getAnnotation(Column.class);
                    if (annos != null) {
                        String annoColumn = annos.toString();
                        result = annoColumn.substring(
                                annoColumn.indexOf(property) + property.length(),
                                annoColumn.indexOf(",", annoColumn.indexOf(property)));
                        if(DataUtil.isStringNullOrEmpty(result)) {
                            result = field.getName().toUpperCase();
                        }
                    }
                    break;
                }
            }
            if(DataUtil.isStringNullOrEmpty(result)) {
                result = attribute.replaceAll("(.)(\\p{Lu})", "$1_$2").toUpperCase();
            }
        }
        return result;
    }
    public static String getTableName(Class c) {
        Annotation annos = c.getAnnotation(Table.class);
        String result = "";
        if (annos != null) {
            String annoColumn = annos.toString();
            annoColumn = annoColumn.substring(annoColumn.indexOf(ANNO_COLUMN_NAME) + ANNO_COLUMN_NAME.length());
            result = annoColumn.substring(0, annoColumn.indexOf(",")).trim();
        }
        return result;
    }

}
