package com.viettel.common;

import java.io.Serializable;

/**
 * Created by dongnd3 on 7/17/2015.
 */
public class CommonObject implements Serializable {
    private String id;
    private String label;
    private Long parrentId;
    public CommonObject() {
        //CommonObject
    }

    public CommonObject(String id) {
        this.id = id;
    }

    public CommonObject(String id, String label) {
        this.id = id;
        this.label = label;
    }
    public CommonObject(String id, String label, Long parrentId) {
        this.id = id;
        this.label = label;
        this.parrentId = parrentId;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getParrentId() {
        return parrentId;
    }

    public void setParrentId(Long parrentId) {
        this.parrentId = parrentId;
    }
}
