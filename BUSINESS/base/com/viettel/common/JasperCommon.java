package com.viettel.common;

/**
 * Created by TuyenNT17 on 2/2/2016.
 */
public class JasperCommon {
    public static final String path_img = "/resources/images";
    public static final String img_checked = "/check.jpg";
    public static final String img_unchecked = "/uncheck.jpg";
}
