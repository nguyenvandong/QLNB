/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bankplus.webchain.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 */
@Entity
@Table(name = "AREA")
@XmlRootElement
public class Area implements Serializable {

    private static final Long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AREA_CODE")
    private String areaCode;
    @Size(max = 15)
    @Column(name = "PARENT_CODE")
    private String parentCode;
    @Size(max = 4)
    @Column(name = "PROVINCE")
    private String province;
    @Size(max = 3)
    @Column(name = "DISTRICT")
    private String district;
    @Size(max = 4)
    @Column(name = "PRECINCT")
    private String precinct;
    @Size(max = 5)
    @Column(name = "STREET_BLOCK")
    private String streetBlock;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "NAME")
    private String name;
    @Size(max = 900)
    @Column(name = "FULL_NAME")
    private String fullName;
    @Size(max = 1)
    @Column(name = "CENTER")
    private String center;
    @Size(max = 1)
    @Column(name = "STATUS")
    private String status;
    @Size(max = 20)
    @Column(name = "PSTN_CODE")
    private String pstnCode;

    public Area() {

    }

    public Area(String areaCode) {
        this.areaCode = areaCode;
    }


    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getPstnCode() {
        return pstnCode;
    }

    public void setPstnCode(String pstnCode) {
        this.pstnCode = pstnCode;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (areaCode == null ? 0 : areaCode.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Area)) {
            return false;
        }
        Area other = (Area) object;
        if ((this.areaCode == null && other.areaCode != null) || (this.areaCode != null && !this.areaCode.equals(other.areaCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.bccs.sale.model.Area[ areaCode=" + areaCode + " ]";
    }

}
