package com.viettel.bankplus.webchain.model;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.StringPath;

import javax.annotation.Generated;

import static com.mysema.query.types.PathMetadataFactory.forVariable;


/**
 * QArea is a Querydsl query type for Area
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QArea extends EntityPathBase<Area> {

    private static final long serialVersionUID = -479993154;

    public static final QArea area = new QArea("area");
    public final StringPath areaCode = createString("areaCode");
    public final StringPath parentCode = createString("parentCode");
    public final StringPath province = createString("province");
    public final StringPath district = createString("district");
    public final StringPath precinct = createString("precinct");
    public final StringPath streetBlock = createString("streetBlock");
    public final StringPath name = createString("name");
    public final StringPath fullName = createString("fullName");
    public final StringPath center = createString("center");
    public final StringPath status = createString("status");
    public final StringPath pstnCode = createString("pstnCode");

    public QArea(String variable) {
        super(Area.class, forVariable(variable));
    }

    public QArea(Path<? extends Area> entity) {
        super(entity.getType(), entity.getMetadata());
    }

    public QArea(PathMetadata<?> metadata) {
        super(Area.class, metadata);
    }

}

