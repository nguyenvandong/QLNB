package com.viettel.bankplus.webchain.model;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

import javax.annotation.Generated;
import java.util.Date;

import static com.mysema.query.types.PathMetadataFactory.forVariable;


/**
 * QActionLogDetail is a Querydsl query type for ActionLogDetail
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QActionLogDetail extends EntityPathBase<ActionLogDetail> {

    private static final long serialVersionUID = 597427597L;

    public static final QActionLogDetail actionLogDetail = new QActionLogDetail("actionLogDetail");

    public final DateTimePath<Date> actionDate = createDateTime("actionDate", Date.class);

    public final NumberPath<Long> actionDetailId = createNumber("actionDetailId", Long.class);

    public final NumberPath<Long> actionLogId = createNumber("actionLogId", Long.class);

    public final StringPath columnName = createString("columnName");

    public final StringPath description = createString("description");

    public final StringPath newValue = createString("newValue");

    public final StringPath oldValue = createString("oldValue");

    public final StringPath tableName = createString("tableName");

    public QActionLogDetail(String variable) {
        super(ActionLogDetail.class, forVariable(variable));
    }

    public QActionLogDetail(Path<? extends ActionLogDetail> path) {
        super(path.getType(), path.getMetadata());
    }

    public QActionLogDetail(PathMetadata<?> metadata) {
        super(ActionLogDetail.class, metadata);
    }

}

