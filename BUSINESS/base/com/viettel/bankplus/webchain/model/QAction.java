package com.viettel.bankplus.webchain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAction is a Querydsl query type for Action
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAction extends EntityPathBase<Action> {

    private static final long serialVersionUID = 519457382L;

    public static final QAction action = new QAction("action");

    public final NumberPath<Long> actionId = createNumber("actionId", Long.class);

    public final StringPath code = createString("code");

    public final StringPath description = createString("description");

    public final StringPath name = createString("name");

    public final NumberPath<Short> status = createNumber("status", Short.class);

    public final StringPath type = createString("type");

    public QAction(String variable) {
        super(Action.class, forVariable(variable));
    }

    public QAction(Path<? extends Action> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAction(PathMetadata<?> metadata) {
        super(Action.class, metadata);
    }

}

