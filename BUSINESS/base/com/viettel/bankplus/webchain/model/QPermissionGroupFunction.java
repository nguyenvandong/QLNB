package com.viettel.bankplus.webchain.model;


import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.PathMetadataFactory;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

import java.util.Date;

public class QPermissionGroupFunction extends EntityPathBase<PermissionGroupFunction> {
    private static final long serialVersionUID = -1236953266L;
    public static final QPermissionGroupFunction permissionGroupFunction = new QPermissionGroupFunction("permissionGroupFunction");
    public final DateTimePath<Date> createDate = this.createDateTime("createDate", Date.class);
    public final StringPath createUser = this.createString("createUser");
    public final NumberPath<Long> functionObjectId = this.createNumber("functionObjectId", Long.class);
    public final NumberPath<Long> permissionGroupFunctionId = this.createNumber("permissionGroupFunctionId", Long.class);
    public final NumberPath<Long> permissionGroupId = this.createNumber("permissionGroupId", Long.class);
    public final NumberPath<Short> status = this.createNumber("status", Short.class);

    public QPermissionGroupFunction(String variable) {
        super(PermissionGroupFunction.class, PathMetadataFactory.forVariable(variable));
    }

    public QPermissionGroupFunction(Path<? extends PermissionGroupFunction> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPermissionGroupFunction(PathMetadata metadata) {
        super(PermissionGroupFunction.class, metadata);
    }
}