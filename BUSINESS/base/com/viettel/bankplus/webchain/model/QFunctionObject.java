package com.viettel.bankplus.webchain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QFunctionObject is a Querydsl query type for FunctionObject
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QFunctionObject extends EntityPathBase<FunctionObject> {

    private static final long serialVersionUID = 1499749735L;

    public static final QFunctionObject functionObject = new QFunctionObject("functionObject");

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final StringPath createUser = createString("createUser");

    public final StringPath description = createString("description");

    public final NumberPath<Short> functionAdminType = createNumber("functionAdminType", Short.class);

    public final StringPath functionCode = createString("functionCode");

    public final StringPath functionName = createString("functionName");

    public final NumberPath<Long> functionObjectId = createNumber("functionObjectId", Long.class);

    public final StringPath functionPath = createString("functionPath");

    public final NumberPath<Short> functionType = createNumber("functionType", Short.class);

    public final NumberPath<Short> orderNo = createNumber("orderNo", Short.class);

    public final NumberPath<Long> parentId = createNumber("parentId", Long.class);

    public final NumberPath<Short> status = createNumber("status", Short.class);

    public final DateTimePath<java.util.Date> updateDate = createDateTime("updateDate", java.util.Date.class);

    public final StringPath updateUser = createString("updateUser");

    public final NumberPath<Short> permissionType = createNumber("permissionType", Short.class);

    public QFunctionObject(String variable) {
        super(FunctionObject.class, forVariable(variable));
    }

    public QFunctionObject(Path<? extends FunctionObject> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFunctionObject(PathMetadata<?> metadata) {
        super(FunctionObject.class, metadata);
    }

}

