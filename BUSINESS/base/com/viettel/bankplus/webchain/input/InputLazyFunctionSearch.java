package com.viettel.bankplus.webchain.input;

public class InputLazyFunctionSearch {
    private String functionCode;
    private Short status;

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
}
