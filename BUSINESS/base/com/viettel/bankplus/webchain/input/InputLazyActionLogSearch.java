package com.viettel.bankplus.webchain.input;

/**
 * Created by Dong on 8/14/2017.
 */
public class InputLazyActionLogSearch {
    private String code;
    private String userName;
    private Long status;
    private String type;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String name) {
        this.userName = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
