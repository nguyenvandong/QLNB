package com.viettel.bankplus.webchain.input;

import java.util.List;

/**
 * @author hungnq on 9/7/2017
 */
public class InputLazyUserPermissionGroup {
    private List<Long> chainUserIdList;
    private List<Long> permissionGroupIdList;
    private Short status;
    private Long chainId;

    public List<Long> getChainUserIdList() {
        return chainUserIdList;
    }

    public void setChainUserIdList(List<Long> chainUserIdList) {
        this.chainUserIdList = chainUserIdList;
    }

    public List<Long> getPermissionGroupIdList() {
        return permissionGroupIdList;
    }

    public void setPermissionGroupIdList(List<Long> permissionGroupIdList) {
        this.permissionGroupIdList = permissionGroupIdList;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Long getChainId() {
        return chainId;
    }

    public void setChainId(Long chainId) {
        this.chainId = chainId;
    }
}
