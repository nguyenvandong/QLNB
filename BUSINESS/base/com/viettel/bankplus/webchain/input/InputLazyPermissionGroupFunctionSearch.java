package com.viettel.bankplus.webchain.input;

/**
 * Created by truon on 07-09-2017.
 */
public class InputLazyPermissionGroupFunctionSearch {
    private String groupCode;
    private Short status;
    private Long shopId;
    private Long userId;

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
