package com.viettel.bankplus.webchain.input;

/**
 * @author daibq
 * @// TODO: 8/8/2018
 */
public class InputLazyEmpSearch {
    private String username;
    private String fullName;
    private String employeeCode;
    private Short employeeType;
    private Double salary;
    private Integer jobType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public Short getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(Short employeeType) {
        this.employeeType = employeeType;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Integer getJobType() {
        return jobType;
    }

    public void setJobType(Integer jobType) {
        this.jobType = jobType;
    }
}
