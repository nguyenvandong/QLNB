package com.viettel.bankplus.webchain.input;

/**
 * Created by Dong on 8/14/2017.
 */
public class InputLazyActionSearch {
    private String code;
    private String name;
    private Long status;
    private String type;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
