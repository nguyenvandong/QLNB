package com.viettel.bankplus.webchain.input;

import java.util.List;

/**
 * Created by buiqu on 10/2/2017.
 */
public class InputLazyUserPermissionGroupCreate {
    private List<Long> chainUserIdListCreate;
    private List<Long> permissionGroupIdListCreate;

    public List<Long> getChainUserIdListCreate() {
        return chainUserIdListCreate;
    }

    public void setChainUserIdListCreate(List<Long> chainUserIdListCreate) {
        this.chainUserIdListCreate = chainUserIdListCreate;
    }

    public List<Long> getPermissionGroupIdListCreate() {
        return permissionGroupIdListCreate;
    }

    public void setPermissionGroupIdListCreate(List<Long> permissionGroupIdListCreate) {
        this.permissionGroupIdListCreate = permissionGroupIdListCreate;
    }
}
