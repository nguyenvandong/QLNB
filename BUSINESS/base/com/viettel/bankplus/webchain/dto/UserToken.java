package com.viettel.bankplus.webchain.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import viettel.passport.client.DeptToken;
import viettel.passport.client.ObjectToken;
import viettel.passport.client.RoleToken;


/**
 * @author hungnq on 12/18/2017
 */
public class UserToken implements Serializable {

    //    private static final Logger logger = new Logger();
    private String aliasName;
    private String birthPlace;
    private String cellphone;
    private Date dateOfBirth;
    private String description;
    private String email;
    private String fax;
    private String fullName;
    private long gender;
    private String identityCard;
    private Date issueDateIdent;
    private Date issueDatePassport;
    private String issuePlaceIdent;
    private String issuePlacePassport;
    private String passportNumber;
    private String staffCode;
    private long status;
    private String telephone;
    private String userName;
    private Long userId;
    private long userRight;
    private long timeToPasswordExpire;
    private Date lastChangePassword;
    private long passwordValidTime;
    private String deptName;
    private Long deptId;
    private long useSalt;
    private List<DeptToken> deptTokens;
    private List<viettel.passport.client.ObjectToken> objectTokens;
    private List<viettel.passport.client.ObjectToken> parentMenu;
    private List<viettel.passport.client.ObjectToken> componentList;
    private List<viettel.passport.client.RoleToken> rolesList;

    public UserToken() {

    }


    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getGender() {
        return gender;
    }

    public void setGender(long gender) {
        this.gender = gender;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public Date getIssueDateIdent() {
        return issueDateIdent;
    }

    public void setIssueDateIdent(Date issueDateIdent) {
        this.issueDateIdent = issueDateIdent;
    }

    public Date getIssueDatePassport() {
        return issueDatePassport;
    }

    public void setIssueDatePassport(Date issueDatePassport) {
        this.issueDatePassport = issueDatePassport;
    }

    public String getIssuePlaceIdent() {
        return issuePlaceIdent;
    }

    public void setIssuePlaceIdent(String issuePlaceIdent) {
        this.issuePlaceIdent = issuePlaceIdent;
    }

    public String getIssuePlacePassport() {
        return issuePlacePassport;
    }

    public void setIssuePlacePassport(String issuePlacePassport) {
        this.issuePlacePassport = issuePlacePassport;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getUserRight() {
        return userRight;
    }

    public void setUserRight(long userRight) {
        this.userRight = userRight;
    }

    public long getTimeToPasswordExpire() {
        return timeToPasswordExpire;
    }

    public void setTimeToPasswordExpire(long timeToPasswordExpire) {
        this.timeToPasswordExpire = timeToPasswordExpire;
    }

    public Date getLastChangePassword() {
        return lastChangePassword;
    }

    public void setLastChangePassword(Date lastChangePassword) {
        this.lastChangePassword = lastChangePassword;
    }

    public long getPasswordValidTime() {
        return passwordValidTime;
    }

    public void setPasswordValidTime(long passwordValidTime) {
        this.passwordValidTime = passwordValidTime;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public long getUseSalt() {
        return useSalt;
    }

    public void setUseSalt(long useSalt) {
        this.useSalt = useSalt;
    }

    public List<DeptToken> getDeptTokens() {
        return deptTokens;
    }

    public void setDeptTokens(List<DeptToken> deptTokens) {
        this.deptTokens = deptTokens;
    }

    public List<ObjectToken> getObjectTokens() {
        return objectTokens;
    }

    public void setObjectTokens(List<ObjectToken> objectTokens) {
        this.objectTokens = objectTokens;
    }

    public List<ObjectToken> getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(List<ObjectToken> parentMenu) {
        this.parentMenu = parentMenu;
    }

    public List<ObjectToken> getComponentList() {
        return componentList;
    }

    public void setComponentList(List<ObjectToken> componentList) {
        this.componentList = componentList;
    }

    public List<RoleToken> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<RoleToken> rolesList) {
        this.rolesList = rolesList;
    }

}
