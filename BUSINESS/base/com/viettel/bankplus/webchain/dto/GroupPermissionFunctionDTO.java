package com.viettel.bankplus.webchain.dto;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;

/**
 * Created by truon on 07-09-2017.
 */
public class GroupPermissionFunctionDTO extends BaseDTO implements Serializable {
    public String getKeySet() {
        return keySet;
    }
    private Long permissionFunctionId;
    private Long permissionId;
    private String permissionCode;
    private String permissionTypeName;
    private String permissionName;
    private String functionName;
    private String functionParentName;
    private Short status;

    public Long getPermissionFunctionId() {
        return permissionFunctionId;
    }

    public void setPermissionFunctionId(Long permissionFunctionId) {
        this.permissionFunctionId = permissionFunctionId;
    }

    public Long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    public String getPermissionTypeName() {
        return permissionTypeName;
    }

    public void setPermissionTypeName(String permissionTypeName) {
        this.permissionTypeName = permissionTypeName;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionParentName() {
        return functionParentName;
    }

    public void setFunctionParentName(String functionParentName) {
        this.functionParentName = functionParentName;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
}
