package com.viettel.bankplus.webchain.dto;

import com.viettel.fw.dto.BaseMessage;

import java.util.List;

public class ApDomainMessageDTO extends BaseMessage {
    private List<ApDomainDTO> listApDomainDTO;


    public List<ApDomainDTO> getListApDomainDTO() {
        return listApDomainDTO;
    }

    public void setListApDomainDTO(List<ApDomainDTO> listApDomainDTO) {
        this.listApDomainDTO = listApDomainDTO;
    }
}
