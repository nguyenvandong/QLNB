package com.viettel.bankplus.webchain.dto;

import java.lang.Long;
import java.util.Date;
import java.lang.Short;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;

public class PermissionGroupDTO extends BaseDTO implements Serializable {
    public String getKeySet() {
        return keySet;
    }

    public static enum COLUMNS {CREATEDATE, CREATEUSER, PERMISSIONGROUPCODE, PERMISSIONGROUPID, PERMISSIONGROUPNAME, PERMISSTIONGROUPTYPE, DESCRIPTION, STATUS, UPDATEDATE, UPDATEUSER, EXCLUSE_ID_LIST}

    ;
    private Date createDate;
    private String createUser;
    private String permissionGroupCode;
    private Long permissionGroupId;
    private String permissionGroupName;
    private Long permissionGroupType;
    private String strPermissionGroupType;
    private String description;
    private Long status;
    private String strStatus;
    private Date updateDate;
    private String updateUser;
    private Long chainShopId;
    private Long userObjectType;
    /*them*/
    private String importExcelError;

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getPermissionGroupCode() {
        return this.permissionGroupCode;
    }

    public void setPermissionGroupCode(String permissionGroupCode) {
        this.permissionGroupCode = permissionGroupCode;
    }

    public Long getPermissionGroupId() {
        return this.permissionGroupId;
    }

    public void setPermissionGroupId(Long permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public String getPermissionGroupName() {
        return this.permissionGroupName;
    }

    public void setPermissionGroupName(String permissionGroupName) {
        this.permissionGroupName = permissionGroupName;
    }

    public Long getPermissionGroupType() {
        return permissionGroupType;
    }

    public void setPermissionGroupType(Long permissionGroupType) {
        this.permissionGroupType = permissionGroupType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Long getChainShopId() {
        return chainShopId;
    }

    public void setChainShopId(Long chainShopId) {
        this.chainShopId = chainShopId;
    }

    public String getImportExcelError() {
        return importExcelError;
    }

    public void setImportExcelError(String importExcelError) {
        this.importExcelError = importExcelError;
    }

    public String getStrStatus() {
        return strStatus;
    }

    public void setStrStatus(String strStatus) {
        this.strStatus = strStatus;
    }

    public String getStrPermissionGroupType() {
        return strPermissionGroupType;
    }

    public void setStrPermissionGroupType(String strPermissionGroupType) {
        this.strPermissionGroupType = strPermissionGroupType;
    }

    public Long getUserObjectType() {
        return userObjectType;
    }

    public void setUserObjectType(Long userObjectType) {
        this.userObjectType = userObjectType;
    }
}
