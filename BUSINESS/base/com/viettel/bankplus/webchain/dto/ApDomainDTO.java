package com.viettel.bankplus.webchain.dto;

import java.lang.Long;
import java.util.Date;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;

public class ApDomainDTO extends BaseDTO implements Serializable {
    public String getKeySet() {
        return keySet;
    }

    public static enum COLUMNS {APDOMAINID, CODE, CREATEBY, CREATEDATE, DESCRIPTION, NAME, STATUS, TYPE, VALUE, EXCLUSE_ID_LIST}

    ;
    private Long apDomainId;
    private String code;
    private String createBy;
    private Date createDate;
    private String description;
    private String name;
    private Long status;
    private String type;
    private String value;

    private boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public Long getApDomainId() {
        return this.apDomainId;
    }

    public void setApDomainId(Long apDomainId) {
        this.apDomainId = apDomainId;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreateBy() {
        return this.createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
