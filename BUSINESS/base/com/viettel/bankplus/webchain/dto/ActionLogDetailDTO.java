package com.viettel.bankplus.webchain.dto;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;
import java.util.Date;

public class ActionLogDetailDTO extends BaseDTO implements Serializable {
    public String getKeySet() {
        return keySet;
    }

    public static enum COLUMNS {ACTIONDATE, ACTIONDETAILID, ACTIONLOGID, COLUMNNAME, DESCRIPTION, NEWVALUE, OLDVALUE, TABLENAME, EXCLUSE_ID_LIST}

    ;
    private Date actionDate;
    private Long actionDetailId;
    private Long actionLogId;
    private String columnName;
    private String description;
    private String newValue;
    private String oldValue;
    private String tableName;

    public Date getActionDate() {
        return this.actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public Long getActionDetailId() {
        return this.actionDetailId;
    }

    public void setActionDetailId(Long actionDetailId) {
        this.actionDetailId = actionDetailId;
    }

    public Long getActionLogId() {
        return this.actionLogId;
    }

    public void setActionLogId(Long actionLogId) {
        this.actionLogId = actionLogId;
    }

    public String getColumnName() {
        return this.columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNewValue() {
        return this.newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getOldValue() {
        return this.oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
