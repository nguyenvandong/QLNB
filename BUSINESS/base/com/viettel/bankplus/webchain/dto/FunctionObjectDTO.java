package com.viettel.bankplus.webchain.dto;

import java.lang.Long;
import java.util.Date;
import java.lang.Short;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;

public class FunctionObjectDTO extends BaseDTO implements Serializable {
    public String getKeySet() {
        return keySet;
    }

    public static enum COLUMNS {CREATEDATE, CREATEUSER, DESCRIPTION, FUNCTIONADMINTYPE, FUNCTIONCODE, FUNCTIONNAME, FUNCTIONOBJECTID, FUNCTIONPATH, FUNCTIONTYPE, ORDERNO, PARENTID, STATUS, UPDATEDATE, UPDATEUSER, EXCLUSE_ID_LIST}

    ;
    private Date createDate;
    private String createUser;
    private String description;
    private Short functionAdminType;
    private String functionCode;
    private String functionName;
    private Long functionObjectId;
    private String functionPath;
    private Short functionType;
    private Short orderNo;
    private Long parentId;
    private Short status;
    private Date updateDate;
    private String updateUser;
    private Short permissionType;
    // add more
    private boolean loadChildren;
    private String parentFunctionName;
    private String oldCode;
    private String oldName;


    public boolean isLoadChildren() {
        return loadChildren;
    }

    public void setLoadChildren(boolean loadChildren) {
        this.loadChildren = loadChildren;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getFunctionAdminType() {
        return this.functionAdminType;
    }

    public void setFunctionAdminType(Short functionAdminType) {
        this.functionAdminType = functionAdminType;
    }

    public String getFunctionCode() {
        return this.functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getFunctionName() {
        return this.functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public Long getFunctionObjectId() {
        return this.functionObjectId;
    }

    public void setFunctionObjectId(Long functionObjectId) {
        this.functionObjectId = functionObjectId;
    }

    public String getFunctionPath() {
        return this.functionPath;
    }

    public void setFunctionPath(String functionPath) {
        this.functionPath = functionPath;
    }

    public Short getFunctionType() {
        return this.functionType;
    }

    public void setFunctionType(Short functionType) {
        this.functionType = functionType;
    }

    public Short getOrderNo() {
        return this.orderNo;
    }

    public void setOrderNo(Short orderNo) {
        this.orderNo = orderNo;
    }

    public Long getParentId() {
        return this.parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Short getStatus() {
        return this.status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getParentFunctionName() {
        return parentFunctionName;
    }

    public void setParentFunctionName(String parentFunctionName) {
        this.parentFunctionName = parentFunctionName;
    }

    public Short getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(Short permissionType) {
        this.permissionType = permissionType;
    }

    public String getOldCode() {
        return oldCode;
    }

    public void setOldCode(String oldCode) {
        this.oldCode = oldCode;
    }

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }
}
