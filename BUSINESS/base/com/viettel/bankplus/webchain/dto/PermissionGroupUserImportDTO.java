package com.viettel.bankplus.webchain.dto;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by buiqu on 11/20/2017.
 */
public class PermissionGroupUserImportDTO extends BaseDTO implements Serializable {
    public String getKeySet() {
        return keySet;
    }
    private Long chainUserId;
    private Long permissionGroupId;
    private String permissionGroupCode;
    private String userName;
    private String createUser;
    private Date createDate;
    private Short status;


    /*them*/
    private String importExcelError;

    public Long getChainUserId() {
        return chainUserId;
    }

    public void setChainUserId(Long chainUserId) {
        this.chainUserId = chainUserId;
    }

    public Long getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(Long permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public String getPermissionGroupCode() {
        return permissionGroupCode;
    }

    public void setPermissionGroupCode(String permissionGroupCode) {
        this.permissionGroupCode = permissionGroupCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImportExcelError() {
        return importExcelError;
    }

    public void setImportExcelError(String importExcelError) {
        this.importExcelError = importExcelError;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
}
