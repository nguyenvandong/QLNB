package com.viettel.bankplus.webchain.service;

import java.util.Date;
import java.util.List;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.fw.dto.BaseMessage;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * @author daibq
 * @// TODO: 7/8/2018
 */
@WebService
public interface ChangePasswordService {

    @WebMethod
    public EmployeeDTO findOne(Long id) throws Exception;

    @WebMethod
    BaseMessage changePassword(String newPassword, String userName, String takeSalt, long id, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public boolean checkPassword(String oldPassword, String takeSalt) throws Exception;

    @WebMethod
    public String takeSaltWithUser(String userName) throws Exception;

    @WebMethod
    boolean checkDuplicatePassword(String newPassword, String takeSalt, String userName) throws Exception;

    @WebMethod
    public Date getLastDateChangePass(String userName);

    @WebMethod
    public Date getSysDate() throws Exception;

    @WebMethod
    long findId(String userName) throws Exception;

    @WebMethod
    public EmployeeDTO getValueByUserName(String userName) throws Exception;

}