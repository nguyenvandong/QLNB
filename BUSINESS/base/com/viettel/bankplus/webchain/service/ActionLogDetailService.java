package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.bankplus.webchain.dto.ActionLogDetailDTO;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.model.ActionLogDetail;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
public interface ActionLogDetailService {

    @WebMethod
    ActionLogDetailDTO findOne(Long id) throws Exception;

    @WebMethod
    Long count(List<FilterRequest> filters) throws Exception;

    @WebMethod
    List<ActionLogDetailDTO> findByFilter(List<FilterRequest> filters) throws Exception;

    List<ActionLogDetail> getActionLogDetailList(ActionLog actionLog, Object newModel, Object oldModel, List<String> trackChangeColums, Date sysDate, boolean trackAll) throws Exception;

    @WebMethod
    int countList(ActionLogDTO dto) throws Exception;

    @WebMethod
    List<ActionLogDetailDTO> findLazingPaging(ActionLogDTO dto, int pageSize, int pageNumber) throws Exception;
}