package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionSearch;
import com.viettel.fw.common.util.extjs.FilterRequest;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface EmployeeService {
    @WebMethod
    public EmployeeDTO findOne(Long id) throws Exception;

    @WebMethod
    long findId(String userName) throws Exception;


}