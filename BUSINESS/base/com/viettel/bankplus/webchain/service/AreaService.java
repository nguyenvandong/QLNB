package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.AreaDTO;

import javax.jws.WebService;
import java.util.List;

/**
 * @author hungnq on 9/6/2017
 */
@WebService
public interface AreaService {
    public List<AreaDTO> getProvinceList() throws Exception;

    public List<AreaDTO> getDistrictListByProvinceCode(String provinceCode) throws Exception;

    public List<AreaDTO> getPrecinctListByDistrictCode(String districtCode) throws Exception;

    public List<AreaDTO> exportArea() throws Exception;

    public boolean isExistArea(String areaCode) throws Exception;
}
