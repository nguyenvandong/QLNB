package com.viettel.bankplus.webchain.service;

import java.util.List;

import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.input.InputLazyActionSearch;
import com.viettel.fw.common.util.extjs.FilterRequest;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ActionService {

    @WebMethod
    public ActionDTO findOne(Long id) throws Exception;

    @WebMethod
    public Long count(List<FilterRequest> filters) throws Exception;

    @WebMethod
    public List<ActionDTO> findByFilter(List<FilterRequest> filters) throws Exception;

    @WebMethod
    public int countActionList(InputLazyActionSearch inputLazyActionSearch) throws Exception;

    @WebMethod
    public List<ActionDTO> findLazingPaging(InputLazyActionSearch inputLazyProgramSearch, int pageSize, int pageNumber) throws Exception;

    @WebMethod
    public void saveOrUpdate(ActionDTO actionDTO, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public void delList(ActionDTO dto, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public boolean isCodeOrIdExist(String code, Long id) throws Exception;

    @WebMethod
    public boolean isDuplicateName(String name) throws Exception;

    @WebMethod
    List<ActionDTO> getValueActionByTypeOrderByCode(String type) throws Exception;

    @WebMethod
    List<ActionDTO> getValueActionByType(String type) throws Exception;

    @WebMethod
    List<ActionDTO> getListAction() throws Exception;

    //@hungnq
    @WebMethod
    List<ActionDTO> getListActionDTOByType(String type) throws Exception;
}