package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.common.DataLogin;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface AuthenticationService {

    @WebMethod
    public DataLogin login(String username, String password) throws Exception;

    @WebMethod
    public EmployeeDTO findOne(Long id) throws Exception;

}
