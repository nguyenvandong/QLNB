package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionSearch;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.model.ActionLogDetail;
import com.viettel.bankplus.webchain.model.Employee;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import com.viettel.bankplus.webchain.repo.ChangePasswordRepo;
import com.viettel.bankplus.webchain.repo.EmployeeRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.AESUtil;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.fw.common.util.mapper.BaseMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebService
public class EmployeeServiceImpl implements EmployeeService {
    private Date current;
    @PersistenceContext(unitName = com.viettel.common.Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;
    BaseMapper<Employee, EmployeeDTO> mapperem = new BaseMapper<>(Employee.class, EmployeeDTO.class);
    private final BaseMapper<ActionLog, ActionLogDTO> mapperActionLog = new BaseMapper<>(ActionLog.class, ActionLogDTO.class);
    @Autowired
    private EmployeeRepo repo;

    public static final Logger LOGGER = Logger.getLogger(EmployeeService.class);
    @Autowired
    private ActionLogRepo actionLogRepo;
    @Autowired
    private ActionLogDetailService actionLogDetailService;
    @Autowired
    private ActionLogDetailRepo actionLogDetailRepo;
    @Autowired
    private ChangePasswordRepo repo1;
    @WebMethod
    public EmployeeDTO findOne(Long id) throws Exception {
        return mapperem.toDtoBean(repo.findOne(id));
    }

    @Override
    public long findId(String userName) throws Exception {
        return repo1.findId(userName);
    }
   /* @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(ChainUserDTO chainUserDTO, ActionLogDTO actionLogDTO) throws Exception {
        try {
            Date sysdate = DbUtil.getSysDate(em);
            // cap nhat
            ChainUser chainUserOld = new ChainUser();
            ChainUser chainUser;
            if (!DataUtil.isNullObject(chainUserDTO.getChainUserId())) {
                chainUser = repo.findOne(chainUserDTO.getChainUserId());
                BeanUtils.copyProperties(chainUser, chainUserOld);
                chainUser.setUpdateDate(sysdate);
                chainUser.setUpdateUser(actionLogDTO.getUserName());
                chainUser.setUserName(chainUserDTO.getUserName());
                chainUser.setPhoneNumber(chainUserDTO.getPhoneNumber());
                chainUser.setFullName(chainUserDTO.getFullName());
                repo.saveAndFlush(chainUser);
                //ghi log action detail
                actionLogDTO.setCreateDate(sysdate);
                actionLogDTO.setAction("UPDATE CHAIN_USER");
                actionLogDTO.setObjectId(chainUser.getChainUserId());
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + chainUserDTO.getChainUserId() + "]");
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.saveAndFlush(actionLog);
                List<ActionLogDetail> actionLogDetails = actionLogDetailService.getActionLogDetailList(actionLog, chainUser, chainUserOld, new ArrayList<>(), sysdate, true);
                actionLogDetailRepo.save(actionLogDetails);
            } else {// them moi
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                String salt = simpleDateFormat.format(sysdate);
                String hashPassword = AESUtil.createHash(chainUserDTO.getPassword(), salt);
                chainUser = mapper.toPersistenceBean(chainUserDTO);
                chainUser.setCreateDate(sysdate);
                chainUser.setCreateUser(actionLogDTO.getUserName());
                chainUser.setChainUserType(Const.WEB_CHAIN.CHAIN_USER_TYPE_CHAIN_STORE);
                chainUser.setPhoneNumber(chainUserDTO.getPhoneNumber());
                chainUser.setStatus(DataUtil.safeToShort(Const.WEB_CHAIN.STATUS_ACTIVE));
                chainUser.setActiveStatus(DataUtil.safeToShort(Const.WEB_CHAIN.STATUS_ACTIVE));
                chainUser.setPassword(hashPassword);
                chainUser.setSalt(salt);
                chainUser.setUserName(chainUserDTO.getPrefixUsername() + chainUser.getUserName());
                repo.saveAndFlush(chainUser);
                actionLogDTO.setCreateDate(sysdate);
                actionLogDTO.setAction("CREATE CHAIN_USER");
                actionLogDTO.setObjectId(chainUser.getChainUserId());
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " ID " + chainUser.getChainUserId());
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.save(actionLog);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteChainUser(Long chainUserId, ActionLogDTO actionLogDTO) throws Exception {
        Date currentDate = getSysDate(em);
        ChainUser chainUser = repo.findOne(chainUserId);
        chainUser.setStatus(DataUtil.safeToShort(Const.WEB_CHAIN.STATUS_IN_ACTIVE));
        chainUser.setUpdateUser(actionLogDTO.getUserName());
        chainUser.setUpdateDate(currentDate);
        repo.saveAndFlush(chainUser);
        actionLogDTO.setAction("DELETE CHAIN_USER");
        actionLogDTO.setObjectId(chainUser.getChainUserId());
        actionLogDTO.setCreateDate(currentDate);
        actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + chainUserId);
        ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
        actionLogRepo.save(actionLog);
    }*/
}
