package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.model.Employee;
import com.viettel.bankplus.webchain.model.FunctionObject;

import com.viettel.bankplus.webchain.model.QEmployee;
import com.viettel.bankplus.webchain.repo.ChangePasswordRepo;
import com.viettel.bankplus.webchain.repo.FunctionObjectRepo;
import com.viettel.common.Const;
import com.viettel.common.DataLogin;
import com.viettel.fw.common.util.AESUtil;
import com.viettel.fw.common.util.ErrorCode;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

@Service
public class AuthenticationServiceImpl extends BaseServiceImpl implements AuthenticationService {
    private static final Logger LOGGER = Logger.getLogger(AuthenticationService.class);
    private final BaseMapper<Employee, EmployeeDTO> chainUserMapper = new BaseMapper<>(Employee.class, EmployeeDTO.class);
    private final BaseMapper<FunctionObject, FunctionObjectDTO> functionObjectMapper = new BaseMapper<>(FunctionObject.class, FunctionObjectDTO.class);
    @PersistenceContext(unitName = com.viettel.common.Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;
    @Autowired
    private ChangePasswordRepo changePasswordRepo;
    @Autowired
    private FunctionObjectRepo functionObjectRepo;


    @WebMethod
    public EmployeeDTO findOne(Long id) throws Exception {
        return chainUserMapper.toDtoBean(changePasswordRepo.findOne(id));
    }

    @Override
    public DataLogin login(String username, String password) throws Exception {
        Date currentDate = getSysDate(em);
        DataLogin dataLogin = new DataLogin();
        dataLogin.setReturnCode(ErrorCode.SUCCESSFUL);
        EmployeeDTO employeeDTO;
        List<FunctionObjectDTO> menuDTO;
        try {
            BooleanExpression booleanExpression = QEmployee.employee.userName.equalsIgnoreCase(username);
            List<EmployeeDTO> chainUserDTOList = chainUserMapper.toDtoBean((List<Employee>) changePasswordRepo.findAll(booleanExpression));
            // username not exist
            if (chainUserDTOList.isEmpty()) {
                dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_NOT_EXIST);
            } else if (chainUserDTOList.size() > 1) { // trung du lieu
                dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_DUPLICATE_OBJECT);
            } else {// loi khac
                employeeDTO = chainUserDTOList.get(0);
                // trang thai nguoi dung
                if (employeeDTO.getActiveStatus() == 0) {
                    dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_IN_ACTIVE);
                } else
                    //validate password
                    if (!AESUtil.validatePassword(password, employeeDTO.getSalt(), employeeDTO.getPassword())) {
                        dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_PASS_INVALID);
                    } else {
                        // luu log dang nhap
                        employeeDTO.setLastLoginTime(currentDate);
                        changePasswordRepo.save(chainUserMapper.toPersistenceBean(employeeDTO));

                        // lay danh sach chuc nang
                        if (dataLogin.getReturnCode().equals(ErrorCode.SUCCESSFUL)) {
                            menuDTO = functionObjectMapper.toDtoBean(functionObjectRepo.getMenuByUserId(employeeDTO.getEmployeeId()));
                            if (null == menuDTO || menuDTO.isEmpty()) {
                                dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_NO_PERMISSION);
                            } else {
                                dataLogin.setMenu(menuDTO);
                                employeeDTO.setPassword(null);
                                employeeDTO.setLastLoginTime(null);
                                employeeDTO.setSalt(null);
                                dataLogin.setProfile(employeeDTO);
                                //check expire password here
                                // lay config expire trong apdomain
                                if (null == employeeDTO.getLastChangePasswordDate()) {
                                    dataLogin.setBlChangePassword(Const.WEB_CHAIN.BL_CHANGE_PASSWORD_TRUE);
                                } else if (null != employeeDTO.getLastChangePasswordDate() && (currentDate.getTime() - employeeDTO.getLastChangePasswordDate().getTime() > java.util.concurrent.TimeUnit.DAYS.toMillis(82))) {
                                    dataLogin.setBlChangePassword(Const.WEB_CHAIN.BL_CHANGE_PASSWORD_TRUE);
                                } else {
                                    dataLogin.setBlChangePassword(Const.WEB_CHAIN.BL_CHANGE_PASSWORD_FALSE);
                                }
                            }
                        }
                        //
                    }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
        return dataLogin;
    }



}
