package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.bankplus.webchain.model.*;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import com.viettel.bankplus.webchain.repo.ChangePasswordRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.AESUtil;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebMethod;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author daibq
 * @// TODO: 7/8/2018
 */
@Service
public class ChangePasswordServiceImpl extends BaseServiceImpl implements ChangePasswordService {
    private Date current;
    @PersistenceContext(unitName = com.viettel.common.Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;
    BaseMapper<Employee, EmployeeDTO> mapperem = new BaseMapper<>(Employee.class, EmployeeDTO.class);
    private final BaseMapper<ActionLog, ActionLogDTO> mapperActionLog = new BaseMapper<>(ActionLog.class, ActionLogDTO.class);
    @Autowired
    private ChangePasswordRepo repo;

    public static final Logger LOGGER = Logger.getLogger(ChangePasswordService.class);
    @Autowired
    private ActionLogRepo actionLogRepo;
    @Autowired
    private ActionLogDetailService actionLogDetailService;
    @Autowired
    private ActionLogDetailRepo actionLogDetailRepo;

    @Override
    public EmployeeDTO findOne(Long id) throws Exception {
        return mapperem.toDtoBean(repo.findOne(id));
    }

    @Override
    public BaseMessage changePassword(String newPassword, String userName, String takeSalt, long id, ActionLogDTO actionLogDTO) throws Exception {
        try {
            current = getSysDate(em);
            EmployeeDTO employeeDTO;
            Employee employeeOld = new Employee();
            String salt = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            employeeDTO = findOne(id);
            BeanUtils.copyProperties(employeeDTO, employeeOld);
            String password = AESUtil.createHash(newPassword, salt);
            employeeDTO.setPassword(password);
            employeeDTO.setLastChangePasswordDate(current);
            employeeDTO.setSalt(salt);
            employeeDTO.setUpdateDate(current);
            employeeDTO.setUpdateUser(userName);
            Employee employee = repo.saveAndFlush(mapperem.toPersistenceBean(employeeDTO));

            /* Write Log*/
            actionLogDTO.setUserName(userName);
            actionLogDTO.setCreateDate(current);
            actionLogDTO.setAction("UPDATE CHAIN_USER");
            actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + id);
            actionLogDTO.setObjectId(id);
            ActionLog actionLog = actionLogRepo.save(mapperActionLog.toPersistenceBean(actionLogDTO));

            List<ActionLogDetail> actionLogDetailList = actionLogDetailService.getActionLogDetailList(actionLog, employee, employeeOld, null, current, true);
            actionLogDetailRepo.save(actionLogDetailList);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return new BaseMessage(true);
    }

    @Override
    public boolean checkPassword(String oldPassword, String takeSalt) throws Exception {
        return repo.checkPassword(oldPassword, takeSalt);
    }

    @Override
    public String takeSaltWithUser(String userName) throws Exception {
        return repo.takeSaltWithUser(userName);
    }


    @Override
    public boolean checkDuplicatePassword(String newPassword, String takeSalt, String userName) throws Exception {
        return repo.checkDuplicatePassword(newPassword, takeSalt, userName);
    }

    @Override
    public Date getSysDate() throws Exception {
        return DbUtil.getSysDate(em);
    }

    @Override
    public long findId(String userName) throws Exception {
        return repo.findId(userName);
    }
    @Override
    public EmployeeDTO getValueByUserName(String userName) throws Exception {
        return repo.getValueByUserName(userName);
    }
    @Override
    public Date getLastDateChangePass(String userName) {
        return repo.getLastDateChangePass(userName);
    }


}
