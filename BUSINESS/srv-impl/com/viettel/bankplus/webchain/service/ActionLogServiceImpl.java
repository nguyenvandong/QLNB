package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.input.InputLazyActionLogSearch;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.service.BaseServiceImpl;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import java.util.List;

@Service
public class ActionLogServiceImpl extends BaseServiceImpl implements ActionLogService {

    private final BaseMapper<ActionLog,ActionLogDTO> mapper = new BaseMapper<>(ActionLog.class,ActionLogDTO.class);

    @Autowired
    private ActionLogRepo repo;
    public static final Logger logger = Logger.getLogger(ActionLogService.class);

    @WebMethod
    public Long count(List<FilterRequest> filters) throws Exception {
        return repo.count(repo.toPredicate(filters));
    }

    @WebMethod
    public ActionLogDTO findOne(Long id) throws Exception {
        return mapper.toDtoBean(repo.findOne(id));
    }

    @WebMethod
    public List<ActionLogDTO> findByFilter(List<FilterRequest> filters) throws Exception {
        return mapper.toDtoBean(repo.findAll(repo.toPredicate(filters)));
    }

    @Override
    public int countList(InputLazyActionLogSearch input) throws Exception {
        return repo.countList(input);
    }

    @Override
    public List<ActionLogDTO> findLazingPaging(InputLazyActionLogSearch input, int pageSize, int pageNumber) throws Exception {
        return repo.findLazingPaging(input, pageSize, pageNumber);
    }

}
