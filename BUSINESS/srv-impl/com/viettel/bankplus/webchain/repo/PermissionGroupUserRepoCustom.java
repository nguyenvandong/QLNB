package com.viettel.bankplus.webchain.repo;


import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.bankplus.webchain.dto.PermissionGroupUserDTO;
import com.viettel.bankplus.webchain.input.InputLazyUserPermissionGroup;

import java.util.List;

/**
 * @author hungnq on 9/7/2017
 */
public interface PermissionGroupUserRepoCustom {

    public int countUserPermissionGroup(InputLazyUserPermissionGroup inputLazyUserPermissionGroup);

    List<PermissionGroupUserDTO> findUserPermissionGroup(InputLazyUserPermissionGroup inputLazyUserPermissionGroup, int pageSize, int pageNumber);


    /*public List<PermissionGroupDTO> checkPermissionGroupTypOrStartus(Long permissionGroupId);*/

}
