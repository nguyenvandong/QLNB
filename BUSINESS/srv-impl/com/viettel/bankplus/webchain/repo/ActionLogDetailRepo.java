package com.viettel.bankplus.webchain.repo;

import com.viettel.fw.persistence.BaseRepository;
import com.viettel.bankplus.webchain.model.ActionLogDetail;

public interface ActionLogDetailRepo extends BaseRepository<ActionLogDetail, Long>, ActionLogDetailRepoCustom {

}