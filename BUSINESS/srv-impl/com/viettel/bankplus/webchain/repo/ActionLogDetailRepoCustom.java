package com.viettel.bankplus.webchain.repo;

import com.mysema.query.types.Predicate;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDetailDTO;
import com.viettel.fw.common.util.extjs.FilterRequest;

import java.util.List;

public interface ActionLogDetailRepoCustom {

  Predicate toPredicate(List<FilterRequest> filters);

  int countList(ActionLogDTO dto) throws Exception;

  List<ActionLogDetailDTO> findLazingPaging(ActionLogDTO dto, int pageSize, int pageNumber) throws Exception;

}