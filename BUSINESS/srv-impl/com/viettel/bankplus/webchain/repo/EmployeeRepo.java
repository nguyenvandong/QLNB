package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.model.Employee;
import com.viettel.fw.persistence.BaseRepository;

/**
 * @author daibq
 * @// TODO: 7/8/2018
 */

public interface EmployeeRepo extends BaseRepository<Employee, Long>, EmployeeRepoCustom {

}