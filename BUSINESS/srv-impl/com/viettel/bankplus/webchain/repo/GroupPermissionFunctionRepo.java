package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.model.PermissionGroupFunction;
import com.viettel.fw.persistence.BaseRepository;

import java.util.List;

/**
 * Created by truon on 06-09-2017.
 */
public interface GroupPermissionFunctionRepo extends BaseRepository<PermissionGroupFunction, Long>, GroupPermissionFunctionRepoCustom {
    List<PermissionGroupFunction> findByPermissionGroupIdAndStatus(Long id, Short status) throws Exception;
}
