package com.viettel.bankplus.webchain.repo;


import com.viettel.bankplus.webchain.model.Area;
import com.viettel.fw.persistence.BaseRepository;

public interface AreaRepo extends BaseRepository<Area, Long>, AreaRepoCustom {

}