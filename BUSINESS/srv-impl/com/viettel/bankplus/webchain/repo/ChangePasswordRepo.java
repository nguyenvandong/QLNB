package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.model.Employee;
import com.viettel.fw.persistence.BaseRepository;
/**
 * @author daibq
 * @// TODO: 7/8/2018
 */

public interface ChangePasswordRepo extends BaseRepository<Employee, Long>, ChangePasswordRepoCustom {

}