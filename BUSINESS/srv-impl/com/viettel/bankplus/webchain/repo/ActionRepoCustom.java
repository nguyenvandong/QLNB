package com.viettel.bankplus.webchain.repo;
import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.input.InputLazyActionSearch;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.mysema.query.types.Predicate;

import java.text.ParseException;
import java.util.List;
public interface ActionRepoCustom {
  Predicate toPredicate(List<FilterRequest> filters);

  int countActionList(InputLazyActionSearch input);

  List<ActionDTO> findLazingPaging (InputLazyActionSearch input, int pageSize, int pageNumber) throws ParseException, Exception;

  List<ActionDTO> getValueActionByTypeOrderByCode(String type) throws Exception;

  List<ActionDTO> getValueActionByType(String type) throws Exception;

  public List<ActionDTO> getListActionDTOByType(String type) throws Exception;

  List<ActionDTO> getActionOrderByName() throws Exception;
}