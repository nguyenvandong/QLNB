package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.AreaDTO;
import com.viettel.bankplus.webchain.model.Area;

import java.util.List;

public interface AreaRepoCustom {

    public List<Area> findByAreaCode(String code) throws Exception;

    public List<AreaDTO> getListDistrictByProvinceCode(String provinceCode) throws Exception;

    public List<AreaDTO> getPrecinctListByDistrictCode(String districtCode) throws Exception;

    public List<AreaDTO> getProvinceList() throws Exception;

    public List<AreaDTO> exportArea() throws Exception;

}