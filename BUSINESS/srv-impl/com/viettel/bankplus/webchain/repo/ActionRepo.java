package com.viettel.bankplus.webchain.repo;
import com.viettel.bankplus.webchain.model.Action;
import com.viettel.fw.persistence.BaseRepository;

import java.lang.Long;

public interface ActionRepo extends BaseRepository<Action, Long>, ActionRepoCustom {

}