package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.dto.GroupPermissionFunctionDTO;
import com.viettel.bankplus.webchain.input.InputLazyPermissionGroupFunctionSearch;

import java.util.List;

/**
 * Created by truon on 06-09-2017.
 */
public interface GroupPermissionFunctionRepoCustom {
    public int countLazyGroupFunction(InputLazyPermissionGroupFunctionSearch inputSearch);

    public List<GroupPermissionFunctionDTO> findLazyGroupFunction(InputLazyPermissionGroupFunctionSearch inputSearch, int pageSize, int pageNumber);
}
