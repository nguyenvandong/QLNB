package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.model.PermissionGroupFunction;
import com.viettel.bankplus.webchain.model.PermissionGroupUser;
import com.viettel.fw.persistence.BaseRepository;

import java.util.List;

/**
 * @author hungnq on 9/7/2017
 */
public interface PermissionGroupUserRepo extends BaseRepository<PermissionGroupUser, Long>, PermissionGroupUserRepoCustom {
    List<PermissionGroupUser> findByPermissionGroupIdAndStatus(Long id, Short status) throws Exception;
}
