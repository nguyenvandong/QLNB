package com.viettel.bankplus.webchain.repo;


import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.fw.common.util.AESUtil;
import com.viettel.fw.common.util.DataUtil;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author daibq
 * @// TODO: 7/8/2018
 */
public class ChangePasswordRepoImpl implements ChangePasswordRepoCustom {

    public static final Logger logger = Logger.getLogger(ChangePasswordRepoCustom.class);
    @PersistenceContext(unitName = com.viettel.common.Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Override
    public boolean checkPassword(String oldPassword, String takeSalt) throws Exception {
        boolean cPass;
        String password = AESUtil.createHash(oldPassword, takeSalt);
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT count(*) FROM EMPLOYEE WHERE 1=1 AND PASSWORD = #password");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("password", password);
        int total = DataUtil.safeToInt(query.getSingleResult());
        if (total > 0) {
            cPass = true;
        } else
            cPass = false;
        return cPass;
    }

    @Override
    public String takeSaltWithUser(String userName) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT SALT FROM EMPLOYEE WHERE 1=1 AND USER_NAME = #userName ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("userName", userName);
        String result = DataUtil.safeToString(query.getSingleResult());
        return result;
    }


    @Override
    public boolean checkDuplicatePassword(String newPassword, String takeSalt, String userName) throws Exception {
        boolean dupliPass;
        String password = AESUtil.createHash(newPassword, takeSalt);
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT COUNT(*) FROM EMPLOYEE WHERE 1=1 AND PASSWORD = #password AND USER_NAME = #userName");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("password", password);
        query.setParameter("userName", userName);
        int total = DataUtil.safeToInt(query.getSingleResult());
        if (total > 0) {
            dupliPass = true;
        } else
            dupliPass = false;
        return dupliPass;
    }

    @Override
    public Date getLastDateChangePass(String userName) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT LAST_CHANGE_PASSWORD_DATE FROM EMPLOYEE WHERE USER_NAME = #userName ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("userName", userName);
        return (Date) query.getSingleResult();
    }

    @Override
    public long findId(String userName) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT EMPLOYEE_ID FROM EMPLOYEE WHERE 1=1 AND USER_NAME = #userName ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("userName", userName);
        long total = DataUtil.safeToLong(query.getSingleResult());
        return total;
    }

    @Override
    public EmployeeDTO getValueByUserName(String userName) throws Exception {
        try {
            if (DataUtil.isNullObject(userName)) {
                return null;
            }
            EmployeeDTO employeeDTO = null;
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT CU.PHONE_NUMBER FROM CHAIN_USER CU WHERE 1=1 AND LOWER(CU.user_name)= #userName");
            Query query = em.createNativeQuery(sql.toString());
            query.setParameter("userName",userName.toLowerCase());
            List lstTemp = query.getResultList();
            if (lstTemp != null && !DataUtil.isNullOrEmpty(lstTemp)) {
                Object[] obj = (Object[]) lstTemp.get(0);
                employeeDTO = new EmployeeDTO();
                employeeDTO.setPhoneNumber(DataUtil.safeToString(obj[0]));

            }
            return employeeDTO;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }


/*    @Override
    public List<ChainUserDTO> getUserByInput(String input) {
        StringBuilder sql = new StringBuilder();
        List<ChainUserDTO> userList = new ArrayList<>();
        sql.append(" SELECT CHAIN_USER_ID, USER_NAME, FULL_NAME FROM CHAIN_USER WHERE LOWER(USER_NAME) LIKE #input ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("input", "%" + input + "%");
        List lstTemp = query.getResultList();
        if (null != lstTemp && !DataUtil.isNullOrEmpty(lstTemp)) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                ChainUserDTO chainUserDTO = new ChainUserDTO();
                chainUserDTO.setChainUserId(DataUtil.safeToLong(obj[0]));
                chainUserDTO.setUserName(DataUtil.safeToString(obj[1]));
                chainUserDTO.setFullName(DataUtil.safeToString(obj[2]));
                userList.add(chainUserDTO);
            }
        }
        return userList;
    }*/


}