package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.input.InputLazyEmpSearch;
import com.viettel.bankplus.webchain.model.Employee;
import com.viettel.fw.common.util.DataUtil;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * @author daibq
 * @// TODO: 7/8/2018
 */
public class EmployeeRepoImpl implements EmployeeRepoCustom {

    public static final Logger logger = Logger.getLogger(ChangePasswordRepoCustom.class);
    @PersistenceContext(unitName = com.viettel.common.Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;


    @Override
    public int countLazyEmp (InputLazyEmpSearch inputLazyEmpSearch) {
        StringBuilder sql = new StringBuilder(500);
        sql.append(" SELECT COUNT(*)");
        sql.append(" FROM ");
        sql.append(" EMPLOYEE em ");
        sql.append(" WHERE ");
        sql.append(" 1 = 1  ");
        List paramList = new ArrayList();
        if (!DataUtil.isNullOrEmpty(inputLazyEmpSearch.getEmployeeCode())) {
            sql.append(" AND LOWER(em.EMPLOYEE_CODE) LIKE?  ");
            String empCode = "%" + inputLazyEmpSearch.getEmployeeCode().toLowerCase().trim() + "%";
            paramList.add(empCode);
        }
        if (!DataUtil.isNullOrEmpty(inputLazyEmpSearch.getFullName())) {
            sql.append(" AND LOWER(em.FULL_NAME) LIKE? ");
            String name = "%" + inputLazyEmpSearch.getFullName().toLowerCase().trim() + "%";
            paramList.add(name);
        }
        if (!DataUtil.isNullObject(inputLazyEmpSearch.getEmployeeType())) {
            sql.append(" AND LOWER(em.EMPLOYEE_TYPE) LIKE? ");
            Short empType = inputLazyEmpSearch.getEmployeeType();
            paramList.add(empType);
        }
        if (!DataUtil.isNullObject(inputLazyEmpSearch.getJobType())) {
            sql.append(" AND em.JOB_TYPE = ? ");
            paramList.add(inputLazyEmpSearch.getJobType());
        }
        if (!DataUtil.isNullOrEmpty(inputLazyEmpSearch.getUsername())) {
            sql.append(" AND LOWER(em.USER_NAME) LIKE? ");
            String username = "%" + inputLazyEmpSearch.getUsername().toLowerCase().trim()+"%"   ;
            paramList.add(username);
        }
        if (!DataUtil.isNullObject(inputLazyEmpSearch.getSalary())) {
            sql.append(" AND em.SALARY = LIKE? ");
            paramList.add("%" + inputLazyEmpSearch.getSalary()+"%");
        }

        sql.append(" ORDER BY em.EMPLOYEE_ID desc");

        Query query = em.createNativeQuery(sql.toString());
        if (!paramList.isEmpty()) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        return DataUtil.safeToInt(query.getSingleResult());
    }

    @Override
    public List<Employee> findLazyEmp(InputLazyEmpSearch inputLazyEmpSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception {
        try {
            StringBuilder sql = new StringBuilder(500);
            sql.append(" SELECT em.* ");
            sql.append(" FROM ");
            sql.append(" EMPLOYEE em ");
            sql.append(" WHERE ");
            sql.append(" 1 = 1  ");
            List paramList = new ArrayList();
            if (!DataUtil.isNullOrEmpty(inputLazyEmpSearch.getEmployeeCode())) {
                sql.append(" AND LOWER(em.EMPLOYEE_CODE) LIKE?  ");
                String empCode = "%" + inputLazyEmpSearch.getEmployeeCode().toLowerCase().trim() + "%";
                paramList.add(empCode);
            }
            if (!DataUtil.isNullOrEmpty(inputLazyEmpSearch.getFullName())) {
                sql.append(" AND LOWER(em.FULL_NAME) LIKE? ");
                String name = "%" + inputLazyEmpSearch.getFullName().toLowerCase().trim() + "%";
                paramList.add(name);
            }
            if (!DataUtil.isNullObject(inputLazyEmpSearch.getEmployeeType())) {
                sql.append(" AND LOWER(em.EMPLOYEE_TYPE) LIKE? ");
                Short empType = inputLazyEmpSearch.getEmployeeType();
                paramList.add(empType);
            }
            if (!DataUtil.isNullObject(inputLazyEmpSearch.getJobType())) {
                sql.append(" AND em.JOB_TYPE = ? ");
                paramList.add(inputLazyEmpSearch.getJobType());
            }
            if (!DataUtil.isNullOrEmpty(inputLazyEmpSearch.getUsername())) {
                sql.append(" AND LOWER(em.USER_NAME) LIKE? ");
                String username = "%" + inputLazyEmpSearch.getUsername().toLowerCase().trim()+"%"   ;
                paramList.add(username);
            }
            if (!DataUtil.isNullObject(inputLazyEmpSearch.getSalary())) {
                sql.append(" AND em.SALARY = LIKE? ");
                paramList.add("%" + inputLazyEmpSearch.getSalary()+"%");
            }

            sql.append(" ORDER BY em.EMPLOYEE_ID desc");

            Query query = em.createNativeQuery(sql.toString(), Employee.class);
            if (!paramList.isEmpty()) {
                for (int i = 0; i < paramList.size(); i++) {
                    query.setParameter(i + 1, paramList.get(i));
                }
            }
            if (isLazySearch) {
                query.setFirstResult(pageNumber * pageSize);
            }
            List<Employee> employeeList = query.getResultList();
            return employeeList;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

}