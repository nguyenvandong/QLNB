package com.viettel.bankplus.webchain.repo;

import com.google.common.collect.Lists;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.template.BooleanTemplate;
import com.viettel.bankplus.webchain.dto.AreaDTO;
import com.viettel.bankplus.webchain.model.Area;
import com.viettel.bankplus.webchain.model.QArea;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AreaRepoImpl implements AreaRepoCustom {

    public static final Logger logger = Logger.getLogger(AreaRepoCustom.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    /**
     * @param code
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public List<Area> findByAreaCode(String code) throws Exception {
        StringBuilder strQuery = new StringBuilder(200);
        strQuery.append("select a FROM Area a WHERE a.areaCode = #p1 AND a.status = 1");
        Query query = em.createQuery(strQuery.toString());
        query.setParameter("p1", code);
        return query.getResultList();
    }


    @Override
    public List<AreaDTO> getListDistrictByProvinceCode(String provinceCode) throws Exception {
        List<AreaDTO> listLocation = Lists.newArrayList();
        try {
            StringBuilder strQuery = new StringBuilder(500);
            strQuery.append(" select a.area_code,a.name from area a ");
            strQuery.append(" where a.PARENT_CODE = #provinceCode ");
            strQuery.append(" and a.status =1 ");
            strQuery.append(" order by a.province,NLSSORT(a.name,'NLS_SORT = VIETNAMESE') ");
            Query query = em.createNativeQuery(strQuery.toString());
            query.setParameter("provinceCode", provinceCode);
            List<Object[]> listResult = query.getResultList();

            for (Object[] obj : listResult) {
                AreaDTO location = new AreaDTO();
                location.setAreaCode(DataUtil.safeToString(obj[0]));
                location.setName(DataUtil.safeToString(obj[1]));
                listLocation.add(location);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
        return listLocation;
    }

    @Override
    public List<AreaDTO> getPrecinctListByDistrictCode(String districtCode) throws Exception {
        List<AreaDTO> listLocation = Lists.newArrayList();
        try {
            StringBuilder strQuery = new StringBuilder();

            strQuery.append("  select a.area_code,a.name from area a ");
            strQuery.append("  where a.PARENT_CODE = #districtCode ");
            strQuery.append(" and a.status =1 ");
            strQuery.append("  order by NLSSORT(a.name,'NLS_SORT = VIETNAMESE')  ");

            Query query = em.createNativeQuery(strQuery.toString());
            query.setParameter("districtCode", districtCode);
            query.setMaxResults(1000);
            List<Object[]> listResult = query.getResultList();
            if (listResult == null || listResult.isEmpty()) {
                return listLocation;
            }
            for (Object[] obj : listResult) {
                AreaDTO location = new AreaDTO();
                location.setName(DataUtil.safeToString(obj[1]));
                location.setAreaCode(DataUtil.safeToString(obj[0]));
                listLocation.add(location);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
        return listLocation;
    }

    @Override
    public List<AreaDTO> getProvinceList() throws Exception {
        List<AreaDTO> provinceList = Lists.newArrayList();
        StringBuilder strQuery = new StringBuilder(200);
        strQuery.append("  select a.name, a.area_code from area a ");
        strQuery.append("  where a.PARENT_CODE is null ");
        strQuery.append(" and a.status =1 ");
        strQuery.append("  order by NLSSORT(a.name,'NLS_SORT = VIETNAMESE')  ");
        Query query = em.createNativeQuery(strQuery.toString());
        query.setMaxResults(1000);
        List<Object[]> listResult = query.getResultList();
        for (Object[] obj : listResult) {
            AreaDTO areaDTO = new AreaDTO();
            areaDTO.setAreaCode(DataUtil.safeToString(obj[1]));
            areaDTO.setName(DataUtil.safeToString(obj[0]));
            provinceList.add(areaDTO);
        }
        return provinceList;
    }

    @Override
    public List<AreaDTO> exportArea() throws Exception {
        List<AreaDTO> areaDTOList = new ArrayList<>();
        StringBuilder sql = new StringBuilder(200);
        sql.append(" SELECT  ");
        sql.append(" av.PROVINDE_CODE, ");
        sql.append(" av.PROVINCE_NAME, ");
        sql.append(" av.DISTRICT_CODE, ");
        sql.append(" av.DISTRICT_NAME, ");
        sql.append(" av.PRECINCT_CODE, ");
        sql.append(" av.PRECINCT_NAME  ");
        sql.append("FROM ");
        sql.append(" AREA_VIEW av  ");
        sql.append("WHERE ");
        sql.append(" PRECINCT_CODE IS NOT NULL  ");
        sql.append(" AND PRECINCT_NAME IS NOT NULL ");
        Query query = em.createNativeQuery(sql.toString());
        query.setMaxResults(1000);
        List<Object[]> listResult = query.getResultList();
        for (Object[] obj : listResult) {
            AreaDTO areaDTO = new AreaDTO();
            //province_code
            areaDTO.setProvince(DataUtil.safeToString(obj[0]));
            //province_name
            areaDTO.setProvinceName(DataUtil.safeToString(obj[1]));
            //district_code
            areaDTO.setDistrict(DataUtil.safeToString(obj[2]));
            //district_name
            areaDTO.setDistrictName(DataUtil.safeToString(obj[3]));
            //precint_code
            areaDTO.setPrecinct(DataUtil.safeToString(obj[4]));
            //precint_name
            areaDTO.setPrecinctName(DataUtil.safeToString(obj[4]));
            areaDTOList.add(areaDTO);
        }
        return areaDTOList;
    }

}