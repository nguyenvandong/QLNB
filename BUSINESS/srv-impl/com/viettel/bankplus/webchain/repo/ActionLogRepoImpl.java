package com.viettel.bankplus.webchain.repo;

import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.template.BooleanTemplate;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.input.InputLazyActionLogSearch;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DateUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.model.QActionLog;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class ActionLogRepoImpl implements ActionLogRepoCustom {

    public static final Logger logger = Logger.getLogger(ActionLogRepoCustom.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Override
    public Predicate toPredicate(List<FilterRequest> filters) {
        logger.info("Entering predicates :: " + filters);
        QActionLog actionLog = QActionLog.actionLog;
        BooleanExpression result = BooleanTemplate.create("1 = 1");
        try {
            for (FilterRequest filter : filters) {
                ActionLog.COLUMNS column = ActionLog.COLUMNS.valueOf(filter.getProperty().toUpperCase());
                BooleanExpression expression = null;
                switch (column) {
                    case ACTION:
                        expression = DbUtil.createStringExpression(actionLog.action, filter);
                        break;
                    case ACTIONID:
                        expression = DbUtil.createLongExpression(actionLog.actionId, filter);
                        break;
                    case ACTIONLOGID:
                        expression = DbUtil.createLongExpression(actionLog.actionLogId, filter);
                        break;
                    case ACTIONTYPE:
                        expression = DbUtil.createStringExpression(actionLog.actionType, filter);
                        break;
                    case CREATEDATE:
                        expression = DbUtil.createDateExpression(actionLog.createDate, filter);
                        break;
                    case DESCRIPTION:
                        expression = DbUtil.createStringExpression(actionLog.description, filter);
                        break;
                    case OBJECTID:
                        expression = DbUtil.createLongExpression(actionLog.objectId, filter);
                        break;
                    case OBJECTTYPE:
                        expression = DbUtil.createStringExpression(actionLog.objectType, filter);
                        break;

                    case USERNAME:
                        expression = DbUtil.createStringExpression(actionLog.userName, filter);
                        break;
                    default:break;
                }
                if (expression != null) {
                        result = result.and(expression);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
        }
        logger.info("Result Predicate :: " + (result != null ? result.toString() : ""));
        logger.info("Exiting predicates");
        return result;
    }

    @Override
    public int countList(InputLazyActionLogSearch input) {
        List paramList = new ArrayList();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT count(*) ");
        sql.append(" FROM ACTION_LOG P INNER JOIN ACTION B ON P.ACTION_ID = B.ACTION_ID WHERE 1=1 ");

        if (!DataUtil.isNullOrEmpty(input.getUserName())) {
            sql.append(" AND LOWER(P.USER_NAME) LIKE? ESCAPE '\\' ");
            String Uname = "%" + DataUtil.formatQueryString(input.getUserName())  + "%";
            paramList.add(Uname);
        }

        if (!DataUtil.isNullOrEmpty(input.getType())) {
            sql.append(" AND B.ACTION_ID = ? AND B.STATUS = 1 ");
            paramList.add(input.getType());
        }

        sql.append(" ORDER BY P.CREATE_DATE DESC");

        Query query = em.createNativeQuery(sql.toString());
        if (!paramList.isEmpty()) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        int total = DataUtil.safeToInt(query.getSingleResult());
        return total;
    }

    @Override
    public List<ActionLogDTO> findLazingPaging(InputLazyActionLogSearch input, int pageSize, int pageNumber) {
        List paramList = new ArrayList();
        StringBuilder sql = new StringBuilder();
        List<ActionLogDTO> listDTO = new ArrayList<>();
        sql.append("SELECT A.ACTION_LOG_ID, A.USER_NAME, A.ACTION_TYPE, A.OBJECT_ID, A.OBJECT_TYPE, A.DESCRIPTION, A.ACTION_ID, TO_CHAR (A.CREATE_DATE,'dd/MM/yyyy hh24:mi:ss'), A.ACTION");
        sql.append(" FROM ACTION_LOG A INNER JOIN ACTION B ON A.ACTION_ID = B.ACTION_ID where B.STATUS = 1 AND 1=1 ");

        if (!DataUtil.isNullOrEmpty(input.getUserName())) {
            sql.append(" AND LOWER(A.USER_NAME) LIKE? ESCAPE '\\' ");
            String Uname = "%" + DataUtil.formatQueryString(input.getUserName()) + "%";
            paramList.add(Uname);
        }
        if (!DataUtil.isNullOrEmpty(input.getType())) {
            sql.append(" AND B.ACTION_ID = ?");
            paramList.add(input.getType());
        }

        sql.append(" ORDER BY A.CREATE_DATE DESC");

        Query query = em.createNativeQuery(sql.toString());
        if (!paramList.isEmpty()) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize);
        List lstTemp = query.getResultList();
        if (lstTemp != null && !lstTemp.isEmpty()) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                ActionLogDTO dto = new ActionLogDTO();
                try{
                    dto.setActionLogId(DataUtil.safeToLong(obj[0]));
                    dto.setUserName(DataUtil.safeToString(obj[1]));
                    dto.setActionType(DataUtil.safeToString(obj[2]));
                    dto.setObjectId(DataUtil.safeToLong(obj[3]));
                    dto.setObjectType(DataUtil.safeToString(obj[4]));
                    dto.setDescription(DataUtil.safeToString(obj[5]));
                    dto.setActionId(DataUtil.safeToLong(obj[6]));
                    dto.setCreateDate(DateUtil.convertStringToTime(DataUtil.safeToString(obj[7]), "dd/MM/yyyy HH:mm:ss"));
                    dto.setAction(DataUtil.safeToString(obj[8]));
                    listDTO.add(dto);
                }catch (Exception e){
                    logger.error(e.getMessage(),e);
                }
            }
        }

        return listDTO;
    }

}