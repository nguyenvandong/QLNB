package com.viettel.bankplus.webchain.repo;

import com.viettel.fw.persistence.BaseRepository;
import com.viettel.bankplus.webchain.model.ActionLog;

public interface ActionLogRepo extends BaseRepository<ActionLog, Long>, ActionLogRepoCustom {

}