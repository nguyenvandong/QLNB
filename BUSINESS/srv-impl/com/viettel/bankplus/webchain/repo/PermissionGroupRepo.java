package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.model.PermissionGroup;
import com.viettel.fw.persistence.BaseRepository;

import java.util.List;


public interface PermissionGroupRepo extends BaseRepository<PermissionGroup, Long>, PermissionGroupRepoCustom {

}