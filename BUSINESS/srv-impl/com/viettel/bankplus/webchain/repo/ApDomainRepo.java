package com.viettel.bankplus.webchain.repo;

import com.viettel.fw.persistence.BaseRepository;
import com.viettel.bankplus.webchain.model.ApDomain;
/**
 * @author daibq
 * @// TODO: 7/8/2018
 */
public interface ApDomainRepo extends BaseRepository<ApDomain, Long>, ApDomainRepoCustom {

}