package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.dto.GroupPermissionFunctionDTO;
import com.viettel.bankplus.webchain.input.InputLazyPermissionGroupFunctionSearch;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by truon on 06-09-2017.
 */
public class GroupPermissionFunctionRepoImpl implements GroupPermissionFunctionRepoCustom {

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Override
    public int countLazyGroupFunction(InputLazyPermissionGroupFunctionSearch inputSearch) {
        String groupCode = DataUtil.formatQueryString(inputSearch.getGroupCode());
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(" (SELECT pgf.PERMISSION_GROUP_FUNCTION_ID, pgf.PERMISSION_GROUP_ID, pg.PERMISSION_GROUP_CODE, pg.PERMISSION_GROUP_NAME,pgf.STATUS,fo.FUNCTION_NAME,fo.PARENT_ID ");
        sql.append(" FROM PERMISSION_GROUP_FUNCTION pgf, PERMISSION_GROUP pg, FUNCTION_OBJECT fo ");
        sql.append(" WHERE pg.STATUS=1 AND fo.STATUS=1 ");
        if(inputSearch.getShopId()!=1){
            sql.append(" AND pg.CHAIN_SHOP_ID=#shopId ");
        }
        if (!DataUtil.isNullOrEmpty(groupCode)) {
            sql.append("  AND LOWER(pg.PERMISSION_GROUP_CODE) LIKE #permissionCode ESCAPE '\\' ");
        }
        if (inputSearch.getStatus() != null) {
            sql.append("  AND pgf.STATUS = #status ");
        }

        sql.append(" AND pg.PERMISSION_GROUP_ID=pgf.PERMISSION_GROUP_ID AND pgf.FUNCTION_OBJECT_ID= fo.FUNCTION_OBJECT_ID) tb LEFT JOIN ");
        if(inputSearch.getShopId()!=1) {
            sql.append(" ( SELECT DISTINCT fo.* FROM FUNCTION_OBJECT fo, PERMISSION_GROUP_FUNCTION pgf, PERMISSION_GROUP_USER pgu, PERMISSION_GROUP pg ");
            sql.append(" WHERE fo.STATUS=1 AND pgu.STATUS=1 AND pg.STATUS=1 AND pgu.PERMISSION_GROUP_ID= pgf.PERMISSION_GROUP_ID ");
            sql.append("  AND pgf.FUNCTION_OBJECT_ID=fo.FUNCTION_OBJECT_ID AND pgu.CHAIN_USER_ID= #userId ) ");
        }else{
            sql.append(" FUNCTION_OBJECT ");
        }
        sql.append( " fo ON fo.FUNCTION_OBJECT_ID= tb.PARENT_ID AND fo.STATUS=1");

        Query query = em.createNativeQuery(sql.toString());
        if (!DataUtil.isNullOrEmpty(groupCode)) {
            query.setParameter("permissionCode", "%" + groupCode + "%");
        }
        if (inputSearch.getStatus() != null) {
            query.setParameter("status", inputSearch.getStatus());
        }
        if(inputSearch.getShopId()!=1) {
            query.setParameter("shopId", inputSearch.getShopId());
            query.setParameter("userId", inputSearch.getUserId());
        }
        return DataUtil.safeToInt(query.getSingleResult());
    }

    @Override
    public List<GroupPermissionFunctionDTO> findLazyGroupFunction(InputLazyPermissionGroupFunctionSearch inputSearch, int pageSize, int pageNumber) {
        String groupCode = DataUtil.formatQueryString(inputSearch.getGroupCode());
        StringBuilder sql = new StringBuilder("SELECT tb.*,fo.FUNCTION_NAME FROM ");
        sql.append(" (SELECT pgf.PERMISSION_GROUP_FUNCTION_ID, pgf.PERMISSION_GROUP_ID, pg.PERMISSION_GROUP_CODE, pg.PERMISSION_GROUP_NAME,pgf.STATUS,fo.FUNCTION_NAME,fo.PARENT_ID ");
        sql.append(" FROM PERMISSION_GROUP_FUNCTION pgf, PERMISSION_GROUP pg, FUNCTION_OBJECT fo ");
        sql.append(" WHERE pg.STATUS=1 AND fo.STATUS=1 ");
        if(inputSearch.getShopId()!=1){
            sql.append(" AND pg.CHAIN_SHOP_ID=#shopId ");
        }
        if (!DataUtil.isNullOrEmpty(groupCode)) {
            sql.append("  AND LOWER(pg.PERMISSION_GROUP_CODE) LIKE #permissionCode ESCAPE '\\' ");
        }
        if (inputSearch.getStatus() != null) {
            sql.append("  AND pgf.STATUS = #status ");
        }

        sql.append(" AND pg.PERMISSION_GROUP_ID=pgf.PERMISSION_GROUP_ID AND pgf.FUNCTION_OBJECT_ID= fo.FUNCTION_OBJECT_ID) tb LEFT JOIN ");
        if(inputSearch.getShopId()==1) {
            sql.append(" FUNCTION_OBJECT ");
        }else{
            sql.append(" ( SELECT DISTINCT fo.* FROM FUNCTION_OBJECT fo, PERMISSION_GROUP_FUNCTION pgf, PERMISSION_GROUP_USER pgu, PERMISSION_GROUP pg ");
            sql.append(" WHERE fo.STATUS=1 AND pgu.STATUS=1 AND pg.STATUS=1 AND pgu.PERMISSION_GROUP_ID= pgf.PERMISSION_GROUP_ID ");
            sql.append("  AND pgf.FUNCTION_OBJECT_ID=fo.FUNCTION_OBJECT_ID AND pgu.CHAIN_USER_ID= #userId ) ");
        }
        sql.append( " fo ON fo.FUNCTION_OBJECT_ID= tb.PARENT_ID AND fo.STATUS=1");

        sql.append(" ORDER BY tb.PERMISSION_GROUP_CODE ASC ");

        Query query = em.createNativeQuery(sql.toString());
        if (!DataUtil.isNullOrEmpty(groupCode)) {
            query.setParameter("permissionCode", "%" + groupCode + "%");
        }
        if (inputSearch.getStatus() != null) {
            query.setParameter("status", inputSearch.getStatus());
        }
        if(inputSearch.getShopId()!=1) {
            query.setParameter("shopId", inputSearch.getShopId());
            query.setParameter("userId", inputSearch.getUserId());
        }
        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize);

        List<GroupPermissionFunctionDTO> groupPermissionFunctionDTOList = new ArrayList<>();
        List<Object[]> lstTemp = query.getResultList();
        for (Object[] obj : lstTemp) {
            GroupPermissionFunctionDTO groupPermissionFunctionDTO = new GroupPermissionFunctionDTO();
            groupPermissionFunctionDTO.setPermissionFunctionId(DataUtil.safeToLong(obj[0]));
            groupPermissionFunctionDTO.setPermissionId(DataUtil.safeToLong(obj[1]));
            groupPermissionFunctionDTO.setPermissionCode(DataUtil.safeToString(obj[2]));
            groupPermissionFunctionDTO.setPermissionName(DataUtil.safeToString(obj[3]));
            groupPermissionFunctionDTO.setStatus(DataUtil.safeToShort(obj[4]));
            groupPermissionFunctionDTO.setFunctionName(DataUtil.safeToString(obj[5]));
            if (obj.length == 8) {
                groupPermissionFunctionDTO.setFunctionParentName(DataUtil.safeToString(obj[7]));
            }
            groupPermissionFunctionDTOList.add(groupPermissionFunctionDTO);
        }
        return groupPermissionFunctionDTOList;
    }
}
