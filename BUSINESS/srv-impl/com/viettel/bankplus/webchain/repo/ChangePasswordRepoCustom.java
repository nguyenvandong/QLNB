package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.EmployeeDTO;

import java.util.Date;

/**
 * @author daibq
 * @// TODO: 7/8/2018
 */
public interface ChangePasswordRepoCustom {
    public boolean checkPassword(String oldPassword, String takeSalt) throws Exception;

    public String takeSaltWithUser(String userName) throws Exception;

    boolean checkDuplicatePassword(String newPassword, String takeSalt, String userName) throws Exception;

    public Date getLastDateChangePass(String userName);

    public long findId(String userName) throws Exception;

    public EmployeeDTO getValueByUserName(String userName) throws Exception;

}