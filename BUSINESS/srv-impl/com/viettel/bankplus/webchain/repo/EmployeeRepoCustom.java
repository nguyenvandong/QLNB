package com.viettel.bankplus.webchain.repo;

import com.mysema.query.types.Predicate;
import com.viettel.bankplus.webchain.dto.EmployeeDTO;
import com.viettel.bankplus.webchain.input.InputLazyEmpSearch;
import com.viettel.bankplus.webchain.model.Employee;
import com.viettel.fw.common.util.extjs.FilterRequest;

import java.util.Date;
import java.util.List;

/**
 * @author daibq
 * @// TODO: 7/8/2018
 */
public interface EmployeeRepoCustom {

    public int countLazyEmp(InputLazyEmpSearch inputLazyEmpSearch);

    public List<Employee> findLazyEmp(InputLazyEmpSearch inputLazyEmpSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception;

}