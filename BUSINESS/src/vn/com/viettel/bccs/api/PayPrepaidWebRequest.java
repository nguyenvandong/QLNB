package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 11/21/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayPrepaidWebRequest")
public class PayPrepaidWebRequest {
    @JsonProperty("order_id")
    protected String orderId;
    @JsonProperty("clientId")
    protected String clientId;
    @JsonProperty("partner_code")
    protected String partnerCode;
    @JsonProperty("service_code")
    protected String serviceCode;
    @JsonProperty("trans_date")
    protected Date transDate;
    @JsonProperty("bank_code")
    protected String bankCode;
    @JsonProperty("amount")
    protected String amount;
    @JsonProperty("acc_no")
    protected String accNo;
    @JsonProperty("shop_code")
    protected String shopCode;
    @JsonProperty("staff_username")
    protected String staffUsername;
    @JsonIgnore
    protected String chainCode;
    @JsonIgnore
    protected String chainName;
    @JsonIgnore
    protected String shopName;
    @JsonIgnore
    protected String provinceCode;
    @JsonIgnore
    protected String staffUserType;
    @JsonIgnore
    protected String fullName;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public String getChainCode() {
        return chainCode;
    }

    public void setChainCode(String chainCode) {
        this.chainCode = chainCode;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getStaffUserType() {
        return staffUserType;
    }

    public void setStaffUserType(String staffUserType) {
        this.staffUserType = staffUserType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }
}
