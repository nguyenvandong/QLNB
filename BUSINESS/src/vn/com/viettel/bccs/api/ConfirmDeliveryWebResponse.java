package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * Created by truon on 14-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfirmDeliveryWebResponse")
public class ConfirmDeliveryWebResponse extends ConfirmDeliveryWebRequest {

    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("error_msg")
    private String errorMsg;

    @JsonProperty("created_date")
    private String createdDate;

    @JsonProperty("trans_date")
    private Date transDate;

    @JsonProperty("cust_fee")
    private String custFee;

    @JsonProperty("sender")
    protected PersonDTO sender;

    @JsonProperty("receiver")
    protected PersonDTO receiver;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("original_order_id")
    private String originalOrderId;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getCustFee() {
        return custFee;
    }

    public void setCustFee(String custFee) {
        this.custFee = custFee;
    }

    public PersonDTO getSender() {
        return sender;
    }

    public void setSender(PersonDTO sender) {
        this.sender = sender;
    }

    public PersonDTO getReceiver() {
        return receiver;
    }

    public void setReceiver(PersonDTO receiver) {
        this.receiver = receiver;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOriginalOrderId() {
        return originalOrderId;
    }

    public void setOriginalOrderId(String originalOrderId) {
        this.originalOrderId = originalOrderId;
    }
}
