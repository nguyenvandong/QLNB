package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class APIUtil {
    public static final ObjectMapper JACKSON_MAPPER_API = new ObjectMapper();

    static {
        JACKSON_MAPPER_API.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JACKSON_MAPPER_API.setSerializationInclusion(Include.NON_NULL);
        // formatdate for ViettelAPI yymmddhh24miss
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        JACKSON_MAPPER_API.setDateFormat(dateFormat);
    }
}
