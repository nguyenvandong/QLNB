package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 11/16/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckVTTransactionResultWebResponse")
public class CheckVTTransactionResultWebResponse extends CheckVTTransactionResultWebRequest {
    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("error_msg")
    private String errorMsg;

    @JsonProperty("reference_code")
    private String referenceCode;
    @JsonProperty("reference_msg")
    private String referenceMsg;

    @JsonProperty("trans_id")
    private String transId;

    @JsonProperty("trans_date")
    private Date transDate;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getReferenceMsg() {
        return referenceMsg;
    }

    public void setReferenceMsg(String referenceMsg) {
        this.referenceMsg = referenceMsg;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }
}
