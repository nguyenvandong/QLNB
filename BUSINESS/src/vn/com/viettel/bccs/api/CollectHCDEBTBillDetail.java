package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 21-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectHCDEBTBillDetail")
public class CollectHCDEBTBillDetail {

    @JsonProperty("period")
    private String period;

    @JsonProperty("expDate")
    private String expDate;

    @JsonProperty("custName")
    private String custName;

    @JsonProperty("minAmount")
    private String minAmount;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("idNumber")
    private String idNumber;

    @JsonProperty("contractNo")
    private String contractNo;

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(String minAmount) {
        this.minAmount = minAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }
}
