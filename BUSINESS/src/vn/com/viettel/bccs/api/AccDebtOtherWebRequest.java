package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccDebtOtherWebRequest")
public class AccDebtOtherWebRequest {
    @JsonProperty("order_id")
    protected String orderId;

    @JsonProperty("request_date")
    protected Date requestDate;

    @JsonProperty("clientId")
    protected String clientId;

    @JsonProperty("shop_code")
    protected String shopCode;

    @JsonProperty("partner_code")
    protected String partnerCode;

    @JsonProperty("staff_username")
    protected String staffUsername;
    @JsonIgnore
    protected String chainCode;
    @JsonIgnore
    protected String chainName;
    @JsonIgnore
    protected String shopName;
    @JsonIgnore
    protected String provinceCode;
    @JsonIgnore
    protected String staffUserType;
    @JsonIgnore
    protected String fullName;

    public String getChainCode() {
        return chainCode;
    }

    public void setChainCode(String chainCode) {
        this.chainCode = chainCode;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getStaffUserType() {
        return staffUserType;
    }

    public void setStaffUserType(String staffUserType) {
        this.staffUserType = staffUserType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }
}
