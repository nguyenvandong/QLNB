package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 11/16/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResendOtherPincodeWebResponse")
public class ResendOtherPincodeWebResponse extends ResendOtherPincodeWebRequest {
    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("error_msg")
    private String errorMsg;

    @JsonProperty("trans_date")
    private Date transDate;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("reference_code")
    private String referenceCode;

    @JsonProperty("original_trans_id")
    private String originalTransId;

    @JsonProperty("real_service_code")
    private String realServiceCode;

    @JsonProperty("real_billing_code")
    private String realBillingCode;

    @JsonProperty("command")
    private String command;

    @JsonProperty("trans_id")
    private String transId;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getOriginalTransId() {
        return originalTransId;
    }

    public void setOriginalTransId(String originalTransId) {
        this.originalTransId = originalTransId;
    }

    public String getRealServiceCode() {
        return realServiceCode;
    }

    public void setRealServiceCode(String realServiceCode) {
        this.realServiceCode = realServiceCode;
    }

    public String getRealBillingCode() {
        return realBillingCode;
    }

    public void setRealBillingCode(String realBillingCode) {
        this.realBillingCode = realBillingCode;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

}
