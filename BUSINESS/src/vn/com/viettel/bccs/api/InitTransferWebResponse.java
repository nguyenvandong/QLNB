package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * Created by truon on 14-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InitTransferWebResponse")
public class InitTransferWebResponse extends InitTransferWebRequest {

    @JsonProperty("trans_id")
    private String transId;

    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("error_msg")
    private String errorMsg;

    @JsonProperty("created_date")
    protected String createdDate;

    @JsonProperty("confirm_code")
    protected String confirmCode;

    @JsonProperty("address")
    protected AddressDTO address;

    @JsonProperty("trans_date")
    private Date transDate;

    @JsonProperty("cust_fee")
    private String custFee;

    @JsonProperty("trans_status")
    private String transStatus;

    @JsonProperty("original_order_id")
    private String originalOrderId;

    @JsonProperty("give_money")
    private String giveMoney;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getConfirmCode() {
        return confirmCode;
    }

    public void setConfirmCode(String confirmCode) {
        this.confirmCode = confirmCode;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getCustFee() {
        return custFee;
    }

    public void setCustFee(String custFee) {
        this.custFee = custFee;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getOriginalOrderId() {
        return originalOrderId;
    }

    public void setOriginalOrderId(String originalOrderId) {
        this.originalOrderId = originalOrderId;
    }

    public String getGiveMoney() {
        return giveMoney;
    }

    public void setGiveMoney(String giveMoney) {
        this.giveMoney = giveMoney;
    }
}
