package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 11/21/2017
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelViettelPartnerWebResponse")
public class CancelViettelPartnerWebResponse extends CancelViettelPartnerWebRequest {

    @JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("error_msg")
    private String errorMsg;
    @JsonProperty("sender")
    private PersonDTO sender;
    @JsonProperty("receiver")
    private PersonDTO receiver;
    @JsonProperty("cust_fee")
    private String custFee;
    @JsonProperty("trans_status")
    private String trans_status;

    @JsonProperty("address")
    private AddressDTO address;
    

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public PersonDTO getSender() {
        return sender;
    }

    public void setSender(PersonDTO sender) {
        this.sender = sender;
    }

    public PersonDTO getReceiver() {
        return receiver;
    }

    public void setReceiver(PersonDTO receiver) {
        this.receiver = receiver;
    }

    public String getCustFee() {
        return custFee;
    }

    public void setCustFee(String custFee) {
        this.custFee = custFee;
    }

    public String getTrans_status() {
        return trans_status;
    }

    public void setTrans_status(String trans_status) {
        this.trans_status = trans_status;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}
