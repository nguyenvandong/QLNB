package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 11/22/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckTransferTransactionWebResponse")
public class CheckTransferTransactionWebResponse extends CheckTransferTransactionWebRequest {
    @JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("error_msg")
    private String errorMsg;
    @JsonProperty("trans_id")
    private String transId;
    @JsonProperty("trans_status")
    private String transStatus;
    @JsonProperty("trans_status_desc")
    private String transStatusDesc;
    @JsonProperty("reference_code")
    private String referenceCode;
    @JsonProperty("trans_date")
    private Date transDate;
    @JsonProperty("original_trans_id")
    private String originalTransId;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getTransStatusDesc() {
        return transStatusDesc;
    }

    public void setTransStatusDesc(String transStatusDesc) {
        this.transStatusDesc = transStatusDesc;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getOriginalTransId() {
        return originalTransId;
    }

    public void setOriginalTransId(String originalTransId) {
        this.originalTransId = originalTransId;
    }
}
