package vn.com.viettel.bccs.api;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import vn.com.viettel.bccs.api.Constant.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTeleChargeVTRequest")
public class GetTeleChargeVTRequest{

	@JsonProperty("order_id")
	protected String orderId;

	@JsonProperty("request_date")
	protected Date requestDate;

	@JsonProperty("shop_code")
	protected String shopCode;

	@JsonProperty("staff_clientId")
	protected String staffClientId;


	@JsonProperty("service_code")
	protected String ServiceCode;

	@JsonProperty("billing_code")
	protected String billingCode;

	@JsonProperty("partner_code")
	protected String partnerCode;

	@JsonProperty("clientId")
	protected String clientId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getStaffClientId() {
		return staffClientId;
	}

	public void setStaffClientId(String staffClientId) {
		this.staffClientId = staffClientId;
	}

	public String getServiceCode() {
		return ServiceCode;
	}

	public void setServiceCode(String serviceCode) {
		ServiceCode = serviceCode;
	}

	public String getBillingCode() {
		return billingCode;
	}

	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

}
