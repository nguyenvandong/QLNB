package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTeleChargeVTResponse")
public class GetTeleChargeVTResponse extends GetTeleChargeVTRequest {
	@JsonProperty("trans_id")
	protected String transId;

	@JsonProperty("trans_date")
	protected Date transDate;

	@JsonProperty("billing_details")
	protected List<TeleChargeBillingDetail> billingDetails;

	@JsonProperty("amount")
	protected String amount;

	@JsonProperty("errorCode")
	protected String errorCode;

	@JsonProperty("error_msg")
	protected String errorMsg;

	@JsonProperty("status")
	protected String status;

	@JsonProperty("tpp_type")
	protected String tpp_type;

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public List<TeleChargeBillingDetail> getBillingDetails() {
		return billingDetails;
	}

	public void setBillingDetails(List<TeleChargeBillingDetail> billingDetails) {
		this.billingDetails = billingDetails;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTpp_type() {
		return tpp_type;
	}

	public void setTpp_type(String tpp_type) {
		this.tpp_type = tpp_type;
	}
}
