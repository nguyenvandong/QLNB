package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 05-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonDTO")
public class PersonDTO {
    @JsonProperty("name")
    protected String name;

    @JsonProperty("msisdn")
    protected String msisdn;

    @JsonProperty("id_number")
    protected String idNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
}
