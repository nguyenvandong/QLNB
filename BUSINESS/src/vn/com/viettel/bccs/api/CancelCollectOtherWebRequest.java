package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 11/20/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelCollectOtherWebRequest")
public class CancelCollectOtherWebRequest {
    @JsonProperty("order_id")
    protected String orderId;
    @JsonProperty("clientId")
    protected String clientId;
    @JsonProperty("billing_code")
    protected String billingCode;
    @JsonProperty("service_code")
    protected String serviceCode;
    @JsonProperty("amount")
    protected String amount;
    @JsonProperty("staff_username")
    protected String staffUsername;
    @JsonProperty("request_date")
    protected Date requestDate;
    @JsonProperty("partner_code")
    protected String partnerCode;
    @JsonProperty("reference_code")
    protected String referenceCode;
    @JsonProperty("original_order_id")
    protected String originalOrderId;
    @JsonProperty("channel_info")
    protected String channelInfo;
    @JsonProperty("partner_type")
    protected String partnerType;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getOriginalOrderId() {
        return originalOrderId;
    }

    public void setOriginalOrderId(String originalOrderId) {
        this.originalOrderId = originalOrderId;
    }

    public String getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(String channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }
}
