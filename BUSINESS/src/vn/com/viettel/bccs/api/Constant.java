package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public class Constant {
    public enum DomainService {
        VT_PREPAID("000000"),
        VT_POSTPAID("000001"),
        VT_MOBILE("100000"),
        VT_INTERNET_OR_CAB("000003"),
        VT_FIXED_PHONE("000004"),
        VT_HOMEPHONE_PREPAID("000012"),
        VT_HOMEPHONE_POSTPAID("000022"),
        VT_HOMEPHONE("200000"),
        OTHER_EVN("EVN"),
        OTHER_WACO("WACO"),
        OTHER_FECREDIT("FECREDIT"),
        OTHER_HOMECREDIT("HOMECREDIT"),
        OTHER_TRANSFER("TRANSFER"),
        OTHER_PCE_CARD("PCE"),
        OTHER_PINCODE("OPCE"),
        VINA_RECHARGE("VNPPRE"),
        MOBI_RECHARGE("VMSPRE"),
        BEE_RECHARGE("BEEPRE"),
        VNM_RECHARGE("VNMPRE"),
        OTHER_RECHARGE("ORECHARGE");

        private String code;

        DomainService(String code) {
            this.code = code;
        }

        @JsonCreator
        public static DomainService toDomainService(String serviceCode) {
            if (serviceCode != null) {
                for (DomainService service : DomainService.values()) {
                    if (serviceCode.equalsIgnoreCase(service.getCode())) {
                        return service;
                    }
                }
            }
            return null;
        }

        @JsonValue
        public String getCode() {
            return code;
        }

        @Override
        public String toString() {
            return super.toString() + "-" + code;
        }
    }

    public enum ApiCmd {
        GET_TELECHARGE_VT_INFO_WEB("Truy vấn nợ cước viễn thông Viettel"),
        PAY_TELECHARGE_VT_WEB("Thanh toán cước viễn thông Viettel"),
        PAY_PINCODE_VT_WEB("Mua thẻ cào Viettel"),
        QUERY_CHAIN_INFO_WEB("Truy vấn công nợ chuỗi"),
        MAKE_TRANSFER("Lập yêu cầu chuyển tiền"),
        EDIT_TRANSFER("Thay đổi thông tin chuyển tiền"),
        GET_FEE("Lấy phí giao dịch chuyển tiền"),
        INIT_TRANSFER("Khởi tạo giao dịch giao tiền/Hủy giao dịch chuyển tiền"),
        CONFIRM_DELIVERY("Xác nhận giao tiền"),
        CHECK_TRANSACTION_WEB("Truy vấn lịch sử giao dịch"),
        RESEND_PINCODE_VT_WEB("Gửi lại mã thẻ cào"),
        RESEND_PINCODE("Gửi lại mã thẻ cào ngoại mạng"),
        CANCEL_TPP_REQUEST_WEB("Lấy mã OTP hủy giao dịch"),
        CANCEL_TPP_WEB("Hủy giao dịch cước viễn thông"),
        CANCEL_PAY("Hủy giao dịch thu hộ"),
        GET_BILL("Truy vấn nợ cước hoá đơn hoặc check kho thẻ cào ngoại mạng"),
        PAY_BILL("Thực hiện gửi yêu cầu thanh toán cho hoá đơn nợ cước đã truy vấn hoặc thực hiện nạp tiền cước, hoặc mua thẻ cào ngoại mạng"),
        SEND_SMS_WEB("Gửi tin nhắn đến người dùng"),
        CHECK_PAY("Kiểm tra kết quả giao dịch"),
        PARTNER_CANCEL("Hủy giao dịch theo mã giao dịch"),
        CHECK_TRANSFER("Kiểm tra giao dịch chuyển tiền"),
        CONFIRM_CANCEL("Xác nhận hủy giao dịch chuyển tiền"),
        QUERY_MAPPING_PREPAID("Mapping tài khoản BCCS"),
        PAY_IN_PREPAID_WEB("Nạp tiền vào tài khoản BCCS"),
        WITHDRAW_PREPAID_WEB("Rút tiền từ tài khoản BCCS"),
        PAY_TELECHARGE_VT_BATCH("Thanh toán cước viễn thông theo lô");
        private String fullName;

        ApiCmd(String fullName) {
            this.fullName = fullName;
        }

        public String getFullName() {
            return fullName;
        }
    }

    //duyetdk
    public enum PartnerService {
        VCOIN, GARENA, MEGACARD, ZINGPCE, GATE, ONCASH, VTCKIS, VTCKAV, VTCBAV,
        VMSPCE, VNPPCE, BEEPCE, VNMPCE, VTCTHS
    }

}
