package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * Created by buiqu on 10/25/2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaychargeHomephoneResponse")
public class PaychargeHomephoneResponse extends PayChargeHomephoneRequest {
    @JsonProperty("trans_fee")
    private String transFee;

    @JsonProperty("trans_id")
    private String transId;

    @JsonProperty("trans_date")
    private Date transDate;

    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("error_msg")
    private String error_msg;

    @JsonProperty("tpp_type")
    protected String tpp_type;

    public String getTransFee() {
        return transFee;
    }

    public void setTransFee(String transFee) {
        this.transFee = transFee;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public String getTpp_type() {
        return tpp_type;
    }

    public void setTpp_type(String tpp_type) {
        this.tpp_type = tpp_type;
    }
}
