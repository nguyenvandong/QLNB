package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 12/12/2017
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayPincodeOtherTelcoWebRequest")
public class PayPincodeOtherTelcoWebRequest {
    @JsonProperty("order_id")
    protected String orderId;

    @JsonProperty("request_date")
    protected Date requestDate;

    @JsonProperty("clientId")
    protected String clientId;

    @JsonProperty("partner_code")
    protected String partnerCode;

    @JsonProperty("channel_info")
    protected String channelInfo;

    @JsonProperty("payer_name")
    protected String payerName;

    @JsonProperty("payer_msisdn")
    protected String payerMsisdn;

    @JsonProperty("payer_address")
    protected String payerAddress;

    @JsonProperty("service_code")
    protected String serviceCode;

    @JsonProperty("partner_type")
    protected String partnerType;

    @JsonProperty("amount")
    protected String amount;

    @JsonProperty("billing_code")
    protected String billingCode;

    @JsonProperty("reference_code")
    protected String referenceCode;

    @JsonProperty("staff_username")
    protected String staffUsername;

    @JsonIgnore
    private String chainCode;
    @JsonIgnore
    private String chainName;
    @JsonIgnore
    private String shopCode;
    @JsonIgnore
    private String shopName;
    @JsonIgnore
    private String provinceCode;
    @JsonIgnore
    private String staffUserType;
    @JsonIgnore
    private String fullName;
    @JsonIgnore
    private String cmd;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(String channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerMsisdn() {
        return payerMsisdn;
    }

    public void setPayerMsisdn(String payerMsisdn) {
        this.payerMsisdn = payerMsisdn;
    }

    public String getPayerAddress() {
        return payerAddress;
    }

    public void setPayerAddress(String payerAddress) {
        this.payerAddress = payerAddress;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public String getChainCode() {
        return chainCode;
    }

    public void setChainCode(String chainCode) {
        this.chainCode = chainCode;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getStaffUserType() {
        return staffUserType;
    }

    public void setStaffUserType(String staffUserType) {
        this.staffUserType = staffUserType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }
}
