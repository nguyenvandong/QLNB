package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 10/20/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayOtherTelecomResponse")
public class PayOtherTelecomResponse extends PayOtherTelecomRequest{

    @JsonProperty("trans_id")
    protected String transId;

    @JsonProperty("errorCode")
    protected String errorCode;

    @JsonProperty("error_msg")
    protected String errorMsg;

    @JsonProperty("trans_fee")
    protected String transFee;

    @JsonProperty("trans_date")
    protected Date transDate;

    @JsonProperty("real_service_code")
    protected String realServiceCode;

    @JsonProperty("real_billing_code")
    protected String realBillingCode;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getTransFee() {
        return transFee;
    }

    public void setTransFee(String transFee) {
        this.transFee = transFee;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getRealServiceCode() {
        return realServiceCode;
    }

    public void setRealServiceCode(String realServiceCode) {
        this.realServiceCode = realServiceCode;
    }

    public String getRealBillingCode() {
        return realBillingCode;
    }

    public void setRealBillingCode(String realBillingCode) {
        this.realBillingCode = realBillingCode;
    }
}
