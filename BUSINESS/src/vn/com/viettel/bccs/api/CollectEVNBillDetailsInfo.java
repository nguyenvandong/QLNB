package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 20-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectEVNBillDetailsInfo")
public class CollectEVNBillDetailsInfo {

    @JsonProperty("dnttdetail")
    private String dnttdetail;

    @JsonProperty("chitietgiadetail")
    private String chitietgiadetail;

    @JsonProperty("chitiettiendetail")
    private String chitiettiendetail;

    public String getDnttdetail() {
        return dnttdetail;
    }

    public void setDnttdetail(String dnttdetail) {
        this.dnttdetail = dnttdetail;
    }

    public String getChitietgiadetail() {
        return chitietgiadetail;
    }

    public void setChitietgiadetail(String chitietgiadetail) {
        this.chitietgiadetail = chitietgiadetail;
    }

    public String getChitiettiendetail() {
        return chitiettiendetail;
    }

    public void setChitiettiendetail(String chitiettiendetail) {
        this.chitiettiendetail = chitiettiendetail;
    }
}
