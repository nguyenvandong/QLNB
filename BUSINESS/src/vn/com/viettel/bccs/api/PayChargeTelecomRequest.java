package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import vn.com.viettel.bccs.api.Constant.DomainService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayChargeTelecomRequest")
public class PayChargeTelecomRequest {
	@JsonProperty("clientId")
	protected String clientId;

	@JsonProperty("order_id")
	protected String orderId;

	@JsonProperty("request_date")
	protected Date requestDate;

	@JsonProperty("shop_code")
	protected String shopCode;

	@JsonProperty("staff_clientId")
	protected String staffClientId;

	@JsonProperty("partner_code")
	protected String partnerCode;

	@JsonProperty("service_code")
	protected String serviceCode;

	@JsonProperty("amount")
	protected String amount;

	@JsonProperty("original_trans_id")
	protected String orginalTransId;

	@JsonProperty("billing_code")
	protected String billingCode;

	@JsonProperty("payer_address")
	protected String payerAddress;

	@JsonProperty("payer_msisdn")
	protected String payerMsisdn;

	@JsonProperty("payer_name")
	protected String payerName;

	@JsonProperty("tpp_type")
	protected String tpp_type;

	public String getBillingCode() {
		return billingCode;
	}

	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOrginalTransId() {
		return orginalTransId;
	}

	public void setOrginalTransId(String orginalTransId) {
		this.orginalTransId = orginalTransId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getStaffClientId() {
		return staffClientId;
	}

	public void setStaffClientId(String staffClientId) {
		this.staffClientId = staffClientId;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getPayerAddress() {
		return payerAddress;
	}

	public void setPayerAddress(String payerAddress) {
		this.payerAddress = payerAddress;
	}

	public String getPayerMsisdn() {
		return payerMsisdn;
	}

	public void setPayerMsisdn(String payerMsisdn) {
		this.payerMsisdn = payerMsisdn;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getTpp_type() {
		return tpp_type;
	}

	public void setTpp_type(String tpp_type) {
		this.tpp_type = tpp_type;
	}
}
