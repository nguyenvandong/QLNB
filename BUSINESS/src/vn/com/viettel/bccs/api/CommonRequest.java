package vn.com.viettel.bccs.api;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonRequest")
public class CommonRequest {
    @JsonProperty("order_id")
    protected String orderId;

    @JsonProperty("clientId")
    protected String clientId;

    @JsonProperty("request_date")
    protected Date requestDate;

    @JsonProperty("staff_clientId")
    protected String staffClientId;

    @JsonProperty("shop_code")
    protected String shopCode;

    @JsonProperty("partner_code")
    protected String partnerCode;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getStaffClientId() {
        return staffClientId;
    }

    public void setStaffClientId(String staffClientId) {
        this.staffClientId = staffClientId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }


}
