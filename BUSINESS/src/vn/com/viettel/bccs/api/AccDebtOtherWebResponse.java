package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccDebtOtherWebResponse")
public class AccDebtOtherWebResponse extends AccDebtOtherWebRequest {

    @JsonProperty("trans_id")
    private String transId;

    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("error_msg")
    private String errorMsg;

    @JsonProperty("trans_date")
    private Date transDate;

    @JsonProperty("account_type")
    private String accountType;

    @JsonProperty("balance")
    private String balance;

    @JsonProperty("debit")
    private String debit;
    @JsonProperty("acc_detail")
    private List<BankplusBccsAcountMapping> accDetailList;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public List<BankplusBccsAcountMapping> getAccDetailList() {
        return accDetailList;
    }

    public void setAccDetailList(List<BankplusBccsAcountMapping> accDetailList) {
        this.accDetailList = accDetailList;
    }
}
