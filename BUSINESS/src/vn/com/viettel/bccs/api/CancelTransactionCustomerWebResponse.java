package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author hungnq on 11/27/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelTransactionCustomerWebResponse")
public class CancelTransactionCustomerWebResponse extends CancelTransactionCustomerWebRequest {
    @JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("error_msg")
    private String errorMsg;
    @JsonProperty("sender")
    private PersonDTO sender;
    @JsonProperty("receiver")
    private PersonDTO receiver;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("cust_fee")
    private String custFee;
    @JsonProperty("trans_status")
    private String transStatus;
    @JsonProperty("original_order_id")
    private String originalOrderId;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public PersonDTO getSender() {
        return sender;
    }

    public void setSender(PersonDTO sender) {
        this.sender = sender;
    }

    public PersonDTO getReceiver() {
        return receiver;
    }

    public void setReceiver(PersonDTO receiver) {
        this.receiver = receiver;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCustFee() {
        return custFee;
    }

    public void setCustFee(String custFee) {
        this.custFee = custFee;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getOriginalOrderId() {
        return originalOrderId;
    }

    public void setOriginalOrderId(String originalOrderId) {
        this.originalOrderId = originalOrderId;
    }

}
