package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author hungnq on 10/20/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayOtherTelecomRequest")
public class PayOtherTelecomRequest{
    @JsonProperty("service_code")
    protected String serviceCode;

    @JsonProperty("billing_code")
    protected String billingCode;

    @JsonProperty("order_id")
    protected String orderID;

    @JsonProperty("staff_username")
    protected String staffUserName;

    @JsonProperty("partner_code")
    protected String partnerCode;

    @JsonProperty("amount")
    protected String amount;

    @JsonProperty("reference_code")
    protected String referenceCode;

    @JsonProperty("payer_msisdn")
    protected String payerMsisdn;

    @JsonProperty("channel_info")
    protected String channelInfo;

    @JsonProperty("partner_type")
    protected String partnerType;

    @JsonProperty("request_date")
    protected String requestDate;

    @JsonProperty("shop_code")
    protected String shopCode;

    @JsonProperty("clientId")
    protected String clientId;

    @JsonProperty("payer_name")
    protected String payerName;
    @JsonProperty("payer_address")
    protected String payerAddress;


    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getStaffUserName() {
        return staffUserName;
    }

    public void setStaffUserName(String staffUserName) {
        this.staffUserName = staffUserName;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getPayerMsisdn() {
        return payerMsisdn;
    }

    public void setPayerMsisdn(String payerMsisdn) {
        this.payerMsisdn = payerMsisdn;
    }

    public String getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(String channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerAddress() {
        return payerAddress;
    }

    public void setPayerAddress(String payerAddress) {
        this.payerAddress = payerAddress;
    }
}
