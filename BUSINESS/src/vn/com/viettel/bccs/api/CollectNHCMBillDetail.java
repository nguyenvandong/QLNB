package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 04-11-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectNHCMBillDetail")
public class CollectNHCMBillDetail {

    @JsonProperty("mahd")
    private String mahd;

    @JsonProperty("sohd")
    private String sohd;

    @JsonProperty("sotien")
    private String sotien;

    @JsonProperty("ky")
    private String ky;

    @JsonProperty("nam")
    private String nam;

    public String getMahd() {
        return mahd;
    }

    public void setMahd(String mahd) {
        this.mahd = mahd;
    }

    public String getSohd() {
        return sohd;
    }

    public void setSohd(String sohd) {
        this.sohd = sohd;
    }

    public String getSotien() {
        return sotien;
    }

    public void setSotien(String sotien) {
        this.sotien = sotien;
    }

    public String getKy() {
        return ky;
    }

    public void setKy(String ky) {
        this.ky = ky;
    }

    public String getNam() {
        return nam;
    }

    public void setNam(String nam) {
        this.nam = nam;
    }
}
