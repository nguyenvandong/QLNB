package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author hungnq on 11/22/2017
 */
public class BankplusBccsAcountMapping {
    @JsonProperty("acc_no")
    private String accNo;
    @JsonProperty("bank_code")
    private String bankCode;

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
