package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 11/16/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckOtherTransactionWebRequest")
public class CheckOtherTransactionWebRequest {
    @JsonProperty("order_id")
    protected String orderId;
    @JsonProperty("clientId")
    protected String clientId;
    @JsonProperty("partner_code")
    protected String partnerCode;
    @JsonProperty("request_date")
    private Date requestDate;
    @JsonProperty("staff_username")
    protected String staffUsername;

    @JsonProperty("service_code")
    protected String serviceCode;
    @JsonProperty("billing_code")
    protected String billingCode;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }
}
