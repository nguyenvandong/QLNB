package vn.com.viettel.bccs.api.topup;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * Created by truon on 05-01-2018.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopupTransferRequest")
public class TopupTransferRequest {
    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("clientId")
    private String clientId;

    @JsonProperty("service_code")
    private String serviceCode;

    @JsonProperty("partner_code")
    private String partnerCode;

    @JsonProperty("partner_msisdn")
    private String partnerMsisdn;

    @JsonProperty("shop_id")
    private String shopId;

    @JsonProperty("channel_info")
    private ChannelInfoDTO channelInfo;

    @JsonProperty("batch")
    private List<TopupTransferDetailDTO> batch;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getPartnerMsisdn() {
        return partnerMsisdn;
    }

    public void setPartnerMsisdn(String partnerMsisdn) {
        this.partnerMsisdn = partnerMsisdn;
    }

    public ChannelInfoDTO getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(ChannelInfoDTO channelInfo) {
        this.channelInfo = channelInfo;
    }

    public List<TopupTransferDetailDTO> getBatch() {
        return batch;
    }

    public void setBatch(List<TopupTransferDetailDTO> batch) {
        this.batch = batch;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
