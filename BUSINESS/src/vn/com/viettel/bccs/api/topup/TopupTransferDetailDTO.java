package vn.com.viettel.bccs.api.topup;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 05-01-2018.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopupTransferDetailDTO")
public class TopupTransferDetailDTO {

    @JsonProperty("suborder_id")
    private String suborderId;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("billing_code")
    private String billingCode;

    public String getSuborderId() {
        return suborderId;
    }

    public void setSuborderId(String suborderId) {
        this.suborderId = suborderId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }
}
