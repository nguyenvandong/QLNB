package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 11/20/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelCollectOtherWebResponse")
public class CancelCollectOtherWebResponse extends CancelCollectOtherWebRequest {
    @JsonProperty("real_service_code")
    private String realServiceCode;
    @JsonProperty("real_billing_code")
    private String realBillingCode;
    @JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("command")
    private String command;
    @JsonProperty("error_msg")
    private String errorMsg;
    @JsonProperty("trans_id")
    private String transId;
    @JsonProperty("trans_date")
    private Date transDate;
    @JsonProperty("original_trans_id")
    private String originalTransId;


    public String getRealServiceCode() {
        return realServiceCode;
    }

    public void setRealServiceCode(String realServiceCode) {
        this.realServiceCode = realServiceCode;
    }

    public String getRealBillingCode() {
        return realBillingCode;
    }

    public void setRealBillingCode(String realBillingCode) {
        this.realBillingCode = realBillingCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getOriginalTransId() {
        return originalTransId;
    }

    public void setOriginalTransId(String originalTransId) {
        this.originalTransId = originalTransId;
    }
}
