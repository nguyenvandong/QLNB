package com.viettel.fw.logging;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.common.collect.Lists;
import com.viettel.fw.Exception.LogicException;
import com.viettel.fw.SystemConfig;
import com.viettel.fw.common.util.*;
import com.viettel.fw.dto.SessionDTO;
import com.viettel.fw.log.LogCollectorActor;
import com.viettel.web.log.LoggingInfo;
import com.viettel.web.log.LoggingUtils;
import com.viettel.ws.common.utils.GenericWebInfo;
import com.viettel.ws.common.utils.Locate;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

/**
 * tuyennt17
 * thiendn1: optimize
 */
@Aspect
public class ServiceLogging {

    private AtomicLong idErrorL = new AtomicLong(0L);
    private static final Logger logger = Logger.getLogger("aop");
    @Autowired
    protected ActorSystem actorSystem;

    @Autowired(required = false)
    @Qualifier(SystemConfig.LOG_ANALYTIC_ACTOR)
    ActorRef logAnalyticActor;

    @Value("${defaultSys:null}")
    private String DEFAULT_SYS;
    @Value("${LOGGING_HELPER:0}")
    private String writeLog;


    private Object proceed(ProceedingJoinPoint jp,GenericWebInfo genericWebInfo) throws Throwable {
        genericWebInfo.setAppCode(DEFAULT_SYS);
        String LOG_TYPE_START_ACTION = "start_action";
        String LOG_TYPE_END_ACTION = "end_action";
        LoggingInfo startLog = getLoggingInfo(jp, LOG_TYPE_START_ACTION,genericWebInfo);
        LoggingInfo endLog = getLoggingInfo(jp, LOG_TYPE_END_ACTION,genericWebInfo);
        Object value = null;
        Date startTime = null;
        Date endTime;
        String idError = getIncremntIdError();
        String errorCode = Const.LOGGING_INFO.ERROR_CODE_DEFAULT;
        String errorDesscription = getText("common.msg.transaction.success");
        String transaction = Const.LOGGING_INFO.TRANS_OK;
        Throwable exception = null;
        try {
            Object[] params = jp.getArgs();
            for (Object o : params) {
                if(o instanceof SessionDTO){
                    ((SessionDTO)o).setSessionId(genericWebInfo.getSessionId());
                    ((SessionDTO)o).setClientNode(genericWebInfo.getCurrentAddress());
                    if(startLog.getAccount() == null) {
                        startLog.setAccount(((SessionDTO)o).getIsdnOrAccount());
                    }
                    if(endLog.getAccount() == null) {
                        endLog.setAccount(((SessionDTO)o).getIsdnOrAccount());
                    }
                }
            }

            startTime = new Date();
            startLog.setTimer(DateUtil.date2StringByPattern(startTime, "yyyy/MM/dd hh:mm:ss:ms"));
            value = jp.proceed();
        }catch (Throwable e){
            errorCode = LoggingUtils.getErrorCode(e, idError);
            errorDesscription =getText("common.msg.transaction.error") +" : " + ( (e instanceof LogicException) ? e.getMessage() : e.getMessage().substring(0, 160));
            transaction = Const.LOGGING_INFO.TRANS_NOK;// transaction fail
            exception = DataUtil.deepCloneObject(e);
        }
        endTime = new Date();
        endLog.setTimer(DateUtil.date2StringByPattern(endTime, "yyyy/MM/dd hh:mm:ss:ms"));
        Long duringTime = Math.abs((startTime != null ? startTime.getTime() : 0) - endTime.getTime());
        //Set ErrorCode
        startLog.setErrorCode(errorCode);
        endLog.setErrorCode(errorCode);
        //Set ErrorDescription
        startLog.setErrorDescription(errorDesscription);
        endLog.setErrorDescription(errorDesscription);
        //Set Transaction
        startLog.setTransactionStatus(transaction);
        endLog.setTransactionStatus(transaction);
        //Set Time during
        startLog.setDurtation(duringTime.toString());
        endLog.setDurtation(duringTime.toString());
        //write LOGGER
        writeLogger(startLog,endLog);
        if(exception != null) {
            throw exception;
        }
        return value;
    }

    private String getIncremntIdError() throws Exception {
        return String.valueOf(idErrorL.incrementAndGet());
    }



    @Around("@annotation(com.viettel.fw.log.annotation.Loggable)")
    public Object loggingTraceAction(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        if(DataUtil.safeEqual(writeLog, Const.LOGGING_INFO.WRITE_LOG_OK)){
            GenericWebInfo genericWebInfo = GetTextFromBundleHelper.getGenericWebInfo();
            return proceed(proceedingJoinPoint, genericWebInfo);
        }
        return proceedingJoinPoint.proceed();
    }



    private void writeLogger(LoggingInfo starLog, LoggingInfo endLog) {
        String logStart = starLog.toString();
        String logEnd = endLog.toString();
        actorSystem.actorSelection("user/ServiceLogger").tell(new LogCollectorActor.ACTION(Lists.newArrayList(logStart, logEnd)), null);

    }

    private LoggingInfo getLoggingInfo(ProceedingJoinPoint jp, String logType, GenericWebInfo genericWebInfo) {
        LoggingInfo loggingInfo = new LoggingInfo(logType);
        MethodSignature methodSignature = (MethodSignature) jp.getSignature();
        try {
            loggingInfo.setParams(getAllParams(jp));
            loggingInfo.setFunctionName(methodSignature.getMethod().getName());
            loggingInfo.setSessionId(genericWebInfo.getSessionId());
            String ipAdress = DataUtil.isNullOrEmpty(genericWebInfo.getServerAddress()) ? genericWebInfo.getIpAddress() : genericWebInfo.getServerAddress();
            String ipPortParrent = DataUtil.isNullOrEmpty(genericWebInfo.getPortAddress()) ? genericWebInfo.getServerPort() : genericWebInfo.getPortAddress();
            loggingInfo.setiPPortParentNode(ipAdress+":"+ipPortParrent);
            loggingInfo.setClassName(methodSignature.getDeclaringTypeName());
            loggingInfo.setUsername(genericWebInfo.getStaffCode());
            loggingInfo.setIpaddress(genericWebInfo.getIpAddress());
            loggingInfo.setPath(genericWebInfo.getCurrentAddress());
            loggingInfo.setAppCode(genericWebInfo.getAppCode());
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            logger.error("System Calling Info Logging : " + e.getMessage());
        }
        return loggingInfo;
    }

    private String getAllParams(ProceedingJoinPoint jp) {
        String params = "";
        try {
            Object[] parammeters = jp.getArgs();
            int length = parammeters.length;
            StringBuilder par = new StringBuilder("");
            Arrays.stream(parammeters).forEach(o -> {
                par.append(o.toString());
                if (!parammeters[length - 1].equals(o)) {
                    par.append(";");
                }
            });
            params = par.toString();
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            logger.error("Exception when try get params");
        }
        return params;
    }

    private Locate getLocate() {
        return GetTextFromBundleHelper.getLocate();
    }

    private String getText(String key) {
        return BundleUtil.getText(getLocate(), key);
    }
}

